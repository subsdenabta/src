# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Simulate params parsing before the controller being hit
def jsonapi_parsed hash
  ActionDispatch::Request.parameter_parsers[:jsonapi].call hash.to_json
end

def jsonapi_document data: {}
  {
    data: data
  }
end

def jsonapi_resource type: nil, id: nil, attributes: nil
  {}.tap do |result|
    result[:type] = type if type
    result[:id] = id if id
    result[:attributes] = attributes if attributes
  end
end
