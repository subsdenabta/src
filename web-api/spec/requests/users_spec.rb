# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "Users", type: :request do

  before do
    connected_as_administrator
  end

  let(:user_data) {
    {
      "data": {
        "type": "users",
        "attributes": {
          "first_name": "John",
          "last_name": "Carter",
          "identifier": "jcarter",
          "email": "jc@ch.tld",
          "disabled_at": nil
        },
        "relationships": {
          "role": {
            "data": {
              "type": "roles",
              "id": "physician"
            }
          }
        }
      }
    }
  }

  describe "POST /users" do

    describe "when the user is valid" do

      it "saves a new user" do
        # Given
        expect {
          # When
          post "/users", params: user_data.to_json, headers: headers
          # Then
        }.to change(User, :count).by(1)
        expect(response).to have_http_status(201)
      end

      it "sends an email" do
        # Given
        expect {
          # When
          post "/users", params: user_data.to_json, headers: headers
          # Then
        }.to change(ActionMailer::Base.deliveries, :count).by(1)
        expect(response).to have_http_status(201)
      end

      describe "when email sending fails" do

        before do
          allow(PasswordMailer).to receive_message_chain("created.deliver").and_raise("SMTP_ERROR")
        end

        it "returns 502 Bad Gateway" do
          # Given
          expect {
            # When
            post "/users", params: user_data.to_json, headers: headers
            # Then
          }.to_not change(ActionMailer::Base.deliveries, :count)
          expect(response).to have_http_status(502)
        end

        it "deletes just created user" do
          # Given
          expect {
            # When
            post "/users", params: user_data.to_json, headers: headers
            # Then
          }.to_not change(User, :count)
        end
      end
    end
  end

  describe "PACTH /users/:id" do

    describe "concerning deactivation" do

      let!(:concerned_user) { create :user, id: 42 }

      before { Doorkeeper::AccessToken.create!(resource_owner_id: concerned_user.id) }

      describe "when datetime provided" do

        it "immediately removes access if it is equal or before current time" do
          # Given
          user_data[:data][:attributes][:disabled_at] = (Time.zone.now - 1.minute).iso8601
          expect {
            # When
            patch "/users/42", params: user_data.to_json, headers: headers
            # Then
          }.not_to have_enqueued_job(DisableUserJob)
          user_tokens = Doorkeeper::AccessToken.where(resource_owner_id: concerned_user.id)
          expect(user_tokens.count).to eq 1
          expect(user_tokens.first).to be_revoked
        end

        it "is scheduled to remove access if it is after the current time" do
          # Given
          user_data[:data][:attributes][:disabled_at] = (Time.zone.now + 1.hour).iso8601
          expect {
            # When
            patch "/users/42", params: user_data.to_json, headers: headers
            # Then
          }.to have_enqueued_job(DisableUserJob)
          user_tokens = Doorkeeper::AccessToken.where(resource_owner_id: concerned_user.id)
          expect(user_tokens.count).to eq 1
          expect(user_tokens.first).not_to be_revoked
        end
      end

      describe "when no datetime provided" do
        it "does not change token access" do
          # Given
          expect {
            # When
            patch "/users/42", params: user_data.to_json, headers: headers
            # Then
          }.not_to have_enqueued_job(DisableUserJob)
          result = Doorkeeper::AccessToken.where(resource_owner_id: concerned_user.id).count
          expect(result).to eq 1
        end
      end
    end
  end
end
