# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "Certificates", type: :request do

  before do
    connected_as_administrator
  end

  describe "PATCH /certificates/:id" do

    let(:certificate_data) {
      {
        data: {
          type: "certificates",
          id: certificate.id.to_s,
          attributes: attributes
        }
      }
    }

    describe "with valid questionnaire and answers" do
      let(:certificate) { create :certificate }
      let(:attributes) do
        {
          questionnaire: {},
          data: {},
        }
      end

      it "saves the received JSON" do
        # When
        patch "/certificates/#{certificate.id}", params: certificate_data.to_json, headers: headers
        # Then
        certificate.reload
        expect(certificate.questionnaire).to eq({})
        expect(certificate.data).to eq({})
      end
    end

    describe "with null questionnaire and answers" do
      let(:certificate) { create :certificate }
      let(:attributes) do
        {
          questionnaire: nil,
          data: nil,
        }
      end

      it "saves the received JSON" do
        # When
        patch "/certificates/#{certificate.id}", params: certificate_data.to_json, headers: headers
        # Then
        certificate.reload
        expect(certificate.questionnaire).to eq(nil)
        expect(certificate.data).to eq(nil)
      end
    end
  end

  describe "PATCH /certificates/:id/send_by_mail" do

    let!(:certificate) { create :certificate, id: 42 }
    let(:certificate_data) {
      {
        data: {
          type: "certificates",
          attributes: {
            recipient: "moulin@pj.fr"
          }
        }
      }
    }

    describe "with invalid recipient" do
      it "returns a 422 error" do
        # Given
        certificate_data[:data][:attributes][:recipient] = "du texte simple"
        expect {
          # When
          patch "/certificates/42/send_by_mail", params: certificate_data.to_json, headers: headers
          # Then
        }.not_to have_enqueued_job(ActionMailer::MailDeliveryJob)
        expect(response).to have_http_status(422)
      end
    end

    describe "with valid recipient" do
      it "creates a job to send the certificate by mail" do
        # Given
        expect {
          # When
          patch "/certificates/42/send_by_mail", params: certificate_data.to_json, headers: headers
          # Then
        }.to have_enqueued_job(ActionMailer::MailDeliveryJob)
        expect(response).to have_http_status(204)
      end
    end
  end
end
