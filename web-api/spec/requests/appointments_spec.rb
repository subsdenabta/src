# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "Appointments", type: :request do

  before { connected_as_administrator }

  let!(:consultation_type) { create :consultation_type, id: 12 }
  let!(:consultation_subtype) { create :consultation_subtype, id: 122, consultation_type: consultation_type }
  let!(:consultation_reason) { create :consultation_reason, id: 122, consultation_type: consultation_type }
  let!(:calling_institution) { create :calling_institution, id: 11 }
  let!(:patient) { create :patient, id: 42 }

  describe "POST /appointments" do

    let(:appointment_data) {
      {
        "data": {
          "type": "appointments",
          "attributes": {
            "starts_at": "2018-05-21T07:30Z",
            "duration": "PT1H30M",
            "officer_name": "Commissaire Jules Maigret",
            "officer_phone": "0617181910",
            "officer_email": "jmaigret@filae.tld",
            "official_report": "223344",
            "circumstances": nil,
            "comment": nil
          },
          "relationships": {
            "consultation_type": {
              "data": { "type": "consultation_types", "id": "12" }
            },
            "consultation_subtype": {
              "data": { "type": "consultation_subtypes", "id": "122" }
            },
            "consultation_reason": {
              "data": { "type": "consultation_reasons", "id": "122" }
            },
            "calling_institution": {
              "data": { "type": "calling_institutions", "id": "11" }
            }
          }
        }
      }
    }

    it "creates a new appointment" do
      # Given
      expect {
        # When
        post "/appointments", params: appointment_data.to_json, headers: headers
        # Then
      }.to change(Appointment, :count).by(1)
      expect(response).to have_http_status(201)
    end

    it "creates a new officer" do
      # Given
      expect {
        # When
        post "/appointments", params: appointment_data.to_json, headers: headers
        # Then
      }.to change(Officer, :count).by(1)
      expect(response).to have_http_status(201)
    end

    describe "regarding existing patient" do
      it "uses her" do
        # Given
        appointment_data[:data][:relationships].merge!({
            "patient": {
              "data": { "type": "patients", "id": "42" }
            }
        })
        expect {
          # When
          post "/appointments", params: appointment_data.to_json, headers: headers
          # Then
        }.not_to change(Patient, :count)
        expect(response).to have_http_status(201)
      end
    end

    describe "regarding a new patient" do
      it "creates her" do
        # Given
        appointment_data[:data][:attributes].merge!({
          "patient_first_name": "Alex",
          "patient_last_name": "Belluci",
          "patient_birthdate": "1964-09-30",
          "patient_gender": "woman",
          "patient_phone": "0716151413",
        })
        expect {
          # When
          post "/appointments", params: appointment_data.to_json, headers: headers
          # Then
        }.to change(Patient, :count).by(1)
        expect(response).to have_http_status(201)
      end
    end

    it "creates a 'created' log job" do
      # Given
      created_action = Log::Action.new("created")
      expect {
        # When
        post "/appointments", params: appointment_data.to_json, headers: headers
        # Then
      }.to have_enqueued_job(LogJob)
      compare_log_event_to([created_action])
    end
  end

  describe "PATCH /appointments/12" do

    let(:officer) { create :officer }
    let!(:other_patient) { create :patient, id: 49 }
    let!(:new_consultation_type) { create :consultation_type, id: 42 }
    let!(:new_consultation_subtype) { create :consultation_subtype, id: 42, consultation_type: consultation_type }
    let!(:new_consultation_reason) { create :consultation_reason, id: 42, consultation_type: consultation_type }
    let!(:new_calling_institution) { create :calling_institution, id: 42 }
    let!(:appointment) {
      create :appointment,
        id: 12,
        officer: officer,
        patient: patient,
        consultation_type: consultation_type,
        consultation_subtype: consultation_subtype,
        consultation_reason: consultation_reason,
        calling_institution: calling_institution
    }

    let(:appointment_data) {
      {
        "data": {
          "type": "appointments",
          "id": "12",
          "attributes": {
            "starts_at": "2018-05-21T07:30Z",
            "duration": "PT1H30M",
            "officer_name": "Commissaire Jules Maigret",
            "officer_phone": "0617181910",
            "officer_email": "jmaigret@filae.tld",
            "official_report": "223344",
            "patient_phone": "0716151413",
            "circumstances": nil,
            "certificate_questionnaire": {
              "pages": [
                {
                  "name": 42,
                  "elements": [
                    {
                      "name": "circumstances-ref",
                      "title": "Circonstances",
                      "description": "Ce qu'il s'est passé",
                      "type": "comment"
                    }
                  ]
                }
              ]
            },
            "certificate_data": {
              "circumstances-ref": "Sortie de discothèque"
            },   "comment": "Lorem Ipsum"
          },
          "relationships": {
            "consultation_type": {
              "data": {
                "type": "consultation_types",
                "id": "42"
              }
            },
            "consultation_subtype": {
              "data": {
                "type": "consultation_subtypes",
                "id": "42"
              }
            },
            "consultation_reason": {
              "data": {
                "type": "consultation_reasons",
                "id": "42"
              }
            },
            "calling_institution": {
              "data": {
                "type": "calling_institutions",
                "id": "42"
              }
            },
            "patient": {
              "data": {
                "type": "patients",
                "id": "42"
              }
            }
          }
        }
      }
    }

    it "updates the appointment attributes" do
      # Given
      expect {
        # When
        patch "/appointments/12", params: appointment_data.to_json, headers: headers
        # Then
      }.to_not change(Patient, :count)
      expect(response).to have_http_status(200)
      appointment.reload
      expect(appointment.comment).to eq "Lorem Ipsum"
      expect(appointment.starts_at.utc).to eq DateTime.new(2018,5,21,07,30).utc
      expect(appointment.duration.iso8601).to eq "PT1H30M"
      expect(appointment.patient_phone).to eq "0716151413"
    end

    it "updates the officer attributes" do
      # Given
      expect {
        # When
        patch "/appointments/12", params: appointment_data.to_json, headers: headers
        # Then
      }.to_not change(Officer, :count)
      expect(response).to have_http_status(200)
      appointment.reload
      officer = appointment.officer
      expect(officer.name).to eq "Commissaire Jules Maigret"
      expect(officer.phone).to eq "0617181910"
      expect(officer.email).to eq "jmaigret@filae.tld"
    end

    describe "regarding an existing patient" do
      it "changes the patient" do
        # Given
        appointment_data[:data][:relationships].merge!({
          "patient": {
            "data": {
              "type": "patients",
              "id": "49"
            }
          }
        })
        expect {
          # When
          patch "/appointments/12", params: appointment_data.to_json, headers: headers
          # Then
        }.to_not change(Patient, :count)
        expect(response).to have_http_status(200)
        expect(appointment.reload.patient.id).to eq 49
      end
    end

    describe "regarding a new patient" do
      it "creates her" do
        # Given
        appointment_data[:data][:relationships].delete(:patient)
        appointment_data[:data][:attributes].merge!({
          "patient_first_name": "Alex",
          "patient_last_name": "Belluci",
          "patient_birthdate": "1964-09-30",
          "patient_gender": "woman",
        })
        expect {
          # When
          patch "/appointments/12", params: appointment_data.to_json, headers: headers
          # Then
        }.to change(Patient, :count).by(1)
        expect(response).to have_http_status(200)
        expect(appointment.reload.patient.id).not_to eq 42
      end
    end

    it "updates the consultation type" do
      # Given
      # When
      patch "/appointments/12", params: appointment_data.to_json, headers: headers
      # Then
      expect(response).to have_http_status(200)
      appointment.reload
      expect(appointment.consultation_type).to eq new_consultation_type
    end

    it "updates the consultation subtype" do
      # Given
      # When
      patch "/appointments/12", params: appointment_data.to_json, headers: headers
      # Then
      expect(response).to have_http_status(200)
      appointment.reload
      expect(appointment.consultation_subtype).to eq new_consultation_subtype
    end

    it "updates the consultation reason" do
      # Given
      # When
      patch "/appointments/12", params: appointment_data.to_json, headers: headers
      # Then
      expect(response).to have_http_status(200)
      appointment.reload
      expect(appointment.consultation_reason).to eq new_consultation_reason
    end

    it "updates the calling institution" do
      # given
      # when
      patch "/appointments/12", params: appointment_data.to_json, headers: headers
      # then
      expect(response).to have_http_status(200)
      appointment.reload
      expect(appointment.calling_institution).to eq new_calling_institution
    end

    describe "regarding cancellation" do

      it "creates a 'cancelled' log job on cancellation" do
        # Given
        appointment.update_attribute(:cancelled_at, nil)
        appointment_data[:data][:attributes][:cancelled_at] = Time.zone.now
        appointment_data[:data][:attributes][:cancelled_by] = "umj"
        cancelled_action = Log::Action.new({ action: "cancelled", source: "umj" })
        expect {
          # When
          patch "/appointments/12", params: appointment_data.to_json, headers: headers
          # Then
        }.to have_enqueued_job(LogJob)
        compare_log_event_to([cancelled_action])
      end

      it "creates an 'uncancelled' log job on uncancellation" do
        # Given
        appointment.update_attribute(:cancelled_at, Time.zone.now)
        appointment_data[:data][:attributes][:cancelled_at] = nil
        uncancelled_action = Log::Action.new("uncancelled")
        expect {
          # When
          patch "/appointments/12", params: appointment_data.to_json, headers: headers
          # Then
        }.to have_enqueued_job(LogJob)
        compare_log_event_to([uncancelled_action])
      end
    end
  end
end
