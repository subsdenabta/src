# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "Patients", type: :request do
  before do
    connected_as_administrator
  end

  describe "GET /patients" do
    it "refuses to fetch all the patients" do
      # Given
      # When
      get '/patients', headers: headers
      # Then
      expect(response).to have_http_status(:bad_request)
      json_body = JSON.parse(response.body)
      expect(json_body["errors"].first["status"]).to eq 400
      expect(json_body["errors"].first["title"]).to eq 'Use pagination to fetch patients (max 100)'
    end

    it "returns paginated patients" do
      # Given
      10.times { create :patient }
      # When
      get '/patients?page[number]=1&page[size]=3', headers: headers
      # Then
      expect(response).to have_http_status(:success)
      json_body = JSON.parse(response.body)
      expect(json_body["data"].length).to eq 3
      expect(json_body["meta"]["total_pages"]).to eq 4
      expect(json_body["meta"]["total_patients"]).to eq 10
      expect(json_body["links"]["self"]).to eq "http://www.example.com/patients?page[number]=1&page[size]=3"
      expect(json_body["links"]["first"]).to eq "http://www.example.com/patients?page[number]=1&page[size]=3"
      expect(json_body["links"]["next"]).to eq "http://www.example.com/patients?page[number]=2&page[size]=3"
      expect(json_body["links"]["prev"]).to be_nil
      expect(json_body["links"]["last"]).to eq "http://www.example.com/patients?page[number]=4&page[size]=3"
    end
  end

  describe "PATCH /patients/:id" do
    let!(:patient1) { create :patient, id: 1 }
    let!(:patient2) { create :patient, id: 2 }
    let!(:appointment1) { create :appointment, id: 1, patient: patient1 }
    let!(:appointment2) { create :appointment, id: 2, patient: patient2 }

    before do
      patient2.update({
        duplicated_patient_id: nil,
        probably_duplicated_patient_id: patient1.id
      })
    end

    let(:patient_data) {
      {
        "data": {
          "type": "patients",
          "id": patient2.id,
          "attributes": {
            "last_name": nil,
            "first_name": nil,
            "gender": nil,
            "birthdate": nil,
            "phone_numbers": nil
          },
          "relationships": {
            "duplicated_patient": {
              "data": {
                "id": "1",
                "type": "patients"
              }
            },
            "probably_duplicated_patient": {
              "data": nil
            }
          }
        }
      }
    }

    describe "merging both" do
      it "updates the last one" do
        # Given
        # When
        patch "/patients/2", params: patient_data.to_json, headers: headers
        # Then
        expect(patient1.reload.duplicated_patient_id).to be nil
        expect(patient1.probably_duplicated_patient_id).to be nil
        expect(patient2.reload.duplicated_patient_id).to eq patient1.id
        expect(patient2.probably_duplicated_patient_id).to be nil
      end

      it 'updates the appointment associated to the last one' do
        # Given
        # When
        patch "/patients/2", params: patient_data.to_json, headers: headers
        # Then
        expect(appointment1.reload.patient).to eq patient1
        expect(appointment2.reload.patient).to eq patient1
        expect(appointment2.original_patient_id).to eq patient2.id
      end
    end
  end
end
