# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

describe "Password requests", type: :request do

  describe "POST /password_requests" do

    let(:request_data) {
      {
        "data": {
          "type": "password_requests",
          "attributes": {
            "identification": "scatterbrain"
          }
        }
      }
    }

    describe "when the identifier exists" do

      let!(:user) { create :user, identifier: "scatterbrain"}

      it "resets the user's password" do
        # Given
        old_password_digest = user.password_digest
        # When
        post "/password_requests", params: request_data.to_json, headers: visitor_headers
        # Then
        expect(old_password_digest).not_to eq user.reload.password_digest
      end

      it "sends an email" do
        # Given
        expect {
          # When
          post "/password_requests", params: request_data.to_json, headers: visitor_headers
          # Then
        }.to change(ActionMailer::Base.deliveries, :count).by(1)
      end

      describe "when email sending fails" do

        before do
          allow(PasswordMailer).to receive_message_chain("reset.deliver").and_raise("SMTP_ERROR")
        end

        it "returns 502 Bad Gateway" do
          # Given
          expect {
            # When
            post "/password_requests", params: request_data.to_json, headers: visitor_headers
            # Then
          }.to_not change(ActionMailer::Base.deliveries, :count)
          expect(response).to have_http_status(502)
        end
      end
    end


    describe "when the user does not exist" do

      it "returns 404 error" do
        # Given
        # When
        post "/password_requests", params: request_data.to_json, headers: visitor_headers
        # Then
          expect(response).to have_http_status(404)
      end
    end
  end
end
