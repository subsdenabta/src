# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe DisableUserJob, type: :job do

  it "revokes all acces tokens of a user" do
    # Given
    concerned_user = create :user
    3.times { Doorkeeper::AccessToken.create!(resource_owner_id: concerned_user.id) }
    # When
    DisableUserJob.perform_now(concerned_user)
    user_tokens = Doorkeeper::AccessToken.where(resource_owner_id: concerned_user.id)
    # Then
    expect(user_tokens.count).to eq 3
    user_tokens.each do |token|
      expect(token).to be_revoked
    end
  end
end
