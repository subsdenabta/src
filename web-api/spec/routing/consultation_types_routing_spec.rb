# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe ConsultationTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/intervention_domains/42/consultation_types").to route_to("consultation_types#index", intervention_domain_id: "42")
    end


    it "routes to #show" do
      expect(:get => "/consultation_types/1").to route_to("consultation_types#show", id: "1")
    end


    it "routes to #create" do
      expect(:post => "/intervention_domains/42/consultation_types").to route_to("consultation_types#create", intervention_domain_id: "42")
    end

    it "routes to #create" do
      expect(:post => "/consultation_types").to route_to("consultation_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/consultation_types/1").to route_to("consultation_types#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/consultation_types/1").to route_to("consultation_types#update", id: "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/consultation_types/1").to route_to("consultation_types#destroy", id: "1")
    end

  end
end
