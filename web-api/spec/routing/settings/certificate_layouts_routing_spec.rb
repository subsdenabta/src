# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require "rails_helper"

RSpec.describe Settings::CertificateLayoutsController, type: :routing do
  describe "routing" do

    it "routes to #show" do
      expect(:get => "/settings/certificate_layout").to route_to("settings/certificate_layouts#show")
    end

    it "routes to #update via PUT" do
      expect(:put => "/settings/certificate_layout").to route_to("settings/certificate_layouts#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/settings/certificate_layout").to route_to("settings/certificate_layouts#update")
    end

  end
end
