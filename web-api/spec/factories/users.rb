# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
FactoryBot.define do
  factory :user do
    identifier { Faker::Internet.unique.user_name }
    email { Faker::Internet.email(name: identifier) }
    first_name { identifier.titleize }
    last_name { Faker::Name.last_name }
    password { Faker::Internet.password }
    role { Role.find_or_create_by(title: "unknown") }
    disabled_at { nil }

    trait :invalid do
      identifier { "" }
    end

    trait :inactive do
      disabled_at { 30.minutes.ago }
    end

    trait :scheduled_for_deactivation do
      disabled_at { Time.zone.now + 30.minutes }
    end

    Role::ALL.each do |role_title|
      trait role_title do
        role { Role.find_by(title: role_title) }
      end
    end
  end
end
