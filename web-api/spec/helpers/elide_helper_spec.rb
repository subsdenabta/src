# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ElideHelper. For example:
#
# describe ElideHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ElideHelper, type: :helper do

  describe "elide" do
    it "removes the trailing character before a vowel" do
      # Given
      vowels = %w[a e é è E É È i o u y]
      # When
      results = vowels.map { |vowel| helper.elide('de', vowel) }
      # Then
      expect(results).to all(start_with("d'"))
    end
  end

  describe "elide" do
    it "adds a space before non-vowels" do
      # Given
      consonants = %w[b c d f]
      # When
      results = consonants.map { |consonant| helper.elide('de', consonant) }
      # Then
      expect(results).to all(start_with('de '))
    end
  end

end
