# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe PdfHelper, type: :helper do

  describe "check_or_warn" do

    it "returns 'À RENSEIGNER' when nil provided" do
      # Given
      # When
      result = helper.check_or_warn(nil)
      # Then
      expect(result).to eq "<span class=\"field-to-provide\">À RENSEIGNER</span>"
    end

    it "returns 'À RENSEIGNER' when nil provided" do
      # Given
      # When
      result = helper.check_or_warn('')
      # Then
      expect(result).to eq "<span class=\"field-to-provide\">À RENSEIGNER</span>"
    end

    describe "when correct value provided" do
      it "returns the value if no block given" do
        # Given
        value = 'Some text'
        # When
        result = helper.check_or_warn(value)
        # Then
        expect(result).to eq value
      end

      it "returns the result of the given block" do
        # Given
        value = Date.new(2019,6,21)
        I18n.locale = 'fr'
        # When
        result = helper.check_or_warn(value) { |v| I18n.l v }
        # Then
        expect(result).to eq "21/06/2019"
        I18n.locale = I18n.default_locale
      end
    end
  end

  describe "print_multiple_choices" do

    describe "regarding simple choices" do

      let(:choices) { [ "un", "deux", "trois" ] }

      describe "without other value" do
        it "joins the provided values" do
          # Given
          data = ["un", "trois"]
          # When
          result = helper.print_multiple_choices(choices, data)
          # Then
          expect(result).to eq "un, trois"
        end
      end

      describe "with other value" do
        it "joins all the values" do
          # Given
          data = ["un", "trois", "other"]
          questionnaire_data = {"position-Comment" => "quatre"}
          # When
          result = helper.print_multiple_choices(choices, data, questionnaire_data["position-Comment"])
          # Then
          expect(result).to eq "un, trois, quatre"
        end
      end
    end

    describe "regarding complex choices" do

      let(:choices) {
        [
          {
            "value" => "one",
            "text" => "un"
          },
          {
            "value" => "two",
            "text" => "deux"
          },
          {
            "value" => "three",
            "text" => "trois"
          }
        ]
      }

      describe "without other value" do
        it "joins the provided values" do
          # Given
          data = ["one", "three"]
          # When
          result = helper.print_multiple_choices(choices, data)
          # Then
          expect(result).to eq "un, trois"
        end
      end

      describe "with other value" do
        it "joins all the values" do
          # Given
          data = ["one", "three", "other"]
          questionnaire_data = {"position-Comment" => "quatre"}
          # When
          result = helper.print_multiple_choices(choices, data, questionnaire_data["position-Comment"])
          # Then
          expect(result).to eq "un, trois, quatre"
        end
      end
    end
  end

  describe "print_choice" do

    describe "regarding simple choice" do

      let(:choices) { [ "un", "deux", "trois" ] }

      describe "without other value" do
        it "returns the selected value" do
          # Given
          data = "deux"
          # When
          result = helper.print_choice(choices, data)
          # Then
          expect(result).to eq "deux"
        end
      end

      describe "with other value" do
        it "returns the provided value" do
          # Given
          data = "other"
          questionnaire_data = {"position-Comment" => "quatre"}
          # When
          result = helper.print_choice(choices, data, questionnaire_data["position-Comment"])
          # Then
          expect(result).to eq "quatre"
        end
      end
    end

    describe "regarding complex choices" do

      let(:choices) {
        [
          {
            "value" => "one",
            "text" => "un"
          },
          {
            "value" => "two",
            "text" => "deux"
          },
          {
            "value" => "three",
            "text" => "trois"
          }
        ]
      }

      describe "without other value" do
        it "returns the selected value" do
          # Given
          data = "two"
          # When
          result = helper.print_choice(choices, data)
          # Then
          expect(result).to eq "deux"
        end
      end

      describe "with other value" do
        it "returns the provided value" do
          # Given
          data = "other"
          questionnaire_data = {"position-Comment" => "quatre"}
          # When
          result = helper.print_choice(choices, data, questionnaire_data["position-Comment"])
          # Then
          expect(result).to eq "quatre"
        end
      end
    end
  end

  describe "format_expression" do

    it "a date is localized" do
      # Given
      data = "2019-06-14"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq "14/06/2019"
    end

    it "a path is returned" do
      # Given
      data = "path/to/me"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq data
    end

    it "any other data is returned" do
      # Given
      data = "any common text"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq data
    end

    it "a number returns the string" do
      # Given
      data = 42
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq "42"
    end
  end

  describe "print_file" do
    it "prints an image" do
      # Given
      title = "Titre"
      file = {
        "name" => "Image 1px.png",
        "type" => "image/png",
        "content" => "data =>image/png;base64,code=="
      }
      expected = "<figure><figcaption>Titre</figcaption><img alt=\"Titre\" src=\"/images/data =&gt;image/png;base64,code==\" /></figure>"
      # When
      result = helper.print_file(file, title)
      # Then
      expect(result).to eq expected
    end

    it "prints the content of the file" do
      # Given
      title = "Titre"
      file = {
        "name" => "fichier.txt",
        "type" => "text/plain",
        "content" => "data:text/plain;base64,Vm9pY2kgZHUgdGV4dGUKYXZlYyByZXRvdXIgw6AgbGEgbGlnbmUhCg=="
      }
      expected = "<dl><dt>Titre</dt><dd>fichier.txt</dd></dl>"
      # When
      result = helper.print_file(file, title)
      # Then
      expect(result).to eq expected
    end
  end

  describe "format_expression" do

    it "a date is localized" do
      # Given
      data = "2019-06-14"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq "14/06/2019"
    end

    it "a path is returned" do
      # Given
      data = "path/to/me"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq data
    end

    it "any other data is returned" do
      # Given
      data = "any common text"
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq data
    end

    it "a number returns the string" do
      # Given
      data = 42
      # When
      result = helper.format_expression(data)
      # Then
      expect(result).to eq "42"
    end
  end
end
