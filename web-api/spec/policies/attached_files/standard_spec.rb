# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user without medical confidentiality role regarding attached files" do |role|
  context "user has access to calendar" do
    before { allow(concerned_item).to receive(:access_allowed_for_user?).with(user).and_return(true) }

    it { is_expected.to forbid_actions([:create, :destroy, :download]) }
    it { is_expected.to permit_actions([:show]) }
  end

  context "user does not have access to calendar" do
    it { is_expected.to forbid_actions([:show, :create, :destroy, :download]) }
  end
end

RSpec.shared_examples "a connected user with medical confidentiality role regarding attached files" do |role|
  context "user has access to calendar" do
    before { allow(concerned_item).to receive(:access_allowed_for_user?).with(user).and_return(true) }

    context "user can take in charge" do
      before { allow(concerned_item).to receive(:allowed_for_user?).with(user).and_return(true) }

      context "if she has the concerned_item in charge" do
        before { concerned_item.update_attribute(:user_in_charge, user)}

        it { is_expected.to permit_actions([:show, :create, :destroy, :download]) }
      end

      context "if she does not have the concerned_item in charge" do
        it { is_expected.to forbid_actions([:destroy]) }
        it { is_expected.to permit_actions([:show, :create, :download]) }
      end

    end

    context "user cannot take in charge" do
      before { allow(concerned_item).to receive(:allowed_for_user?).with(user).and_return(false) }

      it { is_expected.to forbid_actions([:create, :destroy]) }
      it { is_expected.to permit_actions([:show, :download]) }
    end
  end

  context "user does not have access to calendar" do
    before { allow(concerned_item).to receive(:access_allowed_for_user?).with(user).and_return(false) }

    it { is_expected.to forbid_actions([:show, :create, :destroy, :download]) }
  end
end

RSpec.shared_examples "a standard attachment" do

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:show, :create, :destroy, :download]) }
  end

  [
    :administrator,
    :nurse,
    :secretary
  ].each do |role|
    context "as a #{role}" do
      let(:user) { create :user, role }

      it { is_expected.to permit_actions([:show, :create, :destroy, :download]) }
    end
  end

  [
    :association,
    :switchboard_operator
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user without medical confidentiality role regarding attached files" do
        let(:user) { create :user, role }
      end
    end
  end

  [
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user with medical confidentiality role regarding attached files" do
        let(:user) { create :user, role }
      end
    end
  end
end

describe AttachedFile::StandardPolicy do
  subject { described_class.new(user, attached_file) }

  context "regarding appointments" do
    let(:concerned_item) { create :appointment, :with_certificate }
    let(:attached_file) { create :attached_file, attachable: concerned_item.certificate }

    it_behaves_like "a standard attachment"
  end

  context "regarding flowing consultations" do
    let(:concerned_item) { create :flowing_consultation, :with_certificate }
    let(:attached_file) { create :attached_file, attachable: concerned_item.certificates.first }

    it_behaves_like "a standard attachment"
  end
end
