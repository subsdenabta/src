# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding printed signature" do |role|
  context "another user" do
    let(:attached_file) { create :attached_file, attachable: (create :user) }

    it { is_expected.to forbid_actions([:show, :download, :destroy]) }
  end

  context "herself" do
    let(:attached_file) { create :attached_file, attachable: user }

    it { is_expected.to permit_actions([:show, :download, :create, :destroy]) }
  end
end

describe AttachedFile::PrintedSignaturePolicy do

  subject { described_class.new(user, attached_file) }

  context "as a visitor" do

    let(:user) { Visitor.new }
    let(:attached_file) { nil }

    it { is_expected.to forbid_actions([:show, :download, :create, :destroy]) }
  end

  [
    :administrator,
    :association,
    :nurse,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :secretary,
    :switchboard_operator
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user regarding printed signature" do
        let(:user) { create :user, role }
      end
    end
  end
end
