# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding certificates" do |role|
  describe "actions" do
    it { is_expected.to permit_actions([:show]) }
    it { is_expected.to forbid_actions([:update, :destroy, :preview, :send_by_mail]) }
  end

  describe "permitted attributes" do
    it "does not allow any change" do
      expect(subject.permitted_attributes).to be_empty
    end
  end
end

RSpec.shared_examples "a user in charge regarding certificates" do |role|
  describe "actions" do
    it { is_expected.to permit_actions([:show, :update, :destroy, :preview, :send_by_mail]) }
  end

  describe "permitted attributes" do
    it "allows any change" do
      expect(subject.permitted_attributes).to eq [ :validated_at, :validator_id, :delivered_at, :questionnaire, :data, questionnaire: {}, data: {} ]
    end
  end
end

describe CertificatePolicy do
  subject { described_class.new(user, certificate) }

  let(:certificate) { create :certificate }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:show, :update, :destroy, :preview, :send_by_mail]) }
  end

  context "as an administrator" do
    let(:user) { create :user, :administrator }

    it_behaves_like "a user in charge regarding certificates"
  end

  context "as the user in charge" do
    let(:user) { create :user, :physician }
    before { create :appointment, user_in_charge: user, certificate: certificate}

    it_behaves_like "a user in charge regarding certificates"
  end

  [
    :association,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :switchboard_operator
  ].each do |role_title|
    context "as a(n) #{role_title}" do
      it_behaves_like "a connected user regarding certificates" do
        let(:user) { create :user, role_title }
      end
    end
  end

  [
    :nurse,
    :secretary,
  ].each do |role_title|
    context "as a(n) #{role_title}" do
      let(:user) { create :user, role_title }

      describe "actions" do
        it { is_expected.to forbid_actions([:destroy, :preview]) }
        it { is_expected.to permit_actions([:show, :update, :send_by_mail]) }
      end

      describe "permitted attributes" do
        it "allows indicating the certificate has been sent" do
          expect(subject.permitted_attributes).to eq [:validated_at, :validator_id, :delivered_at]
        end
      end
    end
  end
end
