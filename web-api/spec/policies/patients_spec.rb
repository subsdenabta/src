# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a person with medical confidentiality regarding patients" do |role|
  it { is_expected.to permit_actions(%i[index show update]) }

  it "is expected to perform any chnages" do
    expected = %i[
      birth_year
      birthdate
      duplicated_patient_id
      first_name
      gender
      last_name
      probably_duplicated_patient_id
    ]
    expect(subject.permitted_attributes).to match_array expected
  end
end

RSpec.shared_examples "a person without medical confidentiality regarding patients" do |role|
  it { is_expected.to permit_actions(%i[show]) }
  it { is_expected.to forbid_actions(%i[index update]) }

  it "is NOT expected to perform any chnages" do
    expect(subject.permitted_attributes).to be_empty
  end
end

describe PatientPolicy do
  subject { described_class.new(user, patient) }

  let(:patient) { create :patient }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:index]) }
  end

  [
    :administrator,
    :nurse,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :secretary
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a person with medical confidentiality regarding patients" do
        let(:user) { create :user, role }
      end
    end
  end

  [
    :association,
    :switchboard_operator
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a person without medical confidentiality regarding patients" do
        let(:user) { create :user, role }
      end
    end
  end
end
