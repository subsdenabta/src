# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding duty periods" do |role|
  it { is_expected.to permit_actions([:index, :show]) }
  it { is_expected.to forbid_actions([:update]) }
end

describe DutyPeriodPolicy do
  subject { described_class.new(user, duty_period) }

  let(:duty_period) { create :duty_period }

  context "as a visitor" do
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:index, :show]) }
  end

  [
    :association,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :switchboard_operator
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user regarding duty periods" do
        let(:user) { create :user, role }
      end
    end
  end

  [
    :administrator,
    :nurse,
    :secretary
  ].each do |role|
    context "as a #{role}" do
      let(:user) { create :user, role }

      it { is_expected.to permit_actions([:index, :show, :update]) }
    end
  end

  describe 'for assignable user' do
    let(:assigned_role) { :physician }
    let(:consultation_type) { create :consultation_type, assigned_roles: [Role.find_by(title: assigned_role)] }
    let(:duty_period) { create :duty_period, consultation_type: consultation_type }

    context "as a user with assigned role" do
      let(:user) { create :user, assigned_role }

      it { is_expected.to permit_actions([:index, :show, :update]) }
    end

  end
end
