# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding users" do |role|
  context "regarding another user" do
    let(:concerned_user) { create :user }

    it { is_expected.to permit_actions([:index, :show]) }
    it { is_expected.to forbid_actions([:create, :update, :destroy]) }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end
  end

  context "regarding herself" do
    let(:concerned_user) { user }

    it { is_expected.to permit_actions([:show, :update]) }
    it { is_expected.to forbid_actions([:create, :destroy]) }

    it "permits some attributes" do
      expect(subject.permitted_attributes).to eq [:password]
    end
  end
end

describe UserPolicy do
  subject { described_class.new(user, concerned_user) }


  context "as a visitor" do
    let(:concerned_user) { create :user }
    let(:user) { Visitor.new }

    it { is_expected.to forbid_actions([:index, :show, :create, :update, :destroy]) }

    it "permits no attribute" do
      expect(subject.permitted_attributes).to be_empty
    end
  end

  context "as an admin" do
    let(:user) { create :user, :administrator }

    context "regarding another user" do
      let(:concerned_user) { create :user }

      it { is_expected.to permit_actions([:index, :show, :create, :update]) }
      it { is_expected.to forbid_action(:destroy) }

      it "permits main attributes" do
        expect(subject.permitted_attributes).to eq [:identifier, :email, :first_name, :last_name, :disabled_at, :role_id]
      end
    end

    context "regarding herself" do
      let(:concerned_user) { user }

      it { is_expected.to permit_actions([:index, :show, :create, :update]) }
      it { is_expected.to forbid_action(:destroy) }

      it "permits all attributes" do
        expect(subject.permitted_attributes).to eq [:identifier, :email, :first_name, :last_name, :disabled_at, :role_id, :password]
      end
    end

  end

  [
    :association,
    :nurse,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :secretary,
    :switchboard_operator
  ].each do |role|
    context "as a #{role}" do
      it_behaves_like "a connected user regarding users" do
        let(:user) { create :user, role }
      end
    end
  end
end
