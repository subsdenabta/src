# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding certificate templates for actions" do |role|
  it { is_expected.to permit_actions([:index, :show]) }
  it { is_expected.to forbid_actions([:create, :update]) }
end

RSpec.shared_examples "a connected user regarding certificate templates for permitted attributes" do |role|
  it "does not allow any change" do
    expect(subject.permitted_attributes).to be_empty
  end
end

describe CertificateTemplatePolicy do
  subject { described_class.new(user, certificate_template) }

  let(:certificate_template) { create :certificate_template }

  context "regarding actions" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it { is_expected.to forbid_actions([:index, :show, :create, :update]) }
    end

    context "as an admin" do
      let(:user) { create :user, :administrator }

      it { is_expected.to permit_actions([:index, :show, :create, :update]) }
    end

    [
      :association,
      :nurse,
      :physician,
      :psychiatrist,
      :psychiatrist_expert,
      :psychologist,
      :psychologist_expert,
      :secretary,
      :switchboard_operator
    ].each do |role|
      context "as a #{role}" do
        it_behaves_like "a connected user regarding certificate templates for actions" do
          let(:user) { create :user, role }
        end
      end
    end
  end

  describe "regarding permitted attributes" do
    context "as a visitor" do
      it_behaves_like "a connected user regarding certificate templates for permitted attributes" do
        let(:user) { Visitor.new }
      end
    end

    context "as an administrator" do
      let(:user) { create :user, :administrator }

      it "allow any change" do
        expect(subject.permitted_attributes).to eq [:title, :disabled_at, :questionnaire, role_ids: [], questionnaire: {}]
      end
    end

    [
      :association,
      :nurse,
      :physician,
      :psychiatrist,
      :psychiatrist_expert,
      :psychologist,
      :psychologist_expert,
      :secretary,
      :switchboard_operator
    ].each do |role_title|
      context "as a(n) #{role_title}" do
        it_behaves_like "a connected user regarding certificate templates for permitted attributes" do
          let(:user) { create :user, role_title }
        end
      end
    end
  end
end
