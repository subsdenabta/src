# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.shared_examples "a connected user regarding appointments" do |role|
  it { is_expected.to permit_actions([:index, :show]) }
  it { is_expected.to forbid_actions([:create, :update]) }
end

describe AppointmentPolicy do
  subject { described_class.new(user, appointment) }

  let(:appointment) { create :appointment }

  describe "permitted actions" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it { is_expected.to forbid_actions([:index, :show, :create, :update]) }
    end

    [
      :administrator,
      :association,
      :nurse,
      :physician,
      :psychiatrist,
      :psychologist,
      :psychologist_expert,
      :secretary
    ].each do |role|
      context "as a(n) #{role}" do
        let(:user) { create :user, role }

        it { is_expected.to permit_actions([:index, :show, :create, :update]) }
      end
    end

    [
      :psychiatrist_expert,
      :switchboard_operator
    ].each do |role|
      context "as a #{role}" do
        it_behaves_like "a connected user regarding appointments" do
          let(:user) { create :user, role }
        end
      end
    end
  end

  describe "permitted attributes" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it "does not allow any change" do
        expect(subject.permitted_attributes).to be_empty
      end
    end


    context "users allowed to assign themselves" do

      let(:consultation_type) { create :consultation_type, assigned_roles: [user.role] }

      [
        :administrator,
        :association,
        :nurse,
        :physician,
        :psychiatrist,
        :psychologist,
        :secretary
      ].each do |role_title|
        context "as a(n) #{role_title}" do
          let(:user) { create :user, role_title }

          it "allows edition and assignment" do
            appointment.consultation_type = consultation_type
            appointment.save!
            expected = [
              :id, :user_in_charge_id, :starts_at, :comment, :circumstances, :official_report,
              :duration, :cancelled_at, :patient_arrived_at, :patient_released_at, :patient_phone,
              :cancelled_by, :emergency, :patient_missing, :requisition_received_at,
              :consultation_type_id, :consultation_subtype_id, :consultation_reason_id,
              :calling_institution_id, :previous_appointment_id, :originating_appointment_id,
              :patient_id
            ]
            expect(subject.permitted_attributes).to match_array expected
          end
        end
      end

      context "as a user with another role" do

        let(:user) { create :user, :psychiatrist_expert }

        it "allows only assignment" do
          appointment.consultation_type = consultation_type
          appointment.save!
          expected = [ :id, :user_in_charge_id ]
          expect(subject.permitted_attributes).to match_array expected
        end
      end
    end

    context "users NOT allowed to assign themseleves" do
      [
        :administrator,
        :association,
        :nurse,
        :physician,
        :psychiatrist,
        :psychologist,
        :secretary
      ].each do |role_title|
        context "as a(n) #{role_title}" do
          let(:user) { create :user, role_title }

          it "allows only edition" do
            expected = [
              :id, :starts_at, :comment, :circumstances, :official_report,
              :duration, :cancelled_at, :patient_arrived_at, :patient_released_at, :patient_phone,
              :cancelled_by, :emergency, :patient_missing, :requisition_received_at,
              :consultation_type_id, :consultation_subtype_id, :consultation_reason_id,
              :calling_institution_id, :previous_appointment_id, :originating_appointment_id,
              :patient_id
            ]
            expect(subject.permitted_attributes).to match_array expected
          end
        end
      end

      context "as a user with another role" do

        let(:user) { create :user, :psychiatrist_expert }

        it "discards any change" do
          expected = []
          expect(subject.permitted_attributes).to match_array expected
        end
      end
    end
  end
end
