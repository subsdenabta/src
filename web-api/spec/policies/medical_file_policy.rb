# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

describe MedicalFilePolicy do
  subject { described_class.new(user, medical_file) }

  let(:medical_file) { create :medical_file }

  context "regarding actions" do
    context "as a visitor" do
      let(:user) { Visitor.new }

      it { is_expected.to forbid_actions([:show, :update]) }
    end

    [
      :administrator,
      :nurse,
      :physician,
      :psychiatrist,
      :psychiatrist_expert,
      :psychologist,
      :psychologist_expert,
      :secretary
    ].each do |role|
      context "as a #{role}" do

        let(:user) { create :user, role }

        it { is_expected.to permit_actions([:show, :update]) }
      end
    end

    [
      :association,
      :switchboard_operator
    ].each do |role|
      context "as a #{role}" do

        let(:user) { create :user, role }

        it { is_expected.to forbid_actions([:show, :update]) }
      end
    end
  end
end
