# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe Schedule, type: :model do

  subject { build(:schedule) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:activated_on) }

  it ".on returns the schedule with passed date and nearest to it" do
    # Given
    older_schedule = create :schedule, activated_on: "2018-05-02"
    nearest_schedule = create :schedule, activated_on: "2018-05-19"
    future_schedule = create :schedule, activated_on: "2018-05-30"
    date = Date.new(2018,5,21)
    # When
    result = Schedule.on(date)
    # Then
    expect(result).to eq [nearest_schedule]
  end

  it ".for_consultation_type returns the schedules concerning a consultation type" do
    # Given
    domain = create :intervention_domain
    consultation_type = create :consultation_type, intervention_domain: domain
    another_domain = create :intervention_domain
    schedule_1 = create :schedule, intervention_domain: domain
    schedule_2 = create :schedule, intervention_domain: domain
    schedule_3 = create :schedule, intervention_domain: another_domain
    # When
    result = Schedule.for_consultation_type(consultation_type)
    # Then
    expect(result).to eq [schedule_1, schedule_2]
  end

  describe ".duty_periods_for" do

    let(:domain) { create :intervention_domain}
    let(:concerned_consultation_type) { create :consultation_type, intervention_domain: domain }
    let(:another_consultation_type) { create :consultation_type, intervention_domain: domain }
    let(:monday) { Date.new(2018,5,21) }
    let(:older_schedule) { create :schedule, intervention_domain: domain, activated_on: "2018-05-02"  }
    let(:nearest_schedule) { create :schedule, intervention_domain: domain, activated_on: "2018-05-19"  }
    let(:future_schedule) { create :schedule, intervention_domain: domain, activated_on: "2018-05-30"  }

    describe "no time slot exists" do

      it "returns an empty array" do
        # Given
        # When
        result = Schedule.duty_periods_for(concerned_consultation_type, monday)
        # Then
        expect(result).to be_empty
      end

    end

    describe "time slots exist" do

      before do
        2.times do
          concerned_consultation_type.time_slots << create(:time_slot, week_day: 1, schedule: nearest_schedule)
        end
        1.times do
          concerned_consultation_type.time_slots << create(:time_slot, week_day: 3, schedule: nearest_schedule)
          concerned_consultation_type.time_slots << create(:time_slot, week_day: 1, schedule: older_schedule)
          concerned_consultation_type.time_slots << create(:time_slot, week_day: 1, schedule: future_schedule)
          another_consultation_type.time_slots << create(:time_slot, week_day: 1, schedule: nearest_schedule)
        end
      end

      it "returns the corresponding duty periods" do
        # Given
        # When
        result = Schedule.duty_periods_for(concerned_consultation_type, monday)
        # Then
        expect(result.length).to eq 2
      end

      describe "a time slot is added" do

        before { concerned_consultation_type.time_slots << create(:time_slot, week_day: 1, schedule: nearest_schedule) }

        it "also returns the corresponding duty period" do
          # Given
          # When
          result = Schedule.duty_periods_for(concerned_consultation_type, monday)
          # Then
          expect(result.length).to eq 3
        end

      end

      describe "a time slot is deleted" do

        before { concerned_consultation_type.time_slots.first.destroy }

        it "does not return the corresponding duty period" do
          # Given
          # When
          result = Schedule.duty_periods_for(concerned_consultation_type, monday)
          # Then
          expect(result.length).to eq 1
        end
      end

    end

  end
end
