# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe Appointment, type: :model do

  subject { build :appointment }

  it { is_expected.to be_valid }

  describe 'associations' do

    it { is_expected.to have_one(:intervention_domain) }
    it { is_expected.to have_one(:medical_file) }
  end

  it 'returns appointments by ascending starts_at field by default' do
    # Given
    app1 = create :appointment, starts_at: Time.zone.now
    app2 = create :appointment, starts_at: Time.zone.now - 1.hour
    # When
    result = Appointment.all
    # Then
    expect(result).to eq [app2, app1]
  end

  it ".of_consultation_type returns only appointments associeated to a given consultation type" do
    # Given
    ct1 = create :consultation_type
    ct2 = create :consultation_type
    appointment = create :appointment, consultation_type: ct1
    create :appointment, consultation_type: ct2
    # When
    result = Appointment.of_consultation_type(ct1)
    # Then
    expect(result).to eq [appointment]
  end

  it ".scheduled_on returns the appointments of a specific day" do
    # Given
    appointment1 = create :appointment, starts_at: "2018-05-21T00:00Z"
    appointment2 = create :appointment, starts_at: "2018-05-21T23:59"
    appointment3 = create :appointment, starts_at: "2018-05-22T00:00Z"
    target_day = Date.parse("2018-05-21")
    # When
    result = Appointment.scheduled_on(target_day)
    # Then
    expect(result).to eq [appointment1, appointment2]
  end

  describe '.generate_onml_id' do

    it 'creates the sequence on first call and assign an onml id' do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: 'I')
      subject.starts_at = Time.current.change(year: 2018, month: 05)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq 'I18051'
    end

    it 'skips if the intenvention domain has no identifier prefix' do
      # Given
      subject.intervention_domain = create(:intervention_domain, identifier_prefix: nil)
      # When
      subject.save
      # Then
      expect(subject.onml_id).to be_nil
    end

    it 'skips if onml_id is already set' do
      # Given
      subject.onml_id = ''
      # When
      subject.save
      # Then
      expect(subject.onml_id).to eq ''
    end

    it 'uses the same sequence on same month' do
      # Given
      month = 5
      same_month = create(:appointment, intervention_domain: create(:intervention_domain, identifier_prefix: 'I'), starts_at: Time.current.change(year: 2018, month: month))
      subject.intervention_domain = same_month.intervention_domain
      subject.starts_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(same_month.onml_id).to eq 'I18051'
      expect(subject.onml_id).to eq 'I18052'
    end

    it 'uses another sequence on other month' do
      # Given
      other_month, month = [7, 8]
      other_month = create(:appointment, intervention_domain: create(:intervention_domain, identifier_prefix: 'I'), starts_at: Time.current.change(year: 2018, month: other_month))
      subject.intervention_domain = other_month.intervention_domain
      subject.starts_at = Time.current.change(year: 2018, month: month)
      # When
      subject.save
      # Then
      expect(other_month.onml_id).to eq 'I18071'
      expect(subject.onml_id).to eq 'I18081'
    end

    it 'uses another sequence on other intervention domain identifier prefix' do
      # Given
      starts_at = Time.current.change(year: 2018, month: 5)
      other_intervention_domain = create(:intervention_domain, identifier_prefix: 'O');
      intervention_domain = create(:intervention_domain, identifier_prefix: 'I');
      same_identifier_prefix = create(:intervention_domain, identifier_prefix: 'I');

      with_other_domain = create(:appointment, intervention_domain: other_intervention_domain, starts_at: starts_at)
      with_same_prefix = create(:appointment, intervention_domain: same_identifier_prefix, starts_at: starts_at)
      subject.intervention_domain = intervention_domain
      subject.starts_at = starts_at
      # When
      subject.save
      # Then
      expect(with_other_domain.onml_id).to eq 'O18051'
      expect(with_same_prefix.onml_id).to eq 'I18051'
      expect(subject.onml_id).to eq 'I18052'
    end
  end

  describe "#postpone" do

    it 'cancels appointment and sets origin' do
      # Given
      appointment = create :appointment
      # When
      appointment.postpone('patient')
      # Then
      expect(appointment.cancelled_at).not_to be_nil
      expect(appointment.cancelled_by).to eq 'patient'
    end

    it "copies former attached files to new appointment" do
      # Given
      appointment = create :appointment
      2.times { create(:attached_file, attachable: appointment) }
      next_appointment = create :appointment, previous_appointment: appointment
      # When
      appointment.postpone('patient')
      # Then
      expect(next_appointment.attached_files.count).to eq appointment.attached_files.count
      previous_file = appointment.attached_files.first.file
      next_file = next_appointment.attached_files.first.file
      expect(next_file.attachment).not_to be_nil
      expect(next_file.attachment).not_to eq previous_file.attachment
      expect(next_file.blob).to eq previous_file.blob
      expect(next_file.filename).to eq previous_file.filename
    end
  end

  describe ".between" do

    let!(:a1) { create :appointment, starts_at: "13/12/2018 08:00:00"}
    let!(:a2) { create :appointment, starts_at: "14/12/2018 08:00:00"}
    let!(:a3) { create :appointment, starts_at: "15/12/2018 08:00:00"}
    let!(:a4) { create :appointment, starts_at: "18/12/2018 08:00:00"}
    let!(:a5) { create :appointment, starts_at: "19/12/2018 08:00:00"}

    it "[2018-12-14, nil] returns appointments from 14/12/2018" do
      expect(Appointment.between("2018-12-14", nil)).to eq [a2, a3, a4, a5]
    end

    it "[nil, 2018-12-18] returns appointments until 18/12/2018 included" do
      expect(Appointment.between(nil, "2018-12-18")).to eq [a1, a2, a3, a4]
    end

    it "[nil, nil] returns all the appointments" do
      expect(Appointment.between(nil, nil)).to eq [a1, a2, a3, a4, a5]
    end

    it "[2018-12-14, 2018-12-18] returns all appointments between 14/12/2018 and 18/12/2018 included" do
      expect(Appointment.between("2018-12-14", "2018-12-18")).to eq [a2, a3, a4]
    end
  end

  it ".onml_id_like returns appointments with ONML id matching %String% SQL patterns" do
    # Given
    a1 = create :appointment, onml_id: "C181142"
    a2 = create :appointment, onml_id: "C18121"
    a3 = create :appointment, onml_id: "C18122"
    # When
    result = Appointment.onml_id_like('c1812')
    # Then
    expect(result).to eq [a2, a3]
  end

  it ".official_report_like returns appointments with official report matching %String% SQL patterns" do
    # Given
    a1 = create :appointment, official_report: "PV1345"
    a2 = create :appointment, official_report: "NPV123"
    a3 = create :appointment, official_report: "PV1234"
    # When
    result = Appointment.official_report_like('pv123')
    # Then
    expect(result).to eq [a2, a3]
  end

  it ".patient_last_name_like returns appointments associated with patient names beginning with provided string" do
    # Given
    a1 = create :appointment, patient: (build :patient, last_name: "dupont")
    a2 = create :appointment, patient: (build :patient, last_name: "Durant")
    a3 = create :appointment, patient: (build :patient, last_name: "Moldu")
    # When
    result = Appointment.patient_last_name_like("du")
    # Then
    expect(result).to eq [a1, a2]
  end

  describe '.of_patient_gender' do
    it "returns appointments of a given patient's gender" do
      # Given
      a1 = create :appointment, patient: (build :patient, gender: "woman")
      a2 = create :appointment, patient: (build :patient, gender: "man")
      a3 = create :appointment, patient: (build :patient, gender: nil)
      # When
      result = Appointment.of_patient_gender("woman")
      # Then
      expect(result).to eq [a1]
    end

    it "returns appointments with no gender for nil" do
      # Given
      a1 = create :appointment, patient: (build :patient, gender: "woman")
      a2 = create :appointment, patient: (build :patient, gender: "man")
      a3 = create :appointment, patient: (build :patient, gender: nil)
      # When
      result = Appointment.of_patient_gender(nil)
      # Then
      expect(result).to eq [a3]
    end
  end

  it ".of_patient_age_range returns appointment of a given patient's age range" do
    # Given
    today = Date.today
    date_floor = today - 29.years
    date_ceiling = today - 20.years
    now = Time.zone.now
    a1 = create :appointment, starts_at: now, patient: (build :patient, birth_year: nil, birthdate: date_floor-1.year)
    a2 = create :appointment, starts_at: now, patient: (build :patient, birth_year: nil, birthdate: date_floor-1.year+1.day)
    a3 = create :appointment, starts_at: now, patient: (build :patient, birth_year: nil, birthdate: date_ceiling)
    a4 = create :appointment, starts_at: now, patient: (build :patient, birth_year: nil, birthdate: date_ceiling+1.day)
    a5 = create :appointment, starts_at: now, patient: (build :patient, birth_year: nil, birthdate: nil)
    # When
    result = Appointment.of_patient_age_range([20, 29])
    # Then
    expect(result).to be_a ActiveRecord::Relation
    expect(result.to_a).to eq [a2, a3]
  end

  it ".of_patient_birthdate returns appointments of a given patient's birthdate" do
    # Given
    a1 = create :appointment, patient: (build :patient, birthdate: Date.new(2000,5,5))
    a2 = create :appointment, patient: (build :patient, birthdate: Date.new(2000,5,6))
    # When
    result = Appointment.of_patient_birthdate("2000-05-05")
    # Then
    expect(result).to eq [a1]
  end

  it ".of_user_in_charge returns appointments of a given assignee" do
    # Given
    user = create :user
    a1 = create :appointment, user_in_charge: user
    a2 = create :appointment
    # When
    result = Appointment.of_user_in_charge(user.id)
    # Then
    expect(result).to eq [a1]
  end

  it ".of_calling_institution returns appointments of a given calling institution" do
    # Given
    calling_institution = create :calling_institution
    a1 = create :appointment, calling_institution: calling_institution
    a2 = create :appointment
    # When
    result = Appointment.of_calling_institution(calling_institution.id)
    # Then
    expect(result).to eq [a1]
  end

  it ".of_consultation_reason returns appointments of a given consultation reason" do
    # Given
    consultation_reason = create :consultation_reason
    a1 = create :appointment, consultation_reason: consultation_reason
    a2 = create :appointment
    # When
    result = Appointment.of_consultation_reason(consultation_reason.id)
    # Then
    expect(result).to eq [a1]
  end

  it ".updated_after returns appointments updated after a given datetime" do
    # Given
    a1 = create :appointment
    a2 = create :appointment
    now = Time.zone.now
    a1.update_column(:updated_at, now + 10.minutes)
    # When
    result = Appointment.updated_after(now + 5.minutes)
    # Then
    expect(result).to eq [a1]
  end

  context "#validated_certificate" do
    context "without any ValidatedCertificate attached to its certificate" do
      it "returns nil" do
        # Given
        appointment = create :appointment, :with_certificate
        appointment.certificate.attached_files << AttachedFile::Standard.new
        # When
        result = appointment.validated_certificate
        # Then
        expect(result).to be_nil
      end
    end

    context "with at least one attached file of type :validated_certificate" do
      it "returns the last one" do
        # Given
        appointment = create :appointment, :with_certificate
        appointment.certificate.attached_files << AttachedFile::ValidatedCertificate.new
        appointment.certificate.attached_files << AttachedFile::Standard.new
        appointment.certificate.attached_files << (last_one = AttachedFile::ValidatedCertificate.new)
        # When
        result = appointment.validated_certificate
        # Then
        expect(result).to eq last_one
      end
    end
  end

  describe "#allowed_for_user?" do
    describe "when the user can take charge" do
      it "returns true" do
        # Given
        consultation_type = create :consultation_type, assigned_roles: [Role.find_by_title(:physician)]
        appointment = create :appointment, consultation_type: consultation_type
        user = create :user, :physician
        # When
        result = appointment.allowed_for_user?(user)
        # Then
        expect(result).to be_truthy
      end
    end

    describe "when the user cannot take charge" do
      it "returns false" do
        # Given
        consultation_type = create :consultation_type, assigned_roles: [Role.find_by_title(:physician)]
        appointment = create :appointment, consultation_type: consultation_type
        user = create :user, :association
        # When
        result = appointment.allowed_for_user?(user)
        # Then
        expect(result).to be_falsy
      end
    end
  end

  describe "#access_allowed_for_user?" do
    describe "when the user has access" do
      it "returns true" do
        # Given
        consultation_type = create :consultation_type, roles: [Role.find_by_title(:physician)]
        appointment = create :appointment, consultation_type: consultation_type
        user = create :user, :physician
        # When
        result = appointment.access_allowed_for_user?(user)
        # Then
        expect(result).to be_truthy
      end
    end

    describe "when the user doesn't have access" do
      it "returns false" do
        # Given
        consultation_type = create :consultation_type, roles: [Role.find_by_title(:physician)]
        appointment = create :appointment, consultation_type: consultation_type
        user = create :user, :association
        # When
        result = appointment.access_allowed_for_user?(user)
        # Then
        expect(result).to be_falsy
      end
    end
  end

  describe "#actual_medical_file" do
    describe "with a medical_file" do
      describe "as an originating appointment" do

        it "returns the associated medical file" do
          # Given
          medical_file = create :medical_file
          appointment = create :appointment, medical_file: medical_file
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end

      describe "as a secondary appointment" do

        it "returns the medical file of the originating appointment" do
          # Given
          medical_file = create :medical_file
          originating_appointment = create :appointment, medical_file: medical_file
          appointment = create :appointment, originating_appointment: originating_appointment
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end

      describe "as a third appointment" do

        it "returns the medical file of the first originating appointment" do
          # Given
          medical_file = create :medical_file
          originating_appointment = create :appointment, medical_file: medical_file
          appointment = create :appointment, originating_appointment: (create :appointment, originating_appointment: originating_appointment)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq medical_file
        end
      end
    end

    describe "without any medical_file" do
      describe "as an originating appointment" do

        it "returns a just created medical file" do
          # Given
          appointment = create :appointment
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq appointment.reload.medical_file
        end
      end

      describe "as a secondary appointment" do

        it "returns a just created medical file of the originating appointment" do
          # Given
          originating_appointment = create :appointment
          appointment = create :appointment, originating_appointment: originating_appointment
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq originating_appointment.reload.medical_file
        end
      end

      describe "as a third appointment" do

        it "returns a just created medical file of the first originating appointment" do
          # Given
          originating_appointment = create :appointment
          appointment = create :appointment, originating_appointment: (create :appointment, originating_appointment: originating_appointment)
          # When
          result = appointment.actual_medical_file
          # Then
          expect(result).to be_a MedicalFile
          expect(result).to eq originating_appointment.reload.medical_file
        end
      end
    end
  end
end
