# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe User, type: :model do
  subject { build :user }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:identifier) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:identifier).case_insensitive }
  it { is_expected.to have_secure_password }

  describe '#has_secure_password' do

    let(:password) { subject.password }

    it "authenticates on valid credentials" do
      # When
      result = subject.authenticate(password)
      # Then
      expect(result).to eq subject
    end

    it "authenticates on valid credentials" do
      # When
      result = subject.authenticate("buggy" + password)
      # Then
      expect(result).to be false
    end
  end

  it "#name concatenates last and first names" do
    # Given
    user = build :user, last_name: "Potter", first_name: "Harry"
    # When
    result = user.name
    # Then
    expect(result).to eq "Potter Harry"
  end

  it "#inactive? returns true when user has disabled_at value" do
    # Given
    active_user = create :user
    inactive_user = create :user, :inactive
    scheduled_user = create :user, :scheduled_for_deactivation
    # When
    active_result = active_user.inactive?
    inactive_result = inactive_user.inactive?
    scheduled_result = scheduled_user.inactive?
    # Then
    expect(active_result).to be_falsy
    expect(inactive_result).to be_truthy
    expect(scheduled_result).to be_falsy
  end
end
