# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe DutyPeriod, type: :model do

  subject { build(:duty_period) }

  it { is_expected.to be_valid }
  it { is_expected.to validate_presence_of(:scheduled_on) }

  describe "regarding the same associated time_slot" do

    let(:today) { Date.today }
    let(:time_slot) { create :time_slot }

    it "is valid if no other duty period for the same date exist" do
      # Given
      # When
      result = time_slot.duty_periods.build scheduled_on: today
      # Then
      expect(result).to be_valid
    end

    it "is not valid if another duty period for the same date exists" do
      # Given
      time_slot.duty_periods.create scheduled_on: today
      # when
      result = time_slot.duty_periods.build scheduled_on: today
      # Then
      expect(result).not_to be_valid
    end
  end

  describe "regarding datetimes and zones" do
    let(:time_slot) { create :time_slot, week_day: 1, starts_at: "T09:35", ends_at: "T15:45" }

    describe "winter is here!" do
      let(:scheduled_on) { "2018-01-15" }

      it "#starts_at returns computed datetime" do
        # Given
        duty_period = DutyPeriod.create(time_slot: time_slot, scheduled_on: scheduled_on)
        # When
        result = duty_period.starts_at
        # Then
        expect(result).to eq DateTime.new(2018,1,15,8,35)
      end

      it "#ends_at returns computed datetime" do
        # Given
        duty_period = DutyPeriod.create(time_slot: time_slot, scheduled_on: scheduled_on)
        # When
        result = duty_period.ends_at
        # Then
        expect(result).to eq DateTime.new(2018,1,15,14,45)
      end
    end

    describe "summer is here!" do
      let(:scheduled_on) { "2018-07-15" }

      it "#starts_at returns computed datetime" do
        # Given
        duty_period = DutyPeriod.create(time_slot: time_slot, scheduled_on: scheduled_on)
        # When
        result = duty_period.starts_at
        # Then
        expect(result).to eq DateTime.new(2018,7,15,7,35)
      end

      it "#ends_at returns computed datetime" do
        # Given
        duty_period = DutyPeriod.create(time_slot: time_slot, scheduled_on: scheduled_on)
        # When
        result = duty_period.ends_at
        # Then
        expect(result).to eq DateTime.new(2018,7,15,13,45)
      end
    end
  end
end
