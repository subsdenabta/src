# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe PdfGenerator, type: :model do

  let(:attached_file) { create :attached_file, attachable: (create :certificate) }
  let(:blob) { attached_file.file }
  let(:pdf_generator) { described_class.new(blob) }

  describe "initialization" do
    it "raises an error if no ActiveStorage::Attached::One provided" do
      # Given
      blob = 'some data'
      # When
      expect {
        pdf_generator = described_class.new(blob)
        # Then
      }.to raise_error "'blob' must be an ActiveStorage object"
    end

    it "sets @blob on initialization" do
      expect(pdf_generator.instance_variable_get('@blob')).to eq blob
    end
  end

  it "#content_type returns 'application/pdf'" do
    expect(pdf_generator.content_type).to eq 'application/pdf'
  end

  describe "#generate" do

    it "returns the generator if succeeds" do
      # Given
      # When
      result = pdf_generator.generate
      # Then
      expect(result).to eq pdf_generator
    end

    it "returns false if fails" do
      # Given
      allow(pdf_generator).to receive(:system).and_return(false)
      # When
      result = pdf_generator.generate
      # Then
      expect(result).to be_falsy
    end
  end

  describe "#set_pdf_file_path (private)" do
    it "replaces the extension if any exists" do
      # Given
      doc_path = '/path/to/file.doc'
      # When
      pdf_generator.send(:set_pdf_file_path, doc_path)
      # Then
      expect(pdf_generator.instance_variable_get('@pdf_file_path')).to eq '/path/to/file.pdf'
    end

    it "adds the extension if none exist" do
      # Given
      doc_path = '/path/to/file'
      # When
      pdf_generator.send(:set_pdf_file_path, doc_path)
      # Then
      expect(pdf_generator.instance_variable_get('@pdf_file_path')).to eq '/path/to/file.pdf'
    end
  end
end
