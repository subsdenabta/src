# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe Certificate, type: :model do

  subject { build :certificate }

  it { is_expected.to be_valid }

  context "#signed_certificate" do
    context "without any attached file of type :signed_certitificate" do
      it "returns nil" do
        # Given
        certificate = create :certificate
        certificate.attached_files << AttachedFile::Standard.new
        # When
        result = certificate.signed_certificate
        # Then
        expect(result).to be_nil
      end
    end

    context "with at least one attached file of type :signed_certificate" do
      it "returns the last one" do
        # Given
        certificate = create :certificate
        certificate.attached_files << AttachedFile::SignedCertificate.new
        certificate.attached_files << AttachedFile::Standard.new
        certificate.attached_files << (last_one = AttachedFile::SignedCertificate.new)
        # When
        result = certificate.signed_certificate
        # Then
        expect(result).to eq last_one
      end
    end
  end

  context "#validated_certificate" do
    context "without any attached file of type :validated_certitificate" do
      it "returns nil" do
        # Given
        certificate = create :certificate
        certificate.attached_files << AttachedFile::Standard.new
        # When
        result = certificate.validated_certificate
        # Then
        expect(result).to be_nil
      end
    end

    context "with at least one attached file of type :validated_certificate" do
      it "returns the last one" do
        # Given
        certificate = create :certificate
        certificate.attached_files << AttachedFile::ValidatedCertificate.new
        certificate.attached_files << AttachedFile::Standard.new
        certificate.attached_files << (last_one = AttachedFile::ValidatedCertificate.new)
        # When
        result = certificate.validated_certificate
        # Then
        expect(result).to eq last_one
      end
    end
  end

  context "#generate_validated_certificate_pdf" do
    let(:appointment) { create :appointment, :with_patient }

    it "creates a PDF file of the certificate" do
      # Given
      certificate = create :certificate, certificatable: appointment
      create :attached_file, type: "AttachedFile::SignedCertificate", attachable: certificate
      # When
      command_result = certificate.send :generate_validated_certificate_pdf
      file_result = certificate.validated_certificate
      # Then
      expect(command_result).to be_truthy
      expect(file_result).to be_a AttachedFile::ValidatedCertificate
      expect(file_result.file.content_type).to eq 'application/pdf'
      expect(certificate.checksum).not_to be_empty
    end
  end

  context "manage_validated_certificates" do

    let(:appointment) { create :appointment, :with_patient }
    let(:certificate) { create :certificate, certificatable: appointment }
    let!(:signed_certificate) { create :attached_file, type: "AttachedFile::SignedCertificate", attachable: certificate }

    context "when the certificate is NOT validated" do
      it "does nothing" do
        # Given
        certificate.validated_at = nil
        # When
        result = certificate.send :manage_validated_certificates
        # Then
        expect(result).to be_nil
      end
    end

    context "when the certificate is validated" do
      it "generates the pdf" do
        # Given
        certificate.validated_at = Time.zone.now
        # When
        certificate.send :manage_validated_certificates
        result = certificate.validated_certificate
        # Then
        expect(result).to be_a AttachedFile::ValidatedCertificate
      end
    end

    context "when the certificate is devalidated" do
      it "destroys old validated attached files" do
        # Given
        certificate.update_column(:validated_at, Time.zone.now)
        certificate.send :generate_validated_certificate_pdf
        certificate.validated_at = nil
        # When
        certificate.send :manage_validated_certificates
        result = certificate.validated_certificate
        # Then
        expect(result).to be_nil
      end
    end
  end

  describe '#validated_filename' do
    it "returns formated filename with validation datetime" do
      # Given
      Timecop.freeze(Time.local(2019, 12, 11, 10, 32))
      certificate = create :certificate
      create :attached_file, type: "AttachedFile::SignedCertificate", attachable: certificate, filename: "DUPONT Camille - A1912042 - GAV - 11122019-1024.doc"
      # When
      result = certificate.send :validated_filename
      # Then
      expect(result).to eq "Validé - DUPONT Camille - A1912042 - GAV - 11122019-1024 - 11122019-1032.pdf"
      Timecop.return
    end
  end

end
