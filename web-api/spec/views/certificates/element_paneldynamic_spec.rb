# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_paneldynamic", type: :view do

  let(:element) {
    {
      "type" => "paneldynamic",
      "name" => "question-dynamic-panel",
      "title" => "Regroupement dynamique"
    }
  }
  let(:data) { [] }

  describe "without data" do
    it "renders nothing" do
      # Given
      # When
      render partial: "json_certificates/element_paneldynamic", locals: { element: element, data: nil}
      # Then
      expect(rendered).to be_empty
    end
  end

  describe "with data" do
    it "renders a section with the title as h3" do
      # Given
      # When
      render partial: "json_certificates/element_paneldynamic", locals: { element: element, data: data}
      # Then
      expect(rendered).to match /<section.*h3.*>Regroupement dynamique/
    end
  end
end
