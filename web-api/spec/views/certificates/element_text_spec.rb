# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_text", type: :view do

  describe "regarding text type" do
    let(:element) {
      {
        "type" => "text",
        "name" => "question-text",
        "title" => "Intitulé"
      }
    }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_text", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with simple text" do
      it "renders a dl with the title as dt and the given text as dd" do
        # Given
        data = "Lorem ipsum"
        expected = "<dl><dt>Intitulé</dt><dd>Lorem ipsum</dd></dl>"
        # When
        render partial: "json_certificates/element_text", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end

  describe "regarding date type" do
    let(:element) {
      {
        "type" => "text",
        "name" => "question-text",
        "title" => "Intitulé",
        "inputType" => "date"
      }
    }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_text", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with simple text" do
      it "renders a dl with the title as dt and the localized date as dd" do
        # Given
        data = "2019-06-21"
        expected = "<dl><dt>Intitulé</dt><dd>21/06/2019</dd></dl>"
        I18n.locale = :fr
        # When
        render partial: "json_certificates/element_text", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end
end
