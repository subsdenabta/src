# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_multipletext", type: :view do

  let(:element) {
    {
      "type" => "multipletext",
      "name" => "question-multiple-text",
      "title" => "Textes multiples",
      "items" => [
        {
          "name" => "text1",
          "title" => "Texte 1"
        },
        {
          "name" => "text2",
          "title" => "Texte 2"
        }
      ]
    }
  }
  let( :data) {
    {
      "text1" => "Lorem ipsum dolor",
      "text2" => "sit amet"
    }
  }
  let(:expected) { "<dl class=\"Multipletext\"><dt>Texte 1</dt><dd>Lorem ipsum dolor</dd><dt>Texte 2</dt><dd>sit amet</dd></dl>" }

  describe "without data" do
    it "renders nothing" do
      # Given
      # When
      render partial: "json_certificates/element_multipletext", locals: { element: element, data: nil}
      # Then
      expect(rendered).to be_empty
    end
  end

  describe "with data" do
    it "renders a dl with the titles as dts and the given texts as dds" do
      # Given
      # When
      render partial: "json_certificates/element_multipletext", locals: { element: element, data: data}
      # Then
      expect(rendered).to eq expected
    end
  end
end
