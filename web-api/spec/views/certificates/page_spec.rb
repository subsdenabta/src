# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/page", type: :view do

  let(:page) {
    {
      "name" => "page-one",
      "title" => "Première page"
    }
  }
  let(:expected) { "<h2>Première page</h2>" }

  it "renders a section with the title as h2" do
    pending "may be removed"
    # Given
    # When
    render partial: "json_certificates/page", locals: { page: page }
    # Then
    expect(rendered).to match /<section.*h2.*>Première page/
  end
end
