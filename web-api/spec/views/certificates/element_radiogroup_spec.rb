# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_radiogroup", type: :view do

  before(:all) do
    appointment = create(:appointment, :with_certificate)
    appointment.certificate.update_attribute(:data, { "question-radiogroup-Comment" => "quatre" })
    assign(:appointment, appointment)
  end

  let(:expected) { "<dl><dt>Boutons radio</dt><dd>deux</dd></dl>" }

  describe "regarding simple choices" do

    let( :element) {
      {
        "type" => "radiogroup",
        "name" => "question-radiogroup",
        "title" => "Boutons radio",
        "choices" => [
          "un",
          "deux",
          "trois"
        ]
      }
    }
    let(:data) { "deux" }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with selected choice" do
      it "renders a dl with the title as dt and the choice's text as dd" do
        # Given
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end

    describe "with other choice" do
      it "renders a dl with the title as dt and the provided text as dd" do
        # Given
        data = "other"
        expected = "<dl><dt>Boutons radio</dt><dd>quatre</dd></dl>"
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end

  describe "regarding complex choices" do

    let(:element) {
      {
        "type" => "radiogroup",
        "name" => "question-radiogroup",
        "title" => "Boutons radio",
        "choices" => [
          {
            "value" => "one",
            "text" => "un"
          },
          {
            "value" => "two",
            "text" => "deux"
          },
          {
            "value" => "three",
            "text" => "trois"
          }
        ]
      }
    }
    let(:data) { "two" }

    describe "without data" do
      it "renders nothing" do
        # Given
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: nil}
        # Then
        expect(rendered).to be_empty
      end
    end

    describe "with selected choice" do
      it "renders a dl with the title as dt and the choice's text as dd" do
        # Given
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end

    describe "with other choice" do
      it "renders a dl with the title as dt and the provided text as dd" do
        # Given
        data = "other"
        expected = "<dl><dt>Boutons radio</dt><dd>quatre</dd></dl>"
        # When
        render partial: "json_certificates/element_radiogroup", locals: { element: element, data: data}
        # Then
        expect(rendered).to eq expected
      end
    end
  end
end
