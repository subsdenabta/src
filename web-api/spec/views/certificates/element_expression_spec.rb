# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'

RSpec.describe "json_certificates/element_expression", type: :view do

  let(:element) {
    {
      "type" => "expression",
      "name" => "question-expression",
      "title" => "Expression",
      "expression" => "{question-text}",
      "commentText" => "Other (describe)"
    }
  }
  let(:data) { "Lorem ipsum" }
  let(:expected) { "<dl><dt>Expression</dt><dd>Lorem ipsum</dd></dl>" }

  describe "without data" do
    it "renders nothing" do
      # Given
      # When
      render partial: "json_certificates/element_expression", locals: { element: element, data: nil}
      # Then
      expect(rendered).to be_empty
    end
  end

  describe "with data" do
    it "renders a dl with the title as dt and the given text as dd" do
      # Given
      # When
      render partial: "json_certificates/element_expression", locals: { element: element, data: data}
      # Then
      expect(rendered).to eq expected
    end
  end
end
