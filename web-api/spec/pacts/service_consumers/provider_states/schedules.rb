# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
Pact.provider_states_for "WEB GUI - Schedules" do

  provider_state "schedules exist" do
    set_up do
      Builder.create_all(InterventionDomain, Schedule, ConsultationType, TimeSlot)
    end
  end

  provider_state "intervention domains exist" do
    set_up do
      Builder.create_all(InterventionDomain)
    end
  end

end
