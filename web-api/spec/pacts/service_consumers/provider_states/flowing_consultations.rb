# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
Pact.provider_states_for "WEB GUI - Flowing consultations" do

  provider_state "flowing consultations exist" do
    set_up do
      [
        User,
        InterventionDomain,
        ConsultationType,
        CallingArea,
        CallingInstitution,
        Officer,
        Patient,
        FlowingConsultation
      ].each do |model|
        Builder.create_all(model)
      end
    end
  end

  provider_state "consultation types and calling institutions exist" do
    set_up do
      [
        InterventionDomain,
        ConsultationType,
        CallingArea,
        CallingInstitution,
        Patient
      ].each do |model|
        Builder.create_all(model)
      end
    end
  end
end
