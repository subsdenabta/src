# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
require 'rails_helper'
require 'pact/provider/rspec'

include FactoryBot::Syntax::Methods

%w[
builder authentication users intervention_domains consultation_types schedules
time_slots duty_periods consultation_reasons calling_areas calling_institutions
flowing_consultations consultation_subtypes appointments password_requests roles
certificate_layout certificate_templates attached_files medical_files
master_documents certificates patients
].each do |model|
  require "./spec/pacts/service_consumers/provider_states/#{model}.rb"
end

Pact.set_up do
  create_admin_user
end

Pact.service_provider "WEB API" do

  handmade_domains = %i[
    appointments
    attached_files
    authent
    calling_areas
    calling_institutions
    certificate_templates
    consultation_reasons
    consultation_subtypes
    consultation_types
    duty_periods
    flowing_consultations
    intervention_domains
    master_documents
    password_requests
    patients
    schedules
    time_slots
    users
  ]


  handmade_domains.each do |domain|
    honours_pact_with "WEB GUI" do
      pact_uri "../web-gui/pacts/web-api/#{domain}.json"
    end
  end


  generated_domains = %i[
    web-api_certificate-layout
    web-api_certificates
    web-api_master-documents
    web-api_medical-files
    web-api_roles
  ]

  generated_domains.each do |domain|
    honours_pact_with "WEB GUI" do
      pact_uri "../web-gui/pacts/generated/#{domain}.json"
    end
  end
end

def create_admin_user
  user = create :user, id: 1, role_id: 1, identifier: "administrator", password: "administrator"
  Doorkeeper::AccessToken
    .create!(resource_owner_id: user.id)
    .update_attribute(:token, "my_secret_token")
end
