# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MoveRoleAsRelationOnUsers < ActiveRecord::Migration[5.1]
  
  PREVIOUS_ROLES = {
    administrator: 1,
    association: 2,
    nurse: 3,
    physician: 4,
    psychiatrist: 5,
    psychologist: 6,
    secretary: 7,
    switchboard_operator: 8,
    unknown: 0
  }

  def up
    Role::ALL.each do |role|
      Role.find_or_create_by!(title: role)
    end

    add_column :users, :role_id, :integer

    User.all.each do |user|
      previous_role_num = exec_query("select role from users where id = #{user.id}").rows.first.first
      if previous_role_num == 0
        puts "User #{user.identifier} has 'unknown' role, changed to 'association': please check!"
        previous_role = :association
      else
        previous_role = PREVIOUS_ROLES.key(previous_role_num).to_s
      end
      new_role = Role.find_by_title(previous_role)
      user.role = new_role
      user.save!
    end

    change_column_null :users, :role_id, false
    remove_column :users, :role
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
