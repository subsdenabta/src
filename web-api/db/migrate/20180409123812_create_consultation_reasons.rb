# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CreateConsultationReasons < ActiveRecord::Migration[5.1]
  def change
    create_table :consultation_reasons do |t|
      t.interval :appointment_duration, null: false
      t.string :code, null: false
      t.string :colour, null: false
      t.string :title, null: false
      t.references :consultation_type, foreign_key: true, null: false

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        # return value as ISO8601 duration
        db_name = ActiveRecord::Base.connection.current_database
        execute "ALTER DATABASE \"#{db_name}\" SET intervalstyle = iso_8601"
      end
      dir.down do
        # return value as postgres duration
        db_name = ActiveRecord::Base.connection.current_database
        execute "ALTER DATABASE \"#{db_name}\" SET intervalstyle = postgres"
      end
    end
  end
end
