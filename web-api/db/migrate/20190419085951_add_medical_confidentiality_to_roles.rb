# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AddMedicalConfidentialityToRoles < ActiveRecord::Migration[5.2]
  def up
    add_column :roles, :medical_confidentiality, :boolean, null: false, default: false
    execute <<-SQL
      UPDATE roles
      SET medical_confidentiality = true
      WHERE title IN ('administrator', 'physician', 'nurse', 'secretary', 'psychiatrist', 'psychologist', 'psychiatrist_expert', 'psychologist_expert')
    SQL
  end

  def down
    remove_column :roles, :medical_confidentiality
  end
end
