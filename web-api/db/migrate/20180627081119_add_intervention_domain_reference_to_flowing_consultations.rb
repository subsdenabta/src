# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AddInterventionDomainReferenceToFlowingConsultations < ActiveRecord::Migration[5.1]
  def change
    add_reference :flowing_consultations, :intervention_domain, foreign_key: true

    reversible do |dir|
      dir.up do
        FlowingConsultation.all.each do |fc|
          # Before this, consultation_type was required so it must exist
          fc.update_attribute(:intervention_domain, fc.consultation_type.intervention_domain)
        end
      end

      dir.down {}
    end

    change_column_null :flowing_consultations, :intervention_domain_id, false
  end
end
