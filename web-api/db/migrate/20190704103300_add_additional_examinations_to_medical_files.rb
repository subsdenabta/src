# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AddAdditionalExaminationsToMedicalFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_files, :wrist_xray, :boolean, default: false
    add_column :medical_files, :teeth_xray, :boolean, default: false
    add_column :medical_files, :clavicle_xray, :boolean, default: false
    add_column :medical_files, :std_test, :boolean, default: false
    add_column :medical_files, :pre_therapy_test, :boolean, default: false
    add_column :medical_files, :cga_test, :boolean, default: false
    add_column :medical_files, :dna_test, :boolean, default: false
    add_column :medical_files, :chemical_submission, :boolean, default: false
    add_column :medical_files, :triple_therapy, :boolean, default: false
    add_column :medical_files, :dressing, :boolean, default: false
    change_column_default :medical_files, :placed, from: nil, to: false
  end
end
