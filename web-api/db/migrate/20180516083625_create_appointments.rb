# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.datetime :starts_at
      t.interval :duration
      t.string :official_report
      t.text :circumstances
      t.text :comment
      t.references :officer, foreign_key: true
      t.references :patient, foreign_key: true
      t.references :consultation_type, foreign_key: true
      t.references :consultation_reason, foreign_key: true
      t.references :calling_institution, foreign_key: true
      t.references :consultation_subtype, foreign_key: true

      t.timestamps
    end
  end
end
