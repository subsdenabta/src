# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MakeCertifcateRelationshipAsPolymorphic < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :certificates, :appointments
    rename_column :certificates, :appointment_id, :certificatable_id
    add_column :certificates, :certificatable_type, :string, null: false, default: 'Appointment'
    change_column_default :certificates, :certificatable_type, from: 'Appointment', to: nil
  end
end
