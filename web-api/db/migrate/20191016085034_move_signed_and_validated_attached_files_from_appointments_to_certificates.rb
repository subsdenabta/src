# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MoveSignedAndValidatedAttachedFilesFromAppointmentsToCertificates < ActiveRecord::Migration[5.2]
  def up
    Appointment.all.each do |appointment|
      certificate = appointment.certificate
      appointment.attached_files.where(type: 'AttachedFile::SignedCertificate').or(appointment.attached_files.where(type: 'AttachedFile::ValidatedCertificate')).each do |file|
        file.attachable = certificate
        file.save!
      end
    end
  end

  def down
    Certificate.all.each do |certificate|
      if (certificatable = certificate.certificatable).is_a? Appointment
        certificate.attached_files.where(type: 'AttachedFile::SignedCertificate').or(certificate.attached_files.where(type: 'AttachedFile::ValidatedCertificate')).each do |file|
          file.attachable = certificatable
          file.save!
        end
      end
    end
  end
end
