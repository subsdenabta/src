# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class RemoveDuplicatedForAppointmentIdFromPatients < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        remembered_appointment_sql = <<~SQL
          UPDATE appointments a
          SET patient_id = p.id
          FROM patients p
          WHERE p.duplicated_for_appointment_id = a.id;
        SQL
        execute(remembered_appointment_sql)

        forget_duplications_sql = <<~SQL
          UPDATE patients
          SET duplicated_patient_id = NULL, probably_duplicated_patient_id = NULL
        SQL
        execute(forget_duplications_sql)
      end
    end

    remove_reference :patients, :duplicated_for_appointment
  end
end
