# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
# Mandatory for PostgreSQL
# May not work properly under other RDMS
connection = ActiveRecord::Base.connection
connection.execute("ALTER DATABASE \"#{connection.current_database}\" SET IntervalStyle = iso_8601")
connection.reconnect! # Necessary else previous command not taken into account at first tests iteration

Dir.chdir(File.join('db', 'seeds')) do
  Dir.glob('*.json') do |filename|
    /(\w+)_by_(\w+).json/.match(filename) do |matched|
      table_name = matched[1] # e.g. "intervention_domains"
      attr_pivot = matched[2] # e.g. "title"

      puts "Seeding #{table_name}…" unless Rails.env.test?
      model_name = table_name.classify # e.g. "InterventionDomain"
      model = model_name.constantize # e.g. InterventionDomain

      records = JSON.parse(File.read(filename))
      records.each do |record|
        model.where(record.slice(attr_pivot)).first_or_initialize.update(record)
      end
    end
  end
end

puts 'Seeding national holidays…' unless Rails.env.test?
HOLIDAYS_YEARS ||= (2018..2022)
NationalHoliday.destroy_all
fixed_holidays_specs = {
  'Jour de l\'an': [1, 1],
  'Fête du Travail': [5, 1],
  'Victoire 1945': [5, 8],
  'Fête nationale': [7, 14],
  'Assomption': [8, 15],
  'Toussaint': [11, 1],
  'Armistice 1918': [11, 11],
  'Noël': [12, 25]
}
fixed_holidays = fixed_holidays_specs.map do |title, month_day|
  HOLIDAYS_YEARS.map do |year|
    date_components = [year] + month_day
    NationalHoliday.create!(title: title, date: Date.new(*date_components))
  end
end.flatten

moving_holidays_specs = {
  'Lundi de Pâques': [
    Date.new(2018, 4, 2),
    Date.new(2019, 4, 22),
    Date.new(2020, 4, 13),
    Date.new(2021, 4, 5),
    Date.new(2022, 4, 18),
  ],
  'Jeudi de l\'Ascension': [
    Date.new(2018, 5, 10),
    Date.new(2019, 5, 30),
    Date.new(2020, 5, 21),
    Date.new(2021, 5, 13),
    Date.new(2022, 5, 26),
  ],
  'Lundi de Pentecôte': [
    Date.new(2018, 5, 21),
    Date.new(2019, 6, 10),
    Date.new(2020, 6, 1),
    Date.new(2021, 5, 24),
    Date.new(2022, 6, 6),
  ]
}
moving_holidays = moving_holidays_specs.map do |title, dates|
  dates.map do |date|
    NationalHoliday.create!(title: title, date: date)
  end
end.flatten

holidays_count = (fixed_holidays + moving_holidays).count
expected_count = HOLIDAYS_YEARS.count * (fixed_holidays_specs.keys + moving_holidays_specs.keys).count
raise "Wrong number of national holidays: #{holidays_count} vs #{expected_count} expected !" unless expected_count == holidays_count

puts 'Seeding certificate layout…' unless Rails.env.test?
Settings::CertificateLayout.first_or_create!

unless Rails.env.test?
  puts 'Seeding certificate templates…'
  Rake::Task["filae:certificate_templates:seed"].invoke
end

if Rails.env.development? || ENV['FORCE_SEEDS'].present?
  puts 'Seeding users…'
  Role.find_each do |role|
    3.times do |index|
      suffix = index.zero? && '' || (index + 1).to_s
      unique_id = role.title + suffix
      FactoryBot.create(:user,
        first_name: unique_id.titleize,
        identifier: unique_id,
        password: role.title,
        role: role
      )
    end
  end

  puts 'Seeding intervention domains…'
  interventions = [
    {
      domain_attrs: {
        title: "Consultation",
        modus_operandi: :appointments
      },
      consultation_types: [
        {
          type_attrs: {
            title: "Psychologue",
            role_ids: Role.where(title: %w[administrator psychologist]).ids,
            assigned_role_ids: Role.where(title: %w[administrator psychologist]).ids
          },
          with_time_slot: true
        },
        {
          type_attrs: {
            title: "Association",
            role_ids: Role.where(title: %w[administrator association]).ids,
            assigned_role_ids: Role.where(title: %w[administrator association]).ids
          },
          with_time_slot: true
        },
        {
          type_attrs: {
            title: "Consultation médicale",
            role_ids: Role.where(title: %w[administrator physician nurse secretary psychiatrist psychologist association]).ids,
            assigned_role_ids: Role.where(title: %w[administrator physician]).ids,
          },
          with_time_slot: true,
          consultation_reasons: [
            { reason_attrs: { title: "Violence volontaire", code: "VV", colour: "#00AFDF", appointment_duration: "PT1H" } },
            { reason_attrs: { title: "Agression sexuelle", code: "AS", colour: "#00DFAF", appointment_duration: "PT45M" } }
          ],
          complementary_consultation_types: [
            { title: 'Psychologue' },
            { title: 'Association' }
          ]
        },
        { type_attrs: { title: "Psychiatre"}, with_time_slot: true  },
        { type_attrs: { title: "Audition/Expertise psy"}, with_time_slot: true  },
        { type_attrs: { title: "Expertise"}, with_time_slot: true  }
      ],
      schedules: [
        { schedule_attrs: { activated_on: Date.today - 2.days} }
      ]
    },
    {
      domain_attrs: {
        title: "Antenne",
        modus_operandi: :stream,
        role_ids: Role.where(title: %w[administrator physician secretary switchboard_operator]).ids
      },
      consultation_types: [
        { type_attrs: { title: "GAV"} },
        { type_attrs: { title: "Levée de corps"} },
        { type_attrs: { title: "ITT PDAP commissariat"} },
        { type_attrs: { title: "ITT site extérieur"} },
        { type_attrs: { title: "AS"} }
      ]
    },
    {
      domain_attrs: {
        title: "Sur dossier",
        modus_operandi: :stream
      },
      consultation_types: [
        { type_attrs: { title: "Certificat sur dossier"} }
      ]
    }
  ]
  interventions.each do |intervention|
    domain = InterventionDomain.create!(intervention[:domain_attrs])
    consultation_types = []
    intervention[:consultation_types].each do |consultation_type|
      type = ConsultationType.create!(consultation_type[:type_attrs].merge(intervention_domain: domain))
      consultation_types << type if consultation_type[:with_time_slot]
      consultation_type[:consultation_reasons].try(:each) do |consultation_reason|
          ConsultationReason.create!(consultation_reason[:reason_attrs].merge(consultation_type: type))
      end
      consultation_type[:complementary_consultation_types].try(:each) do |attrs|
          type.complementary_consultation_types << ConsultationType.where(attrs).first
      end
    end
    (schedules = intervention[:schedules]) && schedules.each do |schedule|
      schedule = Schedule.create!(schedule[:schedule_attrs].merge(intervention_domain: domain))
      consultation_types.each do |consultation_type|
        (-2..2).each do |offset|
          TimeSlot.create!(
            week_day: (Date.today + offset.send(:days)).cwday,
            starts_at: "09:00",
            ends_at: "17:00",
            schedule: schedule,
            consultation_type: consultation_type
          )
        end
      end
    end
  end

  puts 'Seeding calling areas and institutions…'
  north = CallingArea.create!(title: "Nord", colour: "#00afdf")
  south = CallingArea.create!(title: "Sud", colour: "#00dfaf")
  CallingInstitution.create!(title: "Commissariat de police des Mureaux", calling_area: north)
  CallingInstitution.create!(title: "Gendarmerie de Saint-Germain-en-Laye", calling_area: north)
  CallingInstitution.create!(title: "Gendarmerie de Rambouillet", calling_area: south)

  puts 'Seeding flowing consultations…'
  officer = Officer.create!(name: "Moulin")
  intervention_domain = InterventionDomain.find_by(modus_operandi: 'stream');
  physician_role = Role.find_by_title(:physician)
  physicians = User.where(role: physician_role)
  20.times do
    called_at =Random.rand(1..24).hours.ago
    assigned_at = [called_at + 5.minutes, nil].sample
    closed_at = called_at + 30.minutes
    cancelled_at = [closed_at, nil].sample
    validated_at = !cancelled_at && assigned_at && [closed_at, nil].sample
    assignee = physicians.sample

    patient = FactoryBot.create(:patient, :for_flowing_consultation)

    FlowingConsultation.create!(
      intervention_domain: intervention_domain,
      consultation_type_id: intervention_domain.consultation_type_ids.sample,
      calling_institution_id: CallingInstitution.ids.sample,
      called_at: called_at,
      assigned_at: assigned_at,
      validated_at: validated_at,
      cancelled_at: cancelled_at,
      patient: patient,
      officer: officer,
      assignee: assignee
    )
  end

  puts 'Seeding appointments…'
  officers = FactoryBot.create_list(:officer, 5)
  consultation_type = ConsultationType.find_by(title: 'Consultation médicale');
  assignable_users = User.where(role: consultation_type.assigned_roles)
  20.times do
    starts_at = (Time.current + Random.rand(-2..2).days).change(hour: Random.rand(9..16))
    duration = (30..90).step(15).to_a.sample.minutes

    patient = FactoryBot.build(:patient, :for_appointment)

    Appointment.create!(
      consultation_type: consultation_type,
      consultation_reason_id: consultation_type.consultation_reason_ids.sample,
      calling_institution_id: CallingInstitution.ids.sample,
      starts_at: starts_at,
      duration: duration,
      patient: patient,
      officer: officers.sample
    )
  end
end
