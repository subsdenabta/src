# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_08_130456) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.integer "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "appointments", force: :cascade do |t|
    t.datetime "starts_at"
    t.interval "duration"
    t.string "official_report"
    t.text "circumstances"
    t.text "comment"
    t.bigint "officer_id"
    t.bigint "patient_id"
    t.bigint "consultation_type_id"
    t.bigint "consultation_reason_id"
    t.bigint "calling_institution_id"
    t.bigint "consultation_subtype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "onml_id"
    t.bigint "user_in_charge_id"
    t.datetime "cancelled_at"
    t.datetime "patient_arrived_at"
    t.integer "cancelled_by"
    t.bigint "previous_appointment_id"
    t.boolean "emergency", default: false
    t.boolean "patient_missing", default: false
    t.datetime "requisition_received_at"
    t.bigint "originating_appointment_id"
    t.datetime "patient_released_at"
    t.string "patient_phone"
    t.bigint "original_patient_id"
    t.index ["calling_institution_id"], name: "index_appointments_on_calling_institution_id"
    t.index ["consultation_reason_id"], name: "index_appointments_on_consultation_reason_id"
    t.index ["consultation_subtype_id"], name: "index_appointments_on_consultation_subtype_id"
    t.index ["consultation_type_id"], name: "index_appointments_on_consultation_type_id"
    t.index ["officer_id"], name: "index_appointments_on_officer_id"
    t.index ["original_patient_id"], name: "index_appointments_on_original_patient_id"
    t.index ["originating_appointment_id"], name: "index_appointments_on_originating_appointment_id"
    t.index ["patient_id"], name: "index_appointments_on_patient_id"
    t.index ["previous_appointment_id"], name: "index_appointments_on_previous_appointment_id"
    t.index ["starts_at"], name: "index_appointments_on_starts_at"
  end

  create_table "assignments", force: :cascade do |t|
    t.bigint "duty_period_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["duty_period_id"], name: "index_assignments_on_duty_period_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "attached_files", force: :cascade do |t|
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.index ["attachable_type", "attachable_id"], name: "index_attached_files_on_attachable_type_and_attachable_id"
  end

  create_table "calling_areas", force: :cascade do |t|
    t.string "title", null: false
    t.string "colour", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calling_institutions", force: :cascade do |t|
    t.string "title", null: false
    t.bigint "calling_area_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["calling_area_id"], name: "index_calling_institutions_on_calling_area_id"
  end

  create_table "certificate_templates", force: :cascade do |t|
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "disabled_at"
    t.jsonb "questionnaire"
    t.string "seedname"
    t.index ["seedname"], name: "index_certificate_templates_on_seedname", unique: true
  end

  create_table "certificate_templates_roles", force: :cascade do |t|
    t.bigint "certificate_template_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["certificate_template_id"], name: "index_certificate_templates_roles_on_certificate_template_id"
    t.index ["role_id"], name: "index_certificate_templates_roles_on_role_id"
  end

  create_table "certificates", force: :cascade do |t|
    t.jsonb "questionnaire"
    t.jsonb "data"
    t.datetime "delivered_at"
    t.bigint "certificatable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "validated_at"
    t.bigint "validator_id"
    t.string "certificatable_type", null: false
    t.string "sha512_checksum"
    t.index ["certificatable_id"], name: "index_certificates_on_certificatable_id"
    t.index ["validator_id"], name: "index_certificates_on_validator_id"
  end

  create_table "consultation_reasons", force: :cascade do |t|
    t.interval "appointment_duration", null: false
    t.string "code", null: false
    t.string "colour", null: false
    t.string "title", null: false
    t.bigint "consultation_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consultation_type_id"], name: "index_consultation_reasons_on_consultation_type_id"
  end

  create_table "consultation_subtypes", force: :cascade do |t|
    t.string "title", null: false
    t.interval "appointment_duration"
    t.bigint "consultation_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consultation_type_id"], name: "index_consultation_subtypes_on_consultation_type_id"
  end

  create_table "consultation_type_assigned_roles", force: :cascade do |t|
    t.bigint "consultation_type_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consultation_type_id"], name: "index_consultation_type_assigned_roles_on_consultation_type_id"
    t.index ["role_id"], name: "index_consultation_type_assigned_roles_on_role_id"
  end

  create_table "consultation_type_authorizations", force: :cascade do |t|
    t.bigint "consultation_type_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consultation_type_id"], name: "index_consultation_type_authorizations_on_consultation_type_id"
    t.index ["role_id"], name: "index_consultation_type_authorizations_on_role_id"
  end

  create_table "consultation_type_complementary_consultation_types", force: :cascade do |t|
    t.bigint "consultation_type_id", null: false
    t.bigint "complementary_consultation_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["complementary_consultation_type_id"], name: "index_consult_type_compl_consult_types_on_compl_consult_type_id"
    t.index ["consultation_type_id"], name: "index_consult_type_compl_consult_types_on_consult_type_id"
  end

  create_table "consultation_types", force: :cascade do |t|
    t.string "title", null: false
    t.bigint "intervention_domain_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icon_name"
    t.index ["intervention_domain_id"], name: "index_consultation_types_on_intervention_domain_id"
    t.index ["title", "intervention_domain_id"], name: "index_consultation_types_on_title_and_intervention_domain_id", unique: true
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "domain_authorizations", force: :cascade do |t|
    t.bigint "role_id", null: false
    t.bigint "intervention_domain_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["intervention_domain_id"], name: "index_domain_authorizations_on_intervention_domain_id"
    t.index ["role_id"], name: "index_domain_authorizations_on_role_id"
  end

  create_table "duty_periods", force: :cascade do |t|
    t.date "scheduled_on", null: false
    t.bigint "time_slot_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["scheduled_on", "time_slot_id"], name: "index_duty_periods_on_scheduled_on_and_time_slot_id", unique: true
    t.index ["time_slot_id"], name: "index_duty_periods_on_time_slot_id"
  end

  create_table "flowing_consultations", force: :cascade do |t|
    t.datetime "called_at"
    t.text "comment"
    t.datetime "assigned_at"
    t.datetime "validated_at"
    t.datetime "cancelled_at"
    t.bigint "officer_id"
    t.bigint "patient_id"
    t.bigint "consultation_type_id"
    t.bigint "calling_institution_id"
    t.bigint "assignee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "concerns_minor", default: false
    t.bigint "intervention_domain_id", null: false
    t.string "onml_id"
    t.integer "cancelled_by"
    t.string "patient_phone"
    t.bigint "original_patient_id"
    t.index ["assignee_id"], name: "index_flowing_consultations_on_assignee_id"
    t.index ["calling_institution_id"], name: "index_flowing_consultations_on_calling_institution_id"
    t.index ["consultation_type_id"], name: "index_flowing_consultations_on_consultation_type_id"
    t.index ["intervention_domain_id"], name: "index_flowing_consultations_on_intervention_domain_id"
    t.index ["officer_id"], name: "index_flowing_consultations_on_officer_id"
    t.index ["original_patient_id"], name: "index_flowing_consultations_on_original_patient_id"
    t.index ["patient_id"], name: "index_flowing_consultations_on_patient_id"
  end

  create_table "intervention_domains", force: :cascade do |t|
    t.string "title", null: false
    t.integer "modus_operandi", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "identifier_prefix"
    t.index ["title"], name: "index_intervention_domains_on_title", unique: true
  end

  create_table "logs", force: :cascade do |t|
    t.string "what"
    t.string "what_id", null: false
    t.datetime "occured_at", null: false
    t.string "who"
    t.string "who_id", null: false
    t.jsonb "event", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "master_documents", force: :cascade do |t|
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachable_type", "attachable_id"], name: "index_master_documents_on_attachable_type_and_attachable_id"
  end

  create_table "medical_files", force: :cascade do |t|
    t.text "address"
    t.string "chaperon_name"
    t.string "chaperon_quality"
    t.string "phone_number"
    t.boolean "placed", default: false
    t.string "actor_name"
    t.text "obs_nurse"
    t.text "obs_psychologist"
    t.text "obs_general"
    t.bigint "appointment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "wrist_xray", default: false
    t.boolean "teeth_xray", default: false
    t.boolean "clavicle_xray", default: false
    t.boolean "std_test", default: false
    t.boolean "pre_therapy_test", default: false
    t.boolean "cga_test", default: false
    t.boolean "dna_test", default: false
    t.boolean "chemical_submission", default: false
    t.boolean "triple_therapy", default: false
    t.boolean "dressing", default: false
    t.text "medical_obs"
    t.text "case_history"
    t.text "background"
    t.text "treatment"
    t.text "allergy"
    t.text "clinical_examination"
    t.text "complementary_info"
    t.boolean "psychological_impact_requested", default: false
    t.index ["appointment_id"], name: "index_medical_files_on_appointment_id"
  end

  create_table "national_holidays", force: :cascade do |t|
    t.string "title"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confidential", default: true, null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "officers", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "lambda_user", default: false
  end

  create_table "patients", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "birth_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "birthdate"
    t.integer "gender"
    t.string "phone_deprecated_on_20211018"
    t.bigint "duplicated_patient_id"
    t.bigint "probably_duplicated_patient_id"
    t.index ["duplicated_patient_id"], name: "index_patients_on_duplicated_patient_id"
    t.index ["probably_duplicated_patient_id"], name: "index_patients_on_probably_duplicated_patient_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "medical_confidentiality", default: false, null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.date "activated_on", null: false
    t.bigint "intervention_domain_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["intervention_domain_id"], name: "index_schedules_on_intervention_domain_id"
  end

  create_table "settings_certificate_layouts", force: :cascade do |t|
    t.text "organisation_chart"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_slots", force: :cascade do |t|
    t.integer "week_day", null: false
    t.time "starts_at", null: false
    t.time "ends_at", null: false
    t.bigint "schedule_id", null: false
    t.bigint "consultation_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consultation_type_id"], name: "index_time_slots_on_consultation_type_id"
    t.index ["schedule_id"], name: "index_time_slots_on_schedule_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "identifier", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "email", null: false
    t.integer "role_id", null: false
    t.datetime "disabled_at"
    t.index ["identifier"], name: "index_users_on_identifier", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "appointments", "appointments", column: "originating_appointment_id"
  add_foreign_key "appointments", "appointments", column: "previous_appointment_id"
  add_foreign_key "appointments", "calling_institutions"
  add_foreign_key "appointments", "consultation_reasons"
  add_foreign_key "appointments", "consultation_subtypes"
  add_foreign_key "appointments", "consultation_types"
  add_foreign_key "appointments", "officers"
  add_foreign_key "appointments", "patients"
  add_foreign_key "appointments", "patients", column: "original_patient_id"
  add_foreign_key "assignments", "duty_periods"
  add_foreign_key "assignments", "users"
  add_foreign_key "calling_institutions", "calling_areas"
  add_foreign_key "certificate_templates_roles", "certificate_templates"
  add_foreign_key "certificate_templates_roles", "roles"
  add_foreign_key "certificates", "users", column: "validator_id"
  add_foreign_key "consultation_reasons", "consultation_types"
  add_foreign_key "consultation_subtypes", "consultation_types"
  add_foreign_key "consultation_type_assigned_roles", "consultation_types"
  add_foreign_key "consultation_type_assigned_roles", "roles"
  add_foreign_key "consultation_type_authorizations", "consultation_types"
  add_foreign_key "consultation_type_authorizations", "roles"
  add_foreign_key "consultation_type_complementary_consultation_types", "consultation_types"
  add_foreign_key "consultation_type_complementary_consultation_types", "consultation_types", column: "complementary_consultation_type_id"
  add_foreign_key "consultation_types", "intervention_domains"
  add_foreign_key "domain_authorizations", "intervention_domains"
  add_foreign_key "domain_authorizations", "roles"
  add_foreign_key "duty_periods", "time_slots"
  add_foreign_key "flowing_consultations", "calling_institutions"
  add_foreign_key "flowing_consultations", "consultation_types"
  add_foreign_key "flowing_consultations", "intervention_domains"
  add_foreign_key "flowing_consultations", "officers"
  add_foreign_key "flowing_consultations", "patients"
  add_foreign_key "flowing_consultations", "patients", column: "original_patient_id"
  add_foreign_key "flowing_consultations", "users", column: "assignee_id"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "patients", "patients", column: "duplicated_patient_id"
  add_foreign_key "patients", "patients", column: "probably_duplicated_patient_id"
  add_foreign_key "schedules", "intervention_domains"
  add_foreign_key "time_slots", "consultation_types"
  add_foreign_key "time_slots", "schedules"
end
