# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
namespace :filae do

  namespace :certificate_templates do

    desc "Seed certificate templates into database"
    task :seed => :environment do
      Dir.chdir(File.join('db', 'seeds', 'certificate_templates')) do
        Dir.glob('*.json') do |filename|
          attrs = JSON.parse(File.read(filename)).merge({disabled_at: Time.current})
          CertificateTemplate.where(seedname: filename).first_or_create!(attrs)
        end
      end
    end

    desc "Reload seed of specified certificate template into database (useful in migrations)"
    task :reseed, [:filename] => :environment do |task, args|
      filename = args.filename

      puts "Reseeding certificate template #{filename}…"
      Dir.chdir(File.join('db', 'seeds', 'certificate_templates')) do
        attrs = JSON.parse(File.read(filename))
        CertificateTemplate.find_or_initialize_by(seedname: filename).update!(attrs)
      end
    end
  end
end
