# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class UserPolicy < ApplicationPolicy

  def index?
    connected_user?
  end

  def show?
    connected_user?
  end

  def create?
    administrator_user?
  end

  def update?
    administrator_user? ||
      connected_user? && record == user
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  def permitted_attributes
    allowed_attributes = []
    allowed_attributes += [:identifier, :email, :first_name, :last_name, :disabled_at, :role_id] if administrator_user?
    allowed_attributes += [:password] if connected_user? && record == user
    allowed_attributes
  end
end
