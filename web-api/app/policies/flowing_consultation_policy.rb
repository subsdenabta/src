# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultationPolicy < ApplicationPolicy

  def index?
    connected_user?
  end

  def show?
    connected_user?
  end

  def last_updated?
    connected_user?
  end

  def create?
    can_manage?
  end

  def update?
    record.archived? ? whitelist_roles(%i[administrator nurse secretary]) : can_manage?
  end

  def certificates?
    can_manage?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end

  def permitted_attributes
    return [] unless can_manage?

    [
      :id, :called_at, :comment, :validated_at, :assigned_at, :cancelled_at, :cancelled_by, :concerns_minor,
      :intervention_domain_id, :consultation_type_id, :calling_institution_id, :assignee_id, :patient_id
    ]
  end

  private

  def can_manage?
    whitelist_roles([
      "administrator",
      "physician",
      "nurse",
      "secretary",
      "switchboard_operator"
    ])
  end
end
