# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MedicalFilePolicy < ApplicationPolicy

  def show?
    medical_content_access?
  end

  def update?
    medical_content_access?
  end

  def permitted_attributes
    [
      :address,
      :chaperon_name,
      :chaperon_quality,
      :phone_number,
      :placed,
      :actor_name,
      :obs_physician,
      :obs_nurse,
      :obs_psychologist,
      :obs_general,
      :wrist_xray,
      :teeth_xray,
      :clavicle_xray,
      :std_test,
      :pre_therapy_test,
      :cga_test,
      :dna_test,
      :chemical_submission,
      :triple_therapy,
      :dressing,
      :psychological_impact_requested,
      :medical_obs,
      :case_history,
      :background,
      :treatment,
      :allergy,
      :clinical_examination,
      :complementary_info
    ]
  end
end
