# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationReasonsController < ApplicationController
  before_action :set_consultation_reason, only: [:show, :update, :destroy]
  deserializable_resource :consultation_reason, only: [:create, :update]

  def show
    render jsonapi: @consultation_reason
  end

  def create
    @consultation_reason = ConsultationReason.new(permitted_attributes(ConsultationReason))
    authorize @consultation_reason

    if @consultation_reason.save
      render jsonapi: @consultation_reason, status: :created, location: @consultation_reason
    else
      render jsonapi_errors: @consultation_reason.errors, status: :unprocessable_entity
    end
  end

  def update
    if @consultation_reason.update(permitted_attributes(@consultation_reason))
      render jsonapi: @consultation_reason
    else
      render jsonapi_errors: @consultation_reason.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @consultation_reason.destroy
  end

  private

  def set_consultation_reason
    @consultation_reason = ConsultationReason.find(params[:id])
    authorize @consultation_reason
  end
end
