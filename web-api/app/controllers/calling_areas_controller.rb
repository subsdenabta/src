# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CallingAreasController < ApplicationController
  before_action :set_calling_area, only: [:show, :update, :destroy]
  deserializable_resource :calling_area, only: [:create, :update]

  def index
    @calling_areas = policy_scope(CallingArea).all
    authorize @calling_areas

    render jsonapi: @calling_areas
  end

  def show
    render jsonapi: @calling_area
  end

  def create
    @calling_area = CallingArea.new(permitted_attributes(CallingArea))
    authorize @calling_area

    if @calling_area.save
      render jsonapi: @calling_area, status: :created, location: @calling_area
    else
      render jsonapi_errors: @calling_area.errors, status: :unprocessable_entity
    end
  end

  def update
    if @calling_area.update(permitted_attributes(@calling_area))
      render jsonapi: @calling_area
    else
      render jsonapi_errors: @calling_area.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @calling_area.destroy
  end

  private

  def set_calling_area
    @calling_area = CallingArea.find(params[:id])
    authorize @calling_area
  end
end
