# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class UploadsController < ApplicationController
  before_action :set_upload, only: %i[show download create destroy]
  before_action :set_attachable, only: :create
  before_action :authorize_upload, except: :create

  def show
    render jsonapi: @upload
  end

  def download
    send_data @upload.file.download, filename: @upload.file.filename.to_s, type: @upload.file.content_type
  end

  def create
    if @upload
      if @attachable
        attach(params[:file])
        authorize(@upload)
        render status: :created, jsonapi: @upload
      else
        head :not_found
      end
    else
      head :bad_request
    end
  end

  def destroy
    @upload.destroy
  end

  protected

  def set_upload
    raise 'Override me…'
  end

  def set_attachable
    raise 'Override me…'
  end

  def attach file
    @upload.file.attach file
  end

  private

  def authorize_upload
    authorize(@upload)
  end
end
