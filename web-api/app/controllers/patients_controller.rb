# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class PatientsController < ApplicationController
  before_action :pagination_params, only: :index
  before_action :set_patient, only: %i[show update]
  before_action :filter_params, only: :index
  deserializable_resource :patient, only: %i[update]

  def index
    unless pagination_requested? || filtering_requested?
      skip_authorization
      skip_policy_scope
      render jsonapi_errors: [{
          status: 400,
          title: 'Use pagination to fetch patients (max 100)',
          detail: 'Fetch of all the patients is forbidden for performance responsibility. Please use pagination for this purpose (size limited to 100)'
        }],
        status: :bad_request
      return
    end

    @raw_patients = Patient.fullname_asc

    @raw_patients = @raw_patients.of_matching_lastname(@last_name_filter) if @last_name_filter.present?
    @raw_patients = @raw_patients.of_matching_firstname(@first_name_filter) if @first_name_filter.present?
    @raw_patients = @raw_patients.born_on_date(@birthdate_filter) if @birthdate_filter.present?
    @raw_patients = @raw_patients.born_on_year(@birth_year_filter) if @birth_year_filter.present?

    @raw_patients = @raw_patients.page(@current_page).per(@size) if pagination_requested?

    @patients = policy_scope(@raw_patients)
    authorize @patients

    if pagination_requested?
      @total_pages = @raw_patients.total_pages
      @total_patients = @raw_patients.total_count

      render jsonapi: @patients,
        meta: {
          total_pages: @total_pages,
          total_patients: @total_patients
        },
        links: pagination_urls,
        include: [ logs: [:log_actions] ]
    else
      render jsonapi: @patients
    end
  end

  def show
    patient = Patient.find_by_id(params[:id])
    authorize patient
    render jsonapi: patient
  end

  def show
    render jsonapi: @patient
  end

  def update
    if @patient.update(permitted_attributes(@patient))
      if @patient.duplicated_patient_id_previous_change.present? &&
          @patient.duplicated_patient_id_previous_change.last.present?
        Patient.hand_over_consultations!([@patient.id])
      elsif (sanitized_changes = @patient.previous_changes.except(:updated_at)).present?
        log_actions_for [Log::Action.new({ action: :updated, changes: sanitized_changes })], @patient
      end
      render jsonapi: @patient
    else
      render jsonapi_errors: @patient.errors, status: :unprocessable_entity
    end
  end

  private

  def pagination_params
    if params[:page]
      @current_page = params[:page][:number]
      @size = params[:page][:size]
    end
  end

  def pagination_requested?
    @current_page.present? && @size.present?
  end

  def pagination_urls
    {
      self: "#{patients_url}?page[number]=#{@current_page}&page[size]=#{@size}",
      first: "#{patients_url}?page[number]=1&page[size]=#{@size}",
      next: (next_page = @patients.next_page) ? "#{patients_url}?page[number]=#{next_page}&page[size]=#{@size}" : nil,
      prev: (prev_page = @patients.prev_page) ? "#{patients_url}?page[number]=#{prev_page}&page[size]=#{@size}" : nil,
      last: "#{patients_url}?page[number]=#{@total_pages}&page[size]=#{@size}"
    }
  end

  def set_patient
    @patient = Patient.find(params[:id])
    authorize @patient
  end

  def filter_params
    if params[:filter]
      @last_name_filter = params[:filter][:last_name]
      @first_name_filter = params[:filter][:first_name]
      @birthdate_filter = params[:filter][:birthdate]
      @birth_year_filter = params[:filter][:birth_year]
    end
  end

  def filtering_requested?
    @last_name_filter.present?
  end
end
