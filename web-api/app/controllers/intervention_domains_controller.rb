# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class InterventionDomainsController < ApplicationController
  before_action :set_intervention_domain, only: [:show, :update, :destroy]
  deserializable_resource :intervention_domain, only: [:create, :update]

  def index
    @intervention_domains = policy_scope(InterventionDomain).all
    authorize @intervention_domains

    render jsonapi: @intervention_domains
  end

  def show
    render jsonapi: @intervention_domain
  end

  def create
    sanitize_roles
    @intervention_domain = InterventionDomain.new(permitted_attributes(InterventionDomain))
    authorize @intervention_domain

    if @intervention_domain.save
      render jsonapi: @intervention_domain, status: :created, location: @intervention_domain
    else
      render jsonapi_errors: @intervention_domain.errors, status: :unprocessable_entity
    end
  end

  def update
    sanitize_roles
    if @intervention_domain.update(permitted_attributes(@intervention_domain))
      render jsonapi: @intervention_domain
    else
      render jsonapi_errors: @intervention_domain.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @intervention_domain.destroy
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find(params[:id])
    authorize @intervention_domain
  end

  def sanitize_roles
    sanitize_roles_for(params[:intervention_domain][:role_ids])
  end
end
