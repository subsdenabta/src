# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update]
  deserializable_resource :user, only: [:create, :update]

  def index
    @users = policy_scope(User).all
    authorize @users

    render jsonapi: @users
  end

  def show
    render jsonapi: @user
  end

  def create
    sanitize_role
    @user = User.new(permitted_attributes(User))
    @user.password = SecureRandom.hex(10)
    authorize @user

    if @user.save
      begin
        PasswordMailer.created(@user).deliver
      rescue => e
        @user.destroy
        logger.error "***** ERROR: #{e}"
        render status: :bad_gateway
      else
        render jsonapi: @user, status: :created, location: @user
      end
    else
      render jsonapi_errors: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    sanitize_role
    if @user.update(permitted_attributes(@user))
      if @user.disabled_at.present?
        if @user.disabled_at > Time.zone.now
          DisableUserJob.set(wait_until: @user.disabled_at).perform_later(@user)
        else
          DisableUserJob.perform_now(@user)
        end
      end
      render jsonapi: @user
    else
      render jsonapi_errors: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def set_user
    @user = params[:id] == "me" ? current_user : User.find(params[:id])
    authorize @user
  end

  def sanitize_role
    role_title = params[:user][:role_id]
    params[:user][:role_id] = Role.find_by_title(role_title).try(:id)
  end
end
