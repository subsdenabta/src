# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :update, :destroy]
  before_action :set_intervention_domain, only: [:index, :create]
  deserializable_resource :schedule, only: [:create, :update]

  def index
    @schedules = policy_scope(@intervention_domain.schedules)
    authorize @schedules

    render jsonapi: @schedules
  end

  def show
    render jsonapi: @schedule
  end

  def create
    @schedule = if @intervention_domain
      @intervention_domain.schedules.build(permitted_attributes(Schedule))
    else
      Schedule.new(permitted_attributes(Schedule))
    end
    authorize @schedule

    if @schedule.save
      render jsonapi: @schedule, status: :created, location: @schedule
    else
      render jsonapi_errors: @schedule.errors, status: :unprocessable_entity
    end
  end

  def update
    if @schedule.update(permitted_attributes(@schedule))
      render jsonapi: @schedule
    else
      render jsonapi_errors: @schedule.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @schedule.destroy
  end

  private

  def set_intervention_domain
    @intervention_domain = InterventionDomain.find_by(id: params[:intervention_domain_id])
  end

  def set_schedule
    @schedule = Schedule.find(params[:id])
    authorize @schedule
  end

end
