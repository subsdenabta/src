# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AttachedFilesController < UploadsController

  private

  def set_upload
    @upload = if persisted_id = params[:id]
      AttachedFile.find(persisted_id)
    elsif new_type = params[:type]
      AttachedFile.of_type(new_type)
    end
  end

  def set_attachable
    @attachable = Appointment.find_by_id(params[:appointment_id]) ||
      FlowingConsultation.find_by_id(params[:flowing_consultation_id]) ||
      Certificate.find_by_id(params[:certificate_id]) ||
      User.find_by_id(params[:user_id])
  end

  def attach file
    if @attachable.is_a?(Certificate) &&
        @upload.is_a?(AttachedFile::SignedCertificate)
      extension = File.extname(file.original_filename)[1..-1]
      file.original_filename = @attachable.certificatable.signed_filename(extension)
    end

    @attachable.attached_files << @upload

    super
  end

end
