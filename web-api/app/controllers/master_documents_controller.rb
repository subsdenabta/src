# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class MasterDocumentsController < UploadsController

  protected

  def set_upload
    @upload = if persisted_id = params[:id]
      MasterDocument.find(persisted_id)
    else
      MasterDocument.new
    end
  end

  def set_attachable
    @attachable = CertificateTemplate.find(params[:certificate_template_id])
  end

  def attach file
    @attachable.master_document&.destroy
    @attachable.update(master_document: @upload)

    super
  end

end
