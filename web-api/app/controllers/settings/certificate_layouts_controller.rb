# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Settings::CertificateLayoutsController < ApplicationController

  before_action :set_settings_certificate_layout, only: [:show, :update]
  deserializable_resource :settings_certificate_layout, only: [:update]

  def show
    render jsonapi: @settings_certificate_layout
  end

  def update
    if @settings_certificate_layout.update(settings_certificate_layout_params)
      render jsonapi: @settings_certificate_layout
    else
      render jsonapi: @settings_certificate_layout.errors, status: :unprocessable_entity
    end
  end

  private

  def set_settings_certificate_layout
    @settings_certificate_layout = Settings::CertificateLayout.first_or_initialize
    authorize @settings_certificate_layout
  end

  def settings_certificate_layout_params
    permitted_attributes(@settings_certificate_layout)
  end

end
