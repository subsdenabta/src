# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class CertificateTemplatesController < ApplicationController
  before_action :set_certificate_template, only: [:show, :update, :destroy]
  deserializable_resource :certificate_template, only: [:create, :update]

  def index
    @certificate_templates = policy_scope(CertificateTemplate).all
    authorize @certificate_templates

    render jsonapi: @certificate_templates
  end

  def show
    render jsonapi: @certificate_template
  end

  def create
    sanitize_roles
    @certificate_template = CertificateTemplate.new(permitted_attributes(CertificateTemplate))
    authorize @certificate_template

    if @certificate_template.save
      render jsonapi: @certificate_template, status: :created, location: @certificate_template
    else
      render jsonapi_errors: @certificate_template.errors, status: :unprocessable_entity
    end
  end

  def update
    sanitize_roles
    if @certificate_template.update(permitted_attributes(@certificate_template))
      render jsonapi: @certificate_template
    else
      render jsonapi_errors: @certificate_template.errors, status: :unprocessable_entity
    end
  end

  private

  def set_certificate_template
    @certificate_template = CertificateTemplate.find(params[:id])
    authorize @certificate_template
  end

  def sanitize_roles
    if (role_ids = params[:certificate_template][:role_ids]).present?
      params[:certificate_template][:role_ids] = []
      role_ids.each do |role_title|
        params[:certificate_template][:role_ids] << Role.find_by_title(role_title).try(:id)
      end
    end
    params[:certificate_template][:role_ids]
  end
end
