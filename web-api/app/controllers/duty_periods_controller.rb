# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class DutyPeriodsController < ApplicationController
  before_action :set_consultation_type, only: [:index]
  before_action :set_duty_period, only: [:show, :update]
  skip_after_action :verify_policy_scoped, only: :index
  deserializable_resource :duty_period, only: [:update]

  def index
    authorize DutyPeriod

    scheduled_on = Date.parse(params[:filter][:scheduled_on])
    duty_periods = Schedule.duty_periods_for(@consultation_type, scheduled_on)

    render jsonapi: duty_periods
  end

  def show
    render jsonapi: @duty_period
  end

  def update
    if @duty_period.update(permitted_attributes(@duty_period))
      render jsonapi: @duty_period
    else
      render jsonapi_errors: @duty_period.errors, status: :unprocessable_entity
    end
  end

  private

  def set_consultation_type
    @consultation_type = ConsultationType.find(params[:filter][:consultation_type_id])
  end

  def set_duty_period
    @duty_period = DutyPeriod.find(params[:id])
    authorize @duty_period
  end
end
