# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ApplicationController < ActionController::API
  include Pundit

  before_action :doorkeeper_authorize!
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  rescue_from Pundit::NotAuthorizedError do
    render status: 403
  end

  def set_locale
    I18n.locale = current_user.locale || I18n.default_locale
  end

  private

  def current_user
    current_resource_owner
  end

  def current_resource_owner
    doorkeeper_token ? User.find(doorkeeper_token.resource_owner_id) : Visitor.new
  end

  def sanitize_roles_for(role_ids)
    role_ids&.map! { |r| Role.find_by_title(r).id }
  end

  def action_prefix(changes)
    if changes.first.nil?
      ""
    elsif changes.last.nil?
      "un"
    else
      "re"
    end
  end

  def log_actions_for(actions, item)
    LogJob.perform_later(item, current_user, Time.zone.now.iso8601, actions.map(&:to_json)) unless actions.empty?
  end
end
