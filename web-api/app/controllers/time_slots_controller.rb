# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class TimeSlotsController < ApplicationController
  before_action :set_time_slot, only: [:show, :update, :destroy]
  before_action :set_schedule, only: [:index, :create]
  deserializable_resource :time_slot, only: [:create, :update]

  def index
    @time_slots = policy_scope(@schedule.time_slots)
    authorize @time_slots

    render jsonapi: @time_slots
  end

  def show
    render jsonapi: @time_slot
  end

  def create
    @time_slot = if @schedule
      @schedule.time_slots.build(permitted_attributes(TimeSlot))
    else
      TimeSlot.new(permitted_attributes(TimeSlot))
    end
    authorize @time_slot

    if @time_slot.save
      render jsonapi: @time_slot, status: :created, location: @time_slot
    else
      render jsonapi_errors: @time_slot.errors, status: :unprocessable_entity
    end
  end

  def update
    if @time_slot.update(permitted_attributes(@time_slot))
      render jsonapi: @time_slot
    else
      render jsonapi_errors: @time_slot.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @time_slot.destroy
  end

  private

  def set_schedule
    @schedule = Schedule.find_by(id: params[:schedule_id])
  end

  def set_time_slot
    @time_slot = TimeSlot.find(params[:id])
    authorize @time_slot
  end

end
