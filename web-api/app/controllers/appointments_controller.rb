# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :update, :certificate_preview]
  before_action :set_locale, only: [:certificate_preview]
  deserializable_resource :appointment, only: [:create, :update]

  def index
    consultation_type = ConsultationType.find(params[:filter][:consultation_type_id])
    @appointments = policy_scope(Appointment).of_consultation_type(consultation_type)
    if params[:filter][:scheduled_on]
      scheduled_on = Date.parse(filter_param(:scheduled_on))
      @appointments = @appointments.scheduled_on(scheduled_on)
    else
      @appointments = @appointments.between(filter_param(:start_date), filter_param(:end_date)).
        onml_id_like(filter_param(:onml_id)).
        official_report_like(filter_param(:official_report)).
        patient_last_name_like(filter_param(:patient_last_name)).
        of_patient_age_range(filter_param(:patient_age_range).try(:split, '-')).
        of_patient_birthdate(filter_param(:patient_birthdate)).
        of_user_in_charge(filter_param(:user_in_charge_id)).
        of_calling_institution(filter_param(:calling_institution_id)).
        of_consultation_reason(filter_param(:consultation_reason_id)).
        updated_after(filter_param(:updated_after))

      if patient_gender = filter_param(:patient_gender)
        @appointments = @appointments.of_patient_gender(patient_gender.presence)
      end
    end
    authorize @appointments

    render jsonapi: @appointments, include: %i[consultation_reason certificate patient], expose: { user: current_user }
  end

  def show
    render jsonapi: @appointment, include: [ logs: [:log_actions] ], expose: { user: current_user }
  end

  def create
    @appointment = Appointment.new(permitted_attributes(Appointment))
    @appointment.build_officer(officer_attributes)
    @appointment.build_patient(patient_attributes) unless params[:appointment][:patient_id].present?
    @appointment.build_certificate
    authorize @appointment

    if @appointment.save
      postpone_previous_appointment
      log_actions_for [Log::Action.new("created")], @appointment
      render jsonapi: @appointment, status: :created, location: @appointment, expose: { user: current_user }
    else
      render jsonapi_errors: @appointment.errors, status: :unprocessable_entity
    end
  end

  def update
    @appointment.assign_attributes(permitted_attributes(@appointment))
    @appointment.officer.assign_attributes(officer_attributes)
    @appointment.build_patient(patient_attributes) unless params[:appointment][:patient_id].present?
    @appointment.certificate || @appointment.build_certificate # Still building on update for legacy appointments

    actions = []
    cancellation_changes = @appointment.changes[:cancelled_at]
    if cancellation_changes
      action_name = "#{action_prefix(cancellation_changes)}cancelled"
      actions << if action_name != "uncancelled"
        Log::Action.new({ action: action_name, source: @appointment.cancelled_by })
      else
        Log::Action.new(action_name)
      end
    end

    if @appointment.save
      log_actions_for actions, @appointment
      render jsonapi: @appointment, expose: { user: current_user }
    else
      render jsonapi_errors: @appointment.errors, status: :unprocessable_entity
    end
  end

  def certificate_preview
    send_data PDFKit.new(certificate_preview_as_html).to_pdf, type: 'application/pdf'
  end

  private

  def set_appointment
    @appointment = Appointment.find(params[:id])
    authorize @appointment
  end

  def officer_attributes
    {}.tap do |attrs|
      %w[officer_name officer_phone officer_email].each do |json_field|
        field = json_field.gsub(/officer_/,'')
        attrs[field] = params[:appointment][json_field]
      end
    end
  end

  def patient_attributes
    {}.tap do |attrs|
      %w[patient_first_name patient_last_name patient_birthdate patient_gender].each do |json_field|
        field = json_field.gsub(/patient_/,'')
        attrs[field] = params[:appointment][json_field]
      end
    end
  end

  def postpone_previous_appointment
    if postponed_by = params[:appointment][:previous_appointment_postponed_by]
      @appointment.previous_appointment.postpone(postponed_by)
      log_action = Log::Action.new({ action: "postponed", source: postponed_by })
      log_actions_for [log_action], @appointment.previous_appointment
    end
  end

  def filter_param(sym)
    params[:filter][sym]
  end

  def certificate_preview_as_html
    JsonCertificatesController.render 'preview',
      assigns: { appointment: @appointment }
  end

end
