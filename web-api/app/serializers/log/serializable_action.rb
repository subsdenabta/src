class Log::SerializableAction < SerializableBase
  type 'log_actions'

  attributes :name, :source, :changes

  belongs_to :log do
    linkage always: true
  end
end
