# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializableFlowingConsultation < SerializableBase
  extend JSONAPI::Serializable::Resource::ConditionalFields

  type 'flowing_consultations'

  attributes :called_at, :onml_id, :comment, :assigned_at, :validated_at, :cancelled_at, :cancelled_by, :concerns_minor

  %w[called_at assigned_at validated_at cancelled_at].each do |date_label|
    attribute date_label do
      iso8601_datehourminute @object.send(date_label)
    end
  end

  attribute :updated_at do
    iso8601_datehourminutesec @object.updated_at.utc
  end

  %w[name phone email lambda_user].each do |field|
    attribute "officer_#{field}" do
      @object.officer.try field
    end
  end

  belongs_to :intervention_domain do
    linkage always: true
  end

  belongs_to :consultation_type do
    linkage always: true
  end

  belongs_to :calling_institution do
    linkage always: true
  end

  belongs_to :assignee do
    linkage always: true
  end

  has_many :attached_files do
    linkage always: true
  end

  has_many :certificates do
    linkage always: true
  end

  belongs_to :patient do
    linkage always: true
  end
end
