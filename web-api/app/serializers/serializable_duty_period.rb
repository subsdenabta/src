# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class SerializableDutyPeriod < SerializableBase
  type 'duty_periods'

  %w[starts_at ends_at].each do |field|
    attribute field do
      iso8601_datehourminute @object.send field
    end
  end

  belongs_to :consultation_type do
    linkage always: true
  end

  has_many :assignees do
    linkage always: true
  end
end
