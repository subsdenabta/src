# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class ConsultationType < ApplicationRecord
  belongs_to :intervention_domain
  has_many :time_slots, dependent: :destroy
  has_many :duty_periods, through: :time_slots
  has_many :consultation_reasons, dependent: :destroy
  has_many :consultation_subtypes, dependent: :destroy
  has_many :consultation_type_assigned_roles, dependent: :destroy
  has_many :assigned_roles, through: :consultation_type_assigned_roles, source: :role
  has_many :consultation_type_complementary_consultation_types, dependent: :destroy
  has_many :complementary_consultation_types, through: :consultation_type_complementary_consultation_types, source: :complementary_consultation_type
  has_many :consultation_type_authorizations, dependent: :destroy
  has_many :roles, through: :consultation_type_authorizations

  validates :title, presence: true, uniqueness: { scope: :intervention_domain_id }
end
