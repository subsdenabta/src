# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Appointment < ApplicationRecord
  attribute :duration, :interval

  belongs_to :officer, optional: true, autosave: true
  belongs_to :patient, optional: true, autosave: true
  belongs_to :consultation_type
  has_one :intervention_domain, through: :consultation_type
  belongs_to :consultation_reason, optional: true
  belongs_to :calling_institution, optional: true
  belongs_to :consultation_subtype, optional: true
  belongs_to :user_in_charge, class_name: 'User', optional: true
  belongs_to :previous_appointment, class_name: 'Appointment', optional: true
  has_one :next_appointment, class_name: 'Appointment', foreign_key: :previous_appointment_id, dependent: :nullify
  has_one :certificate, as: :certificatable, autosave: true
  has_one :medical_file
  has_many :attached_files, as: :attachable
  has_many :complementary_appointments, class_name: 'Appointment', foreign_key: :originating_appointment_id
  belongs_to :originating_appointment, class_name: 'Appointment', optional: true

  enum cancelled_by: { patient: 1, umj: 2, opj: 3}

  default_scope { order(:starts_at) }
  scope :of_calling_institution, ->(institution_id) { institution_id.present? ? where(calling_institution_id: institution_id) : current_scope }
  scope :of_consultation_reason, ->(reason_id) { reason_id.present? ? where(consultation_reason_id: reason_id) : current_scope }
  scope :of_consultation_type, ->(type) { where(consultation_type: type) }
  scope :scheduled_on, ->(date) { where(starts_at: date.beginning_of_day..date.end_of_day)}
  scope :onml_id_like, ->(pattern) { pattern.present? ? where("LOWER(onml_id) like ?", "%#{pattern.downcase}%") : current_scope }
  scope :official_report_like, ->(pattern) { pattern.present? ? where("LOWER(official_report) like ?", "%#{pattern.downcase}%") : current_scope }
  scope :of_patient_gender, ->(gender) { joins(:patient).where(patients: {gender: gender}) }
  scope :of_patient_birthdate, ->(date) { date.present? ? joins(:patient).where(patients: {birthdate: date}) : current_scope }
  scope :of_user_in_charge, ->(user_id) { user_id.present? ? where(user_in_charge_id: user_id) : current_scope }
  scope :patient_last_name_like, ->(pattern) { pattern.present? ? joins(:patient).where("LOWER(patients.last_name) like ?", "#{pattern.downcase}%") : current_scope }
  scope :updated_after, ->(datetime) { datetime.present? ? where('updated_at > ?', datetime) : current_scope }

  after_save :generate_onml_id

  def self.between(floor, ceiling)
    floor_time = (floor.present? ? Date.parse(floor).beginning_of_day : nil)
    ceiling_time = (ceiling.present? ? Date.parse(ceiling).end_of_day : nil)

    if floor_time.present? && ceiling_time.present?
      self.where(starts_at: floor_time..ceiling_time)
    elsif floor_time.present?
      self.where("starts_at >= ?", floor_time)
    elsif ceiling_time.present?
      self.where("starts_at <= ?", ceiling_time)
    else
      all
    end
  end

  def self.of_patient_age_range(range)
    if range.present?
      range_floor = range[0]
      range_ceiling = range[1]
    end

    if range.present? && range_floor.present? && range_ceiling.present?
      birthdate_sql = "extract(year from age(appointments.starts_at, patients.birthdate)) between ? and ?"
      joins(:patient).where.not(patients: { birthdate: nil }).distinct.where("#{birthdate_sql}", range_floor, range_ceiling)
    else
      current_scope
    end
  end

  def allowed_for_user?(user)
    consultation_type.assigned_roles.include?(user.role)
  end

  def access_allowed_for_user?(user)
    consultation_type.roles.include?(user.role)
  end

  def postpone(origin)
    self.update({
      cancelled_at: Time.zone.now,
      cancelled_by: origin
    })
    self.attached_files.each do |attached_file|
      duplicate_attached_file = attached_file.dup
      duplicate_attached_file.file.attach(attached_file.file.blob)
      duplicate_attached_file.attachable = next_appointment
      duplicate_attached_file.save!
    end
  end

  def validated_certificate
    certificate.attached_files.where(type: "AttachedFile::ValidatedCertificate").last
  end

  def actual_medical_file
    originating_appointment ? originating_appointment.actual_medical_file : (medical_file || self.create_medical_file)
  end

  def signed_filename(extension)
    [
      [
        patient.fullname,
        onml_id,
        consultation_type.title,
        I18n.l(starts_at, format: :filename, locale: :fr)
      ].join(' - '),
      extension
    ].join('.')
  end

  private

  def generate_onml_id
    self.onml_id ||= if (identifier_prefix = intervention_domain.identifier_prefix).present?
      full_prefix = '%s%02i%02i' % [identifier_prefix, starts_at.year % 100, starts_at.month]

      sequence_name = "flowing_consultations_onml_id_#{full_prefix}_seq"
      create_sequence_and_get_val = <<-SQL
      CREATE SEQUENCE IF NOT EXISTS #{sequence_name} OWNED BY flowing_consultations.onml_id;
      SELECT nextval('#{sequence_name}');
      SQL

      rows = self.class.connection.execute(create_sequence_and_get_val)
      nextval = rows.first['nextval']
      onml_id = full_prefix + nextval.to_s

      self.update_columns(onml_id: onml_id) && onml_id
    end
  end
end
