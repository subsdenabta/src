# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class FlowingConsultation < ApplicationRecord

  TIPPING_DELAY = 12.hours

  belongs_to :intervention_domain
  belongs_to :officer, optional: true, autosave: true
  belongs_to :patient, optional: true, autosave: true
  belongs_to :consultation_type, optional: true
  belongs_to :calling_institution, optional: true
  belongs_to :assignee, class_name: "User", optional: true
  has_many :attached_files, as: :attachable
  has_many :certificates, as: :certificatable, autosave: true

  alias_attribute :user_in_charge, :assignee
  alias_attribute :starts_at, :assigned_at

  enum cancelled_by: {
    'custody-ended': 1,
    'sent-to-hospital': 2,
    'brought-before-the-courts': 3,
    'unconcerned': 4,
    'sent-to-morgue': 5,
    'moved-to-appointments': 6,
    'other': 7
  }

  scope :of_intervention_domain, ->(domain) { where(intervention_domain_id: domain.id) }
  scope :of_consultation_type, ->(type) { where(consultation_type_id: type.id) }
  scope :of_calling_institution, ->(institution) { where(calling_institution_id: institution.id) }
  scope :of_patient_gender, ->(gender) { joins(:patient).where(patients: {gender: gender}) }
  scope :patient_last_name_like, ->(pattern) { joins(:patient).where("LOWER(patients.last_name) like ?", "#{pattern.downcase}%") }
  scope :of_assignee, ->(user) { where(assignee_id: user.id)}
  scope :archived, -> { where("cancelled_at <= ?", FlowingConsultation::TIPPING_DELAY.ago).or(where("validated_at <= ?", FlowingConsultation::TIPPING_DELAY.ago)) }
  scope :active, -> { where.not(id: archived) }
  scope :last_updated, -> { order(updated_at: :desc).limit(1).first }

  validates :consultation_type, presence: { unless: :simple_message? }

  after_save :generate_onml_id

  def self.sort_with_instructions(sort_instructions)
    return current_scope unless sort_instructions

    order_instructions = []
    sort_instructions.split(',').each do |instruction|
      matches = instruction.match(/(-)?(\w*)/)
      way = matches[1] ? "DESC" : "ASC"
      order_instructions << "#{ matches[2] } #{ way }" if FlowingConsultation.new.has_attribute?(matches[2])
    end
    order(order_instructions.join(','))
  end

  def self.of_patient_age_range(range_floor, range_ceiling)
    birthyear_sql = "EXTRACT(year FROM flowing_consultations.called_at) - patients.birth_year BETWEEN ? AND ?"
    joins(:patient).where(birthyear_sql, range_floor, range_ceiling).distinct
  end

  def self.with_status(status)
    case status
    when "cancelled"
      where.not(cancelled_at: nil)
    when "validated"
      where.not(validated_at: nil)
    else
      current_scope
    end
  end

  def self.between(column, floor, ceiling)
    column = column.to_s
    return current_scope unless %w[called_at cancelled_at validated_at assigned_at].include?(column)

    if floor.present? && ceiling.present?
      self.where(column => floor..ceiling)
    elsif floor.present?
      self.where("#{column} >= ?", floor)
    elsif ceiling.present?
      self.where("#{column} <= ?", ceiling)
    else
      current_scope
    end
  end

  def simple_message?
    return officer&.lambda_user
  end

  def archived?
    tipping_date = FlowingConsultation::TIPPING_DELAY.ago
    cancelled_at.try(:<=, tipping_date) || validated_at.try(:<=, tipping_date)
  end

  def allowed_for_user?(user)
    user.has_role?(:physician)
  end

  def access_allowed_for_user?(user)
    intervention_domain.roles.include?(user.role)
  end

  def signed_filename(extension)
    [
      [
        patient.fullname,
        onml_id,
        consultation_type.title,
        I18n.l(Time.zone.now, format: :filename, locale: :fr)
      ].join(' - '),
      extension
    ].join('.')
  end

  private

  def generate_onml_id
    self.onml_id ||= if (identifier_prefix = intervention_domain.identifier_prefix).present?
      full_prefix = '%s%02i%02i' % [identifier_prefix, called_at.year % 100, called_at.month]

      sequence_name = "flowing_consultations_onml_id_#{full_prefix}_seq"
      create_sequence_and_get_val = <<-SQL
      CREATE SEQUENCE IF NOT EXISTS #{sequence_name} OWNED BY flowing_consultations.onml_id;
      SELECT nextval('#{sequence_name}');
      SQL

      rows = self.class.connection.execute(create_sequence_and_get_val)
      nextval = rows.first['nextval']
      onml_id = full_prefix + nextval.to_s

      self.update_columns(onml_id: onml_id) && onml_id
    end
  end

end
