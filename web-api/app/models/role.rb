# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class Role < ApplicationRecord

  # Order matters because of API requirements…
  ALL = [
    :administrator,
    :association,
    :nurse,
    :physician,
    :psychiatrist,
    :psychiatrist_expert,
    :psychologist,
    :psychologist_expert,
    :secretary,
    :switchboard_operator,
  ]

  has_many :domain_authorizations, dependent: :destroy
  has_many :intervention_domains, through: :domain_authorizations

  has_many :certificate_templates_roles
  has_many :certificate_templates, through: :certificate_templates_roles

  validates :title, presence: true, uniqueness: { case_sensitive: false }

  default_scope { order(title: :asc) }
end
