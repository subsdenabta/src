# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
class User < ApplicationRecord
  has_secure_password
  has_many :appointments
  has_many :attached_files, as: :attachable

  belongs_to :role

  validates :identifier, presence: true, uniqueness: { case_sensitive: false }
  validates :email, presence: true

  def has_role?(title)
    self.role == Role.find_by_title(title)
  end

  def connected?
    true
  end

  def name
    "#{last_name} #{first_name}"
  end

  def inactive?
    disabled_at&.past?
  end

  def locale
    'fr'
  end

end
