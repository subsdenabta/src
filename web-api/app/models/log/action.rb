class Log::Action

  attr_reader :id
  attr_reader :log

  def initialize(data, log = nil)
    @data = if data.is_a? Hash
      HashData.new(data)
    else
      BaseData.new(data)
    end

    # /!\ `a = b and c = d` != `a = b && c = d` /!\
    @log = log and @id = "#{@log.what_id}-#{@log.id}-#{name}"
  end

  delegate :name, :source, :changes, :to_json, to: :data

  private

  attr_reader :data

  class BaseData

    attr_reader :name
    attr_reader :source
    attr_reader :changes

    def initialize name
      @name = name
    end

    def to_json
      { action: name, source: source , changes: changes}.compact.to_json
    end

  end

  class HashData < BaseData

    def initialize hash
      hash.stringify_keys!
      @name = hash['action']
      @source = hash['source']
      @changes = hash['changes']
    end

  end

end
