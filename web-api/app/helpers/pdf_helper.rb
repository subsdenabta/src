# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#
module PdfHelper

  def check_or_warn(value)
    if value.present?
      if block_given?
        yield(value)
      else
        value
      end
    else
      content_tag :span, 'À RENSEIGNER', class: 'field-to-provide'
    end
  end

  def print_choice(available_choices, datum, other=nil)
    if datum == "other"
      other
    elsif available_choices.include?(datum)
      datum
    else
      available_choices.find { |item| item["value"] == datum }["text"]
    end
  end

  def print_multiple_choices(available_choices, data, other=nil)
    [].tap do |choices|
      data.each do |datum|
        choices << print_choice(available_choices, datum, other)
      end
      return choices.join(', ')
    end
  end

  def format_expression(data)
    data = data.to_s
    data.match?(/\A\d{4}-\d{2}-\d{2}\z/) ? I18n.l(Date.parse(data), locale: :fr) : data
  end

  def print_file(file, title=nil)
    title ||= file['name']
    if %r{^image/} =~ file['type']
      content_tag :figure do
        content_tag(:figcaption, title) +
        image_tag(file['content'], alt: title)
      end
    else
      content_tag :dl do
        content_tag(:dt, title) +
        content_tag(:dd, file['name'])
      end
    end
  end
end
