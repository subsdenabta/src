/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';

module('Acceptance | Login', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('visiting /login', async function(assert) {
    await visit('/login');

    assert.equal(currentURL(), '/login');
  });

  test('signing in as administrator with correct credentials', async function(assert) {
    await visit('/login');
    await fillIn('[data-test-identification] input', 'administrator');
    await fillIn('[data-test-password] input', 'administrator');
    await click('[data-test-submit-button]')

    assert.equal(currentURL(), '/');
  });

  test('signing in as administrator with wrong credentials', async function(assert) {
    await visit('/login');
    await fillIn('[data-test-identification] input', 'administrator');
    await fillIn('[data-test-password] input', 'password');
    await click('[data-test-submit-button]')

    assert.equal(currentURL(), '/login');
  });
});
