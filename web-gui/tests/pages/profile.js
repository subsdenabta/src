/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import {
  create,
  clickable,
  fillable,
  text
} from 'ember-cli-page-object';

export default create({
  mainMenu: clickable('[data-test-main-menu]'),
  profile: clickable('[data-test-profile] a'),
  editPassword: clickable('[data-test-edit-password]'),
  password: fillable('[data-test-password] input'),
  passwordErrors: text('[data-test-password] .paper-input-error', { multiple: true }),
  passwordConfirmation: fillable('[data-test-password-confirmation] input'),
  passwordConfirmationErrors: text('[data-test-password-confirmation] .paper-input-error', { multiple: true }),
  submit: clickable('[data-test-submit]'),
  success: text('div.alert-success')
});
