/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  setupPact,
  given,
  interaction,
  specifyMatchingRules
} from 'ember-cli-pact';
import { type } from 'ember-cli-pact/matchers';
import { authenticateSession } from 'ember-simple-auth/test-support';

module('Pact | web-api | Master document', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_master-documents'
  });

  hooks.beforeEach(async function() {
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  test('fetching a master document', async function(assert) {
    // Given
    given('a master document exists for certificate template 1');
    specifyMatchingRules({
      response: {
        body: {
          data: {
            attributes: {
              filename: type(),
              byte_size: type(),
              content_type: type()
            }
          }
        }
      }
    })
    // When
    let result = await interaction(() => this.store().findRecord('master-document', "1"));
    // Then
    assert.equal(result.get('id'), 1);
  });

});
