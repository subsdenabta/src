/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { run } from '@ember/runloop';
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  setupPact,
  given,
  interaction,
  specifyMatchingRules,
  getProvider
} from 'ember-cli-pact';
import { regex, type, arrayElements } from 'ember-cli-pact/matchers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import Response from 'ember-cli-mirage/response';
import moment from 'moment';

module('Pact | web-api | Certificates', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_certificates'
  });

  hooks.beforeEach(async function() {
    given('certificates exist');
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  test('fetching a certificate', async function(assert) {
    // Given
    specifyMatchingRules({
      response: {
        body: {
          data: {
            attributes: {
              questionnaire: type(),
              data: type(),
              checksum: type(),
              validated_at: regex(/\A([2-9][0-9]{3})-(0[0-9]|1[0-2])-([0-2][0-9]|3[0,1])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])Z\z/),
              delivered_at: regex(/\A([2-9][0-9]{3})-(0[0-9]|1[0-2])-([0-2][0-9]|3[0,1])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])Z\z/),
            },
            relationships: {
              flowing_consultation: {
                data: {
                  id: type()
                }
              },
              // see https://github.com/salsify/ember-cli-pact/issues/37
              attached_files: {
                data: arrayElements({
                  id: type()
                })
              }
            }
          }
        }
      }
    })
    // When
    let result = await interaction(() => this.store().findRecord('certificate', "1"));
    // Then
    assert.equal(result.get('id'), 1);
  });

  test('deleting a certificate', async function(assert) {
    // Given
    let certificate = await run(() => this.store().findRecord('certificate', '1'))
    // see https://github.com/salsify/ember-cli-pact/issues/32#issuecomment-534475272
    getProvider().server.delete('certificates/1', (schema, request) => {
      getProvider().interaction.recordRequest(request);

      return new Response(204, {'Content-Type': undefined}, '');
    })();
    // When
    await interaction(() => certificate.destroyRecord());
    // Then
    assert.ok(certificate.get('isDeleted'));
    assert.notOk(certificate.get('hasDirtyAttributes'));
    assert.notOk(certificate.get('isSaving'));
  })

  test('previewing a certificate as PDF', async function(assert) {
    // Given
    let certificate = await run(() => this.store().findRecord('certificate', '1'))
    // see https://github.com/salsify/ember-cli-pact/issues/32#issuecomment-534475272
    getProvider().server.get('certificates/1/preview', (schema, request) => {
      getProvider().interaction.recordRequest(request);

      return new Response(200, {'Content-Type': 'application/pdf', 'Content-Transfer-Encoding': 'binary'}, '')
    })();
    // When
    await interaction(() => certificate.preview());
    // Then
    // the pact si written...
    assert.ok(true)
  })

  test('updating a certificate', async function(assert) {
    // Given
    specifyMatchingRules({
      response: {
        body: {
          data: {
            attributes: {
              checksum: type()
            },
            relationships: {
              flowing_consultation: {
                data: {
                  id: type()
                }
              },
              // see https://github.com/salsify/ember-cli-pact/issues/37
              attached_files: {
                data: arrayElements({
                  id: type()
                })
              }
            }
          }
        }
      }
    })

    let now = moment("2019-10-14T12:04:10.123Z")
    let certificate = await run(() => {
      let promisedCertificate = this.store().findRecord('certificate', '1');

      promisedCertificate.then((certificate) => {
        certificate.set('validatedAt', now);
      })

      return promisedCertificate;
    })
    // When
    let result = await interaction(() => certificate.save());
    // Then
    assert.equal(result.get('validatedAt').toISOString(), "2019-10-14T12:04:00.000Z");
  });

  test('sending a certificate by mail', async function(assert) {
    // Given
    let certificate = await run(() => this.store().findRecord('certificate', '1'))
    // see https://github.com/salsify/ember-cli-pact/issues/32#issuecomment-534475272
    getProvider().server.patch('certificates/1/send_by_mail', (schema, request) => {
      getProvider().interaction.recordRequest(request);

      return new Response(204, {'Content-Type': undefined}, '');
    })();
    // When
    await interaction(() => certificate.sendByMail({recipient: 'moulin@pj.fr'}));
    // Then
    // the pact si written...
    assert.ok(true)
  })
});
