/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupPact, given, interaction } from 'ember-cli-pact';
import { authenticateSession } from 'ember-simple-auth/test-support';

module('Pact | web-api | Roles', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_roles'
  });

  hooks.beforeEach(async function() {
    given('roles exist');
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  test('fetching all roles', async function(assert) {
    // When
    let result = await interaction(() => this.store().findAll('role'));
    // Then
    assert.equal(result.get('length'), 10);
  });

  test('fetching one role', async function(assert) {
    // When
    let result = await interaction(() => this.store().findRecord('role', 'administrator'));
    // Then
    assert.equal(result.get('id'), 'administrator');
    assert.equal(result.get('medicalConfidentiality'), true);
  });
});
