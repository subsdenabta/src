/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test, skip } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  setupPact,
  given,
  interaction,
  specifyMatchingRules
} from 'ember-cli-pact';
import { type, regex, arrayElements } from 'ember-cli-pact/matchers';
import { authenticateSession } from 'ember-simple-auth/test-support';

module('Pact | web-api | Patients', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_patients'
  });

  hooks.beforeEach(async function() {
    given('patients exist');
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  skip('fetching all patients returns 400', async function(assert) {
    // Skip reason: returns 400 but keeps going on and carshes
    // When
    let result = await interaction(() => this.store().findAll('patient')).catch((error) => {
    // Then
      let api_error = error.errors[0]
      assert.equal(api_error["status"], 400)
    });
  });

  skip('fetching paginated patients is ok', async function(assert) {
    // skip reasons:
    // - https://github.com/salsify/ember-cli-pact/issues/37
    // - https://github.com/pact-foundation/pact-ruby/issues/197
    // When
    specifyMatchingRules({
      response: {
        body: {
          data: arrayElements({
            attributes: {
              last_name: type(),
              first_name: type(),
              birth_date: regex(/\A([1-2][0-9]{3})-(0[0-9]|1[0-2])-([0-2][0-9]|3[0,1])\z/),
              birth_year: type(),
            },
            id: type()
          }),
        }
      }
    })
    let result = await interaction(() => this.store().query('patient',
      { page: {
        number: 1,
        size: 10
        }
      }));
    // Then
    assert.equal(result.get('length'), 2);
  });
});
