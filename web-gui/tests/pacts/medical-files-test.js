/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import {
  setupPact,
  given,
  interaction,
  specifyMatchingRules
} from 'ember-cli-pact';
import { type } from 'ember-cli-pact/matchers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { run } from '@ember/runloop';

module('Pact | web-api | Medical files', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'web-api_medical-files'
  });

  hooks.beforeEach(async function() {
    given('a medical file exists');
    await authenticateSession({
      access_token: 'my_secret_token',
      token_type: 'bearer',
      expires_in: 7200
    });
  });

  test('fetching a medical file', async function(assert) {
    // Given
    specifyMatchingRules({
      response: {
        body: {
          data: {
            attributes: {
              address: type(),
              chaperon_name: type(),
              chaperon_quality: type(),
              phone_number: type(),
              placed: type(),
              actor_name: type(),
              obs_nurse: type(),
              obs_psychologist: type(),
              obs_general: type(),
              wrist_xray: type(),
              teeth_xray: type(),
              clavicle_xray: type(),
              std_test: type(),
              pre_therapy_test: type(),
              cga_test: type(),
              dna_test: type(),
              chemical_submission: type(),
              triple_therapy: type(),
              dressing: type(),
              psychological_impact_requested: type(),
              medical_obs: type(),
              case_history: type(),
              background: type(),
              treatment: type(),
              allergy: type(),
              clinical_examination: type(),
              complementary_info: type(),
            }
          }
        }
      }
    })
    // When
    let result = await interaction(() => this.store().findRecord('medical-file', "1"));
    // Then
    assert.equal(result.get('id'), 1);
    //assert.equal(result.get('appointment.id'), 1105211);
  });

  test('updating a medical file', async function(assert) {
    // Given
    specifyMatchingRules({
      response: {
        body: {
          data: {
            attributes: {
              chaperon_name: type(),
              chaperon_quality: type(),
              phone_number: type(),
              placed: type(),
              actor_name: type(),
              obs_nurse: type(),
              obs_psychologist: type(),
              obs_general: type(),
              wrist_xray: type(),
              teeth_xray: type(),
              clavicle_xray: type(),
              std_test: type(),
              pre_therapy_test: type(),
              cga_test: type(),
              dna_test: type(),
              chemical_submission: type(),
              triple_therapy: type(),
              dressing: type(),
              psychological_impact_requested: type(),
              medical_obs: type(),
              case_history: type(),
              background: type(),
              treatment: type(),
              allergy: type(),
              clinical_examination: type(),
              complementary_info: type(),
            }
          }
        }
      }
    })
    let newAddress = 'Chez moi'
    let medicalFile = await run(() => {
      let promisedMedicalFile = this.store().findRecord('medical-file', '1');

      promisedMedicalFile.then((medicalFile) => {
        medicalFile.set('address', newAddress);
      })

      return promisedMedicalFile;
    })
    // When
    let result = await interaction(() => medicalFile.save())
    // Then
    assert.equal(result.get('address'), newAddress);
  })
});
