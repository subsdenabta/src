/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { providerState } from 'ember-cli-pact';

providerState('roles exist', (server) => {
  server.create('role', 'administrator');
  server.create('role', 'association');
  server.create('role', 'nurse');
  server.create('role', 'physician');
  server.create('role', 'psychiatrist');
  server.create('role', 'psychiatrist_expert');
  server.create('role', 'psychologist');
  server.create('role', 'psychologist_expert');
  server.create('role', 'secretary');
  server.create('role', 'switchboard_operator');
});
