/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupIntl } from 'ember-intl/test-support';
import moment from 'moment';

module('Unit | Model | log', function(hooks) {
  setupTest(hooks);
  setupIntl(hooks, 'fr-fr');

  hooks.beforeEach(function() {
    moment.tz.setDefault("Europe/Paris");
  });

  hooks.afterEach(function() {
    moment.tz.setDefault();
  });

  test('appointment cancellation log displays correctly', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let logAction = store.createRecord('log-action', { name: 'cancelled', source: 'patient' })
    let log = store.createRecord('log', { who: 'Greg House', occuredAt: '2021-12-10T10:36Z', actions: [logAction] });
    // When
    let humanTitle = log.get('humanTitle')
    // Then
    assert.equal(humanTitle, 'Annulé à la demande du patient le 10/12/2021 à 11:36 par Greg House');
  });

  test('appointment postpone log displays correctly', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let logAction = store.createRecord('log-action', { name: 'postponed', source: 'opj' })
    let log = store.createRecord('log', { who: 'Greg House', occuredAt: '2021-12-10T10:36Z', actions: [logAction] });
    // When
    let humanTitle = log.get('humanTitle')
    // Then
    assert.equal(humanTitle, 'Reporté à la demande de l\'OPJ le 10/12/2021 à 11:36 par Greg House');
  });

  test('patient modification log displays correctly', function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let logAction = store.createRecord('log-action', { name: 'updated', changes: {first_name: ['Jean', 'Juan'], birth_year: [1964, 1965] } })
    let log = store.createRecord('log', { who: 'Greg House', occuredAt: '2021-12-10T10:36Z', actions: [logAction] });
    // When
    let humanTitle = log.get('humanTitle')
    let humanChanges = log.get('humanChanges')
    // Then
    assert.equal(humanTitle, 'Modifié le 10/12/2021 à 11:36 par Greg House');
    assert.equal(humanChanges, 'first_name : Jean => Juan // birth_year : 1964 => 1965');
  });
});
