/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, skip } from 'qunit';
import { setupTest } from 'ember-qunit';
import { setupPact, given, interaction } from 'ember-cli-pact';

module('Unit | Controller | login', function(hooks) {
  setupTest(hooks);
  setupPact(hooks, {
    providerName: 'WEB API Authentication'
  });

  hooks.beforeEach(function() {
    given('users exist');
  });

  skip('it authenticates a user with valid credentials', async function(assert) {
    // Given
    let controller = this.owner.lookup('controller:login');
    controller.set('identification', 'administrator');
    controller.set('password', 'administrator');
    // When
    await interaction(() => controller.send('authenticate'));
    // Then
    assert.ok(controller.get('session.isAuthenticated'));
  });
});
