/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | patients/index', function(hooks) {
  setupTest(hooks);

  test('model is a composed of patients and metadata', function(assert) {
    // Given
    let controller = this.owner.lookup('controller:patients/index');
    let model = {
      "patients": ['some', 'thing'],
      "meta": { "just": "some", "other": "things"}
    };
    // When
    controller.set('model', model);
    // Then
    assert.strictEqual(model["patients"], controller.get('patients'));
    assert.strictEqual(model["meta"], controller.get('meta'));
  });

  test('allPages is an array containing tootalPages entries', function(assert) {
    // Given
    let controller = this.owner.lookup('controller:patients/index');
    let model = {
      "meta": { "totalPages": 4}
    };
    // When
    controller.set('model', model);
    // Then
    assert.strictEqual([1,2,3,4].toString(), controller.get('allPages').toString());
  });

  test('numberOfRows returns the number of patients returned by api', function(assert) {
    // Given
    let controller = this.owner.lookup('controller:patients/index');
    let model = {
      "patients": ['some', 'thing']
    };
    // When
    controller.set('model', model);
    controller.set('size', 20)
    // Then
    assert.strictEqual(2, controller.get('numberOfRows'));
  });
});
