/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Transform | datehourminute', function(hooks) {
  setupTest(hooks);

  test('#serialize', function(assert) {
    let transform = this.owner.lookup('transform:datehourminute');

    assert.equal(transform.serialize(null), null)
    assert.equal(transform.serialize(undefined), undefined)
    assert.equal(transform.serialize(new Date("2000-06-15T14:42+02:00")), "2000-06-15T12:42Z")
    assert.equal(transform.serialize(new Date("2000-06-15T25:42+02:00")), "Invalid date")
  });

  test('#deserialize', function(assert) {
    let transform = this.owner.lookup('transform:datehourminute');

    assert.equal(transform.deserialize(null), null)
    assert.equal(transform.deserialize(undefined), undefined)
    assert.deepEqual(transform.deserialize("2000-06-15T12:42Z"), new Date("2000-06-15T14:42+02:00"))
    assert.equal(transform.deserialize("2000-06-15T25:42Z"), "Invalid Date")
  });
});
