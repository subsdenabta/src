/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import moment from 'moment';

module('Unit | Transform | date only', function(hooks) {
  setupTest(hooks);

  test('#serialize', function(assert) {
    let transform = this.owner.lookup('transform:date-only');

    assert.equal(transform.serialize(null), null)
    assert.equal(transform.serialize(undefined), undefined)
    assert.equal(transform.serialize(moment("2000-06-15").toDate()), "2000-06-15")
    assert.equal(transform.serialize(moment("2000-02-30").toDate()), "Invalid date")
  });

  test('#deserialize', function(assert) {
    let transform = this.owner.lookup('transform:date-only');

    assert.equal(transform.deserialize(null), null)
    assert.equal(transform.deserialize(undefined), undefined)
    assert.deepEqual(transform.deserialize("2000-06-15"), moment("2000-06-15").toDate())
    assert.equal(transform.deserialize("2000-02-30"), "Invalid Date")
  });
});
