/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import 'ember-qunit';
import { run } from '@ember/runloop'
import helpers from './helpers'

module('Unit | Ability | appointment', function(hooks) {
  setupTest(hooks)

  let subject = function (appointment) {
    let ability = this.owner.lookup('ability:appointment');
    ability.set('model', appointment);

    return ability;
  }

  let createAppointment = function (...roles) {
    let store = this.owner.lookup('service:store')
    let consultationType = run(() => { return store.createRecord('consultation-type')})
    consultationType.get('roles').pushObjects(roles)
    let appointment = run(() => { return store.createRecord('appointment', { consultationType: consultationType})});

    return appointment;
  }

  test('administrator rights regarding a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'administrator');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.ok(ability.get('canConfirm'))
    assert.ok(ability.get('canFinalize'))
    assert.ok(ability.get('canSetRequisitionStatus'))
    assert.ok(ability.get('canEditArchived'))
    assert.ok(ability.get('canEditCertificate'))
  })

  test('association rights', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'association');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
  })

  test('nurse rights regarding a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'nurse');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.ok(ability.get('canConfirm'))
    assert.ok(ability.get('canFinalize'))
    assert.ok(ability.get('canSetRequisitionStatus'))
    assert.ok(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
  })

  test('physician rights regarding a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'physician');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.notOk(ability.get('canConfirm'))
    assert.ok(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
  })

  test('psychiatrist rights a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'psychiatrist');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
    assert.ok(ability.get('canPostpone'))
  })

  test('psychiatrist expert rights a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'psychiatrist_expert');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canCreate'))
    assert.notOk(ability.get('canEdit'))
    assert.notOk(ability.get('canCancel'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
    assert.notOk(ability.get('canPostpone'))
  })

  test('psychologist rights', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'psychologist');
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
  })

  test('psychologist expert rights a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'psychologist_expert');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
    assert.ok(ability.get('canPostpone'))
  })

  test('secretary rights regarding a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'secretary');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canCreate'))
    assert.ok(ability.get('canEdit'))
    assert.ok(ability.get('canCancel'))
    assert.ok(ability.get('canPostpone'))
    assert.ok(ability.get('canConfirm'))
    assert.ok(ability.get('canFinalize'))
    assert.ok(ability.get('canSetRequisitionStatus'))
    assert.ok(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
  })

  test('switchboard_operator rights a given appointment', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'switchboard_operator');
    let appointment = createAppointment.call(this, role)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canCreate'))
    assert.notOk(ability.get('canEdit'))
    assert.notOk(ability.get('canCancel'))
    assert.notOk(ability.get('canConfirm'))
    assert.notOk(ability.get('canFinalize'))
    assert.notOk(ability.get('canSetRequisitionStatus'))
    assert.notOk(ability.get('canEditArchived'))
    assert.notOk(ability.get('canEditCertificate'))
    assert.notOk(ability.get('canPostpone'))
  })

  test('user can consult and edit the certificate of an appointment she is in charge of', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'physician');
    let appointment = createAppointment.call(this)
    run(() => { appointment.set('userInCharge', this.owner.lookup('service:current-user').get('user')) })

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canEditCertificate'))
    assert.ok(ability.get('canConsultCertificate'))
  })

  test('user with medical confidentiality role can consult the certificate of any appointment and edit medical file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.ok(ability.get('canConsultCertificate'))
    assert.ok(ability.get('canEditMedicalFile'))
  })

  test('user without medical confidentiality role cannot consult the certificate of any appointment nor edit medical file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', false);
    let appointment = createAppointment.call(this)

    let ability = subject.call(this, appointment);

    assert.notOk(ability.get('canConsultCertificate'))
    assert.notOk(ability.get('canEditMedicalFile'))
  })
});
