/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import 'ember-qunit';
import { run } from '@ember/runloop'
import helpers from './helpers'

module('Unit | Ability | attached-file', function(hooks) {
  setupTest(hooks)

  let subject = function (attachedFile) {
    let ability = this.owner.lookup('ability:attached-file');
    ability.set('model', attachedFile);

    return ability;
  }

  let createAttachedFile = function (type = 'any', userInCharge = false) {
    let store = this.owner.lookup('service:store')
    let attachable = run(() => { return store.createRecord('attachable')})
    if (userInCharge === true ) {
      run(() => { attachable.set('userInCharge', this.owner.lookup('service:current-user').get('user')) })
    }
    let attachedFile = run(() => { return store.createRecord('attached-file', { type: type, attachable: attachable })})

    return attachedFile;
  }

  test('user with medical confidentiality role can download any', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let attachedFile = createAttachedFile.call(this)

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDownload'))
  })

  test('user without medical confidentiality role cannot download any', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', false);
    let attachedFile = createAttachedFile.call(this)

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDownload'))
  })

  test('secretary can validate signed certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary');
    let attachedFile = createAttachedFile.call(this, 'signed-certificate')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canValidate'))
  })

  test('nurse can validate signed certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse');
    let attachedFile = createAttachedFile.call(this, 'signed-certificate')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canValidate'))
  })

  test('user in charge can validate signed certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'physician');
    let attachedFile = createAttachedFile.call(this, 'signed-certificate', true)

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canValidate'))
  })

  test('administrator can destroy any attached file except validated certificate', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');
    let attachedFile = createAttachedFile.call(this)

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('administrator cannot destroy validated certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');
    let attachedFile = createAttachedFile.call(this, 'validated-certificate')

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })

  test('secretary can destroy any standard attached file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary', true);
    let attachedFile = createAttachedFile.call(this, 'standard')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('secretary can destroy any medical attached file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary', true);
    let attachedFile = createAttachedFile.call(this, 'medical-content')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('secretary can destroy any signed certificate', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary', true);
    let attachedFile = createAttachedFile.call(this, 'signed-certificate')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('secretary cannot destroy validated certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary', true);
    let attachedFile = createAttachedFile.call(this, 'validated-certificate')

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })

  test('nurse can destroy any standard attached file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse', true);
    let attachedFile = createAttachedFile.call(this, 'standard')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('nurse can destroy any medical attached file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse', true);
    let attachedFile = createAttachedFile.call(this, 'medical-content')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('nurse can destroy any signed certificate', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse', true);
    let attachedFile = createAttachedFile.call(this, 'signed-certificate')

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('nurse cannot destroy validated certificates', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse', true);
    let attachedFile = createAttachedFile.call(this, 'validated-certificate')

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })

  test('user with medical confidentiality can destroy an attached file if she is in charge', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let attachedFile = createAttachedFile.call(this, 'any', true)

    let ability = subject.call(this, attachedFile);

    assert.ok(ability.get('canDestroy'))
  })

  test('user with medical confidentiality cannot destroy a validated certificate if she is in charge', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let attachedFile = createAttachedFile.call(this, 'validated-certificate', true)

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })

  test('user with medical confidentiality cannot destroy an attached file if she is not in charge', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let attachedFile = createAttachedFile.call(this, 'any', false)

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })

  test('user without medical confidentiality cannot destroy any attached file', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'any', true);
    let attachedFile = createAttachedFile.call(this, 'any')

    let ability = subject.call(this, attachedFile);

    assert.notOk(ability.get('canDestroy'))
  })
});
