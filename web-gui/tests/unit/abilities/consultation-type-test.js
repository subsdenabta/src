/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import 'ember-qunit';
import { run } from '@ember/runloop'
import helpers from './helpers'

module('Unit | Ability | consultation-type', function(hooks) {
  setupTest(hooks)

  let subject = function () {
    return this.owner.lookup('ability:consultation-type')
  }

  test('administrator rights', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');

    let ability = subject.call(this);

    assert.ok(ability.get('canAssignUsers'))
  })

  test('secretary rights', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'secretary')

    let ability = subject.call(this);

    assert.ok(ability.get('canAssignUsers'))
  })

  test('nurse rights', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'nurse')

    let ability = subject.call(this);

    assert.ok(ability.get('canAssignUsers'))
  })

  test('authenticated user with granted right can assign users to duty periods', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this)

    let consultationType = run(() => { return this.owner.lookup('service:store').createRecord('consultation-type')})
    consultationType.get('assignedRoles').pushObject(role)

    let ability = subject.call(this);
    ability.set('consultationType', consultationType)

    assert.ok(ability.get('canAssignUsers'))
  })

  test('authenticated user without granted right cannot assign users to duty periods', function(assert) {
    helpers.setCurrentUserForRole.call(this)

    let consultationType = run(() => { return this.owner.lookup('service:store').createRecord('consultation-type')})

    let ability = subject.call(this);
    ability.set('consultationType', consultationType)

    assert.notOk(ability.get('canAssignUsers'))
  })

  test('user with granted access can consult [archived] appointments', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this)

    let consultationType = run(() => { return this.owner.lookup('service:store').createRecord('consultation-type')})
    consultationType.get('roles').pushObject(role)

    let ability = subject.call(this);
    ability.set('consultationType', consultationType)

    assert.ok(ability.get('canConsultAppointments'))
    assert.ok(ability.get('canConsultArchivedAppointments'))
  })

  test('user without granted access cannot consult [archived] appointments', function(assert) {
    helpers.setCurrentUserForRole.call(this)

    let consultationType = run(() => { return this.owner.lookup('service:store').createRecord('consultation-type')})

    let ability = subject.call(this);
    ability.set('consultationType', consultationType)

    assert.notOk(ability.get('canConsultAppointments'))
    assert.notOk(ability.get('canConsultArchivedAppointments'))
  })
});
