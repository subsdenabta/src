/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import 'ember-qunit';
import { run } from '@ember/runloop'
import helpers from './helpers'

module('Unit | Ability | intervention-domain', function(hooks) {
  setupTest(hooks)

  let subject = function (interventionDomain) {
    let ability = this.owner.lookup('ability:intervention-domain');
    ability.set('model', interventionDomain);

    return ability;
  }

  let createInterventionDomain = function (...roles) {
    let interventionDomain = run(() => { return this.owner.lookup('service:store').createRecord('intervention-domain')});
    interventionDomain.get('roles').pushObjects(roles);

    return interventionDomain;
  }

  test('administrator can configure', function(assert) {
    helpers.setCurrentUserForRole.call(this, 'administrator');

    let ability = subject.call(this);

    assert.ok(ability.get('canConfigure'))
  })

  test('other user cannot configure', function(assert) {
    helpers.setCurrentUserForRole.call(this);

    let ability = subject.call(this);

    assert.notOk(ability.get('canConfigure'))
  })

  // Flowing consultations

  test('user with granted access can consult flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this);
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canConsultFlowingConsultations'))
  })

  test('user without granted access cannot consult flowing consultations', function(assert) {
    helpers.setCurrentUserForRole.call(this);
    let interventionDomain = createInterventionDomain.call(this)

    let ability = subject.call(this, interventionDomain);

    assert.notOk(ability.get('canConsultFlowingConsultations'))
  })

  test('administrator with granted access can manage flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'administrator');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canManageFlowingConsultations'))
  })

  test('secretary with granted access can manage flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'secretary');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canManageFlowingConsultations'))
  })

  test('nurse with granted access can manage flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'nurse');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canManageFlowingConsultations'))
  })

  test('physician with granted access can manage flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'physician');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canManageFlowingConsultations'))
  })

  test('switchboard_operator with granted access can manage flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'switchboard_operator');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canManageFlowingConsultations'))
  })

  // Archived flowing consultations

  test('administrator with granted access can consult and edit archived flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'administrator');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canConsultArchivedFlowingConsultations'))
    assert.ok(ability.get('canEditArchivedFlowingConsultations'))
  })

  test('physician with granted access can consult archived flowing consultations but cannot edit any', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'physician');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canConsultArchivedFlowingConsultations'))
    assert.notOk(ability.get('canEditArchivedFlowingConsultations'))
  })

  test('secretary with granted access can consult and edit archived flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'secretary');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canConsultArchivedFlowingConsultations'))
    assert.ok(ability.get('canEditArchivedFlowingConsultations'))
  })

  test('nurse with granted access can consult and edit archived flowing consultations', function(assert) {
    let role = helpers.setCurrentUserForRole.call(this, 'nurse');
    let interventionDomain = createInterventionDomain.call(this, role)

    let ability = subject.call(this, interventionDomain);

    assert.ok(ability.get('canConsultArchivedFlowingConsultations'))
    assert.ok(ability.get('canEditArchivedFlowingConsultations'))
  })
});
