/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | format-phone', function(hooks) {
  setupRenderingTest(hooks);

  test('it handles French numbers', async function(assert) {
    this.set('inputValue', '0102030405, 0706050403 (mère)');

    await render(hbs`{{format-phone inputValue}}`);

    assert.dom(this.element).hasText('01.02.03.04.05, 07.06.05.04.03 (mère)');
  });

  test('it handles foreign numbers', async function(assert) {
    this.set('inputValue', '+44 791183319932');

    await render(hbs`{{format-phone inputValue}}`);

    assert.dom(this.element).hasText('+44 79.11.83.31.9932');
  });
});
