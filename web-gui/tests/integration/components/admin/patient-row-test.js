/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | admin/patient-row', function(hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    // Set any properties with this.set('myProperty', 'value');
    this.set('patient', store.createRecord('patient', {lastName: 'Rella', firstName: 'Cindy'}));
    // Handle any actions with this.set('myAction', function(val) { ... });

    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('td:nth-of-type(1)')).hasText('t:patient.gender.abbreviated.undefined:()');
    assert.dom(this.element.querySelector('td:nth-of-type(2)')).hasText('RELLA Cindy');
  });

  test('it renders the birthdate', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    // Set any properties with this.set('myProperty', 'value');
    this.set('patient', store.createRecord('patient', {birthdate: '2015-05-19', birthYear: 2015}));
    // Handle any actions with this.set('myAction', function(val) { ... });

    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('td:nth-of-type(3)')).hasText('19/05/2015');
  });

  test('it renders the birth year', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    // Set any properties with this.set('myProperty', 'value');
    this.set('patient', store.createRecord('patient', {birthYear: 2015}));
    // Handle any actions with this.set('myAction', function(val) { ... });

    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('td:nth-of-type(3)')).hasText('2015');
  });

  test('it renders a link when duplicate suspected', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let duplicatedPatient = store.createRecord('patient', {id: 1});
    this.set('patient', store.createRecord('patient', {id: 2, probablyDuplicatedPatient: duplicatedPatient}));
    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('td[data-test-actions] a')).hasText('Doublon ?');
  });

  test('it indicates when duplicated patient was merged', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    let duplicatedPatient = store.createRecord('patient', {id: 1});
    this.set('patient', store.createRecord('patient', {id: 2, duplicatedPatient: duplicatedPatient}));
    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.dom(this.element.querySelector('td[data-test-actions]')).hasText('Doublon fusionné !');
  });

  test('it renders a link to edit a patient', async function(assert) {
    // Given
    let store = this.owner.lookup('service:store');
    this.set('patient', store.createRecord('patient'));
    // When
    await render(hbs`<Admin::PatientRow @patient={{patient}} />`);

    // Then
    assert.equal(this.element.querySelector('td[data-test-actions] a').getAttribute('title'), 'Modifier');
  });
});
