/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | fa-icon', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders a solid house with an empty title', async function(assert) {
    await render(hbs`<FaIcon @icon="house" />`);

    assert.equal(this.element.querySelector('i').getAttribute('class'), 'fas fa-house');
    assert.equal(this.element.querySelector('i').getAttribute('title'), '');
  });

  test('it renders a solid house with a custom title', async function(assert) {
    await render(hbs`<FaIcon @icon="house" @title="This is my house!" />`);

    assert.equal(this.element.querySelector('i').getAttribute('class'), 'fas fa-house');
    assert.equal(this.element.querySelector('i').getAttribute('title'), 'This is my house!');
  });

  test('it renders a regular house', async function(assert) {
    await render(hbs`<FaIcon @icon="house" @prefix="far" />`);

    assert.equal(this.element.querySelector('i').getAttribute('class'), 'far fa-house');
  });

  test('it renders the font-awesome brand', async function(assert) {
    await render(hbs`<FaIcon @icon="font-awesome" @prefix="fab" />`);

    assert.equal(this.element.querySelector('i').getAttribute('class'), 'fab fa-font-awesome');
  });
});
