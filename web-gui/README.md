# web-gui

This README outlines the details of collaborating on this Ember application.

* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

Before each followng command attach tot the *web-gui* container : `docker-compose exec web-gui bash`

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Linting

* `yarn lint`
* `yarn lint:fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

### Updating EmberJS

- [dev] connect to *web-gui* container : `make connect-gui`
- [web-gui] update `ember-cli-update` to latest version : `yarn add ember-cli-update`
- [web-gui] update `ember-cli` & co to specific version : `ember-cli-update --to 3.19`
- [dev] restart *web-gui* container : `make restart-gui`
- [dev] launch tests : `make test-gui && make test-uat`
- [dev]  `git commit -am 'Update EmberJS to latest 3.19.x version.'`
- [dev] restart *web-api* container : `make restart-api` (to cleanup database)
- [web-gui] run codemods : `ember-cli-update run-codemods --to 3.19`
- [dev] launch tests : `make test-gui && make test-uat`
- [dev]  `git commit -am 'Run codemods for latest EmberJS 3.19.x version.'`

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
