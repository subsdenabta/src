/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const envIsProduction = (process.env.EMBER_ENV === 'production');
const nodeSass = require('node-sass');
const fs = require('fs');

module.exports = function (defaults) {
  let app = new EmberApp(defaults, {
    sassOptions: {
      extension: 'sass',
      sourceMapEmbed: !envIsProduction,
      implementation: nodeSass,
      functions: {
        toolbarColor: function() {
          let rgbColor = process.env.TOOLBAR_COLOR || '009999';
          let argbColor = '0xFF' + rgbColor;
          return new nodeSass.types.Color(parseInt(argbColor));
        }
      }
    },

    autoprefixer: {
      enabled: true,
      cascade: true,
      sourcemap: !envIsProduction
    },

    'ember-bootstrap': {
      bootstrapVersion: 4,
      importBootstrapCSS: false
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  app.import('node_modules/survey-jquery/survey.min.css');
  app.import('node_modules/survey-jquery/survey.jquery.min.js');
  app.import('node_modules/moment/locale/fr.js');

  // FontAwesome
  let FAFontsPath = 'vendor/fontawesome-free/webfonts/'
  let FAFonts = fs.readdirSync(FAFontsPath);
  FAFonts.forEach( (font) => {
    app.import(FAFontsPath + font, { destDir: 'assets/webfonts'})
  })

  return app.toTree();
};
