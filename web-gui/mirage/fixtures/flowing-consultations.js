/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    "id": "11",
    "calledAt": "2018-05-21T21:42Z",
    "officerName": "Commissaire Swan Laurence",
    "officerPhone": "0607080900",
    "officerEmail": "slaurence@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Denny",
    "patientLastName": "Duquette",
    "patientBirthYear": 1966,
    "comment": null,
    "concerns_minor": false,
    "assignedAt":  "2018-05-21T22:22Z",
    "validatedAt":  "2018-05-22T00:23Z",
    "interventionDomainId": "2",
    "consultationTypeId": "21",
    "callingInstitutionId": "11",
    "assigneeId": "2"
  },
  {
    "id": "12",
    "calledAt": "2018-05-21T23:42Z",
    "officerName": "Commissaire Jules Maigret",
    "officerPhone": "0617181910",
    "officerEmail": "jmaigret@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Monsieur",
    "patientLastName": "Argan",
    "patientBirthYear": 1973,
    "comment": null,
    "concerns_minor": false,
    "assignedAt":  "2018-05-21T23:52Z",
    "interventionDomainId": "2",
    "consultationTypeId": "21",
    "callingInstitutionId": "11",
    "assigneeId": "2"
  },
  {
    "id": "13",
    "calledAt": "2018-05-22T03:42Z",
    "officerName": "Capitaine Frédéric Caïn",
    "officerPhone": "0627282920",
    "officerEmail": "fcain@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Franck",
    "patientLastName": "Carsenti",
    "patientBirthYear": 1978,
    "comment": null,
    "concerns_minor": true,
    "interventionDomainId": "2",
    "consultationTypeId": "22",
    "callingInstitutionId": "11"
  },
  {
    "id": "14",
    "calledAt": "2018-05-22T04:42Z",
    "officerName": "Agent spécial Dale Cooper",
    "officerPhone": "0637383930",
    "officerEmail": "dcooper@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Donna",
    "patientLastName": "Hayward",
    "patientBirthYear": 1990,
    "comment": null,
    "concerns_minor": false,
    "cancelledAt": "2018-05-22T05:23Z",
    "interventionDomainId": "2",
    "consultationTypeId": "21",
    "callingInstitutionId": "11"
  },
  {
    "id": "15",
    "calledAt": "2018-04-22T04:42Z",
    "officerName": "Agent spécial Dale Cooper",
    "officerPhone": "0637383930",
    "officerEmail": "dcooper@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Donna",
    "patientLastName": "Hayward",
    "patientBirthYear": 1990,
    "comment": null,
    "concerns_minor": false,
    "cancelledAt": "2018-05-22T05:23Z",
    "interventionDomainId": "2",
    "consultationTypeId": "21",
    "callingInstitutionId": "11"
  },
  {
    "id": "16",
    "calledAt": "2018-04-21T18:42Z",
    "officerName": "Commissaire Moulin",
    "officerPhone": "0637383930",
    "officerEmail": "dcooper@filae.tld",
    "officerLambdaUser": false,
    "patientFirstName": "Donna",
    "patientLastName": "Hayward",
    "patientBirthYear": 1990,
    "comment": null,
    "concerns_minor": false,
    "assigned_at": "2018-04-22T04:23Z",
    "validated_at": "2018-04-22T05:23Z",
    "interventionDomainId": "2",
    "consultationTypeId": "21",
    "callingInstitutionId": "11"
  },
  {
    "id": "17",
    "calledAt": "2018-04-21T17:42Z",
    "officerName": "Mme Duchmol",
    "officerPhone": "0102030405",
    "officerEmail": "duchmol@domain.tld",
    "officerLambdaUser": true,
    "patientFirstName": null,
    "patientLastName": null,
    "patientBirthYear": null,
    "concerns_minor": false,
    "comment": "Primum non nocere",
    "assigned_at": "2018-04-22T03:23Z",
    "validated_at": "2018-04-22T04:23Z",
    "interventionDomainId": "2"
  },
  {
    "id": "18",
    "calledAt": "2018-05-21T22:42Z",
    "officerName": "Mickeal Kael",
    "officerPhone": "0605040302",
    "officerEmail": "mickeal@groland.net",
    "officerLambdaUser": true,
    "patientFirstName": null,
    "patientLastName": null,
    "patientBirthYear": null,
    "comment": "Primum non nocere",
    "concerns_minor": false,
    "assigned_at": "2018-04-22T03:23Z",
    "interventionDomainId": "2",
    "assigneeId": "2"
  }
];
