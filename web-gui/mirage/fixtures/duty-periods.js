/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    "id": "1105211",
    "startsAt": "2018-05-21T06:00Z",
    "endsAt": "2018-05-21T10:00Z",
    "consultationTypeId": "11",
    "assigneeIds": ["2"]
  },
  {
    "id": "1105212",
    "startsAt": "2018-05-21T11:00Z",
    "endsAt": "2018-05-21T15:00Z",
    "consultationTypeId": "11",
    "assigneeIds": []
  }
];
