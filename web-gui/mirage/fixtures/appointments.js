/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    "id": "1105211",
    "startsAt": "2018-05-21T06:00Z",
    "duration": "PT30M",
    "officerName": "Commissaire Swan Laurence",
    "officerPhone": "0607080900",
    "officerEmail": "slaurence@filae.tld",
    "officialReport": "11223344",
    "patientFirstName": "Cindy",
    "patientLastName": "Rella",
    "patientBirthdate": "1950-02-15",
    "patientGender": "woman",
    "patientPhone": "0706050403",
    "circumstances": "Patiente forcée à s'habiller de haillons et à accomplir toutes les tâches ménagères.",
    "comment": null,
    "consultationTypeId": "11",
    "consultationReasonId": "111",
    "callingInstitutionId": "11"
  },
  {
    "id": "1105212",
    "startsAt": "2018-05-21T06:30Z",
    "duration": "PT1H",
    "officerName": "Commissaire Jules Maigret",
    "officerPhone": "0617181910",
    "officerEmail": "jmaigret@filae.tld",
    "officialReport": "223344",
    "patientFirstName": "Alex",
    "patientLastName": "Belluci",
    "patientBirthdate": "1964-09-30",
    "patientGender": "woman",
    "patientPhone": "0716151413",
    "circumstances": null,
    "comment": null,
    "consultationTypeId": "11",
    "consultationReasonId": "112",
    "callingInstitutionId": "11"
  },
  {
    "id": "1205211",
    "startsAt": "2018-05-21T07:30Z",
    "duration": "PT1H30M",
    "officerName": "Commissaire Jules Maigret",
    "officerPhone": "0617181910",
    "officerEmail": "jmaigret@filae.tld",
    "officialReport": "223344",
    "patientFirstName": "Alex",
    "patientLastName": "Belluci",
    "patientBirthdate": "1964-09-30",
    "patientGender": "woman",
    "patientPhone": "0716151413",
    "circumstances": null,
    "comment": null,
    "consultationTypeId": "12",
    "consultationSubtypeId": "122",
    "callingInstitutionId": "11"
  }
];
