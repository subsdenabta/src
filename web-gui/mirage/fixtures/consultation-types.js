/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    id: "11",
    title: 'Médicale',
    interventionDomainId: "1",
    consultationReasonIds: ["111", "112"]
  },
  {
    id: "12",
    title: 'Psychiatrique',
    interventionDomainId: "1",
    consultationSubtypeIds: ["121", "122"]
  },
  {
    id: "21",
    title: 'Garde à vue',
    interventionDomainId: "2"
  },
  {
    id: "22",
    title: 'Levée de corps',
    interventionDomainId: "2"
  }
];
