/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
export default [
  {
    "id": "11111",
    "weekDay": 1,
    "startsAt": "T08:00",
    "endsAt": "T12:00",
    "scheduleId": "11",
    "consultationTypeId": "11"
  },
  {
    "id": "11112",
    "weekDay": 1,
    "startsAt": "T13:00",
    "endsAt": "T17:00",
    "scheduleId": "11",
    "consultationTypeId": "11"
  },
  {
    "id": "11113",
    "weekDay": 2,
    "startsAt": "T08:00",
    "endsAt": "T12:00",
    "scheduleId": "11",
    "consultationTypeId": "11"
  },
  {
    "id": "11121",
    "weekDay": 3,
    "startsAt": "T08:00",
    "endsAt": "T12:00",
    "scheduleId": "11",
    "consultationTypeId": "12"
  }
];
