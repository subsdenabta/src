/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Factory } from 'ember-cli-mirage';

import faker from 'faker';

export default Factory.extend({
  address() { return `${faker.address.streetName()} ${faker.address.zipCode()} ${faker.address.city()}`},
  chaperonName() { return `${faker.name.lastName()} ${faker.name.firstName()}`},
  chaperonQuality() { return faker.random.arrayElement(["Mère","Père","Oncle","Tante","Voisin(e)"]);},
  phoneNumber() { return faker.phone.phoneNumber() },
  placed() { return faker.random.boolean() },
  actorName() { return `${faker.name.lastName()} ${faker.name.firstName()}`},
  obsNurse() { return faker.lorem.paragraph() },
  obsPsychologist() { return faker.lorem.paragraph() },
  obsGeneral() { return faker.lorem.paragraph() },
  wristXRay() { return faker.random.boolean() },
  teethXRay() { return faker.random.boolean() },
  clavicleXRay() { return faker.random.boolean() },
  stdTest() { return faker.random.boolean() },
  preTherapyTest() { return faker.random.boolean() },
  cgaTest() { return faker.random.boolean() },
  dnaTest() { return faker.random.boolean() },
  chemicalSubmission() { return faker.random.boolean() },
  tripleTherapy() { return faker.random.boolean() },
  dressing() { return faker.random.boolean() },
  psychologicalImpactRequested() { return faker.random.boolean() },
  medicalObs() { return faker.lorem.paragraph() },
  caseHistory() { return faker.lorem.paragraph() },
  background() { return faker.lorem.paragraph() },
  treatment() { return faker.lorem.paragraph() },
  allergy() { return faker.lorem.paragraph() },
  clinicalExamination() { return faker.lorem.paragraph() },
  complementaryInfo() { return faker.lorem.paragraph() },
});
