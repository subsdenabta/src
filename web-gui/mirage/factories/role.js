/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Factory, trait } from 'ember-cli-mirage';

export default Factory.extend({
  id(i) {
    return `role${i}`
  },
  medicalConfidentiality: false,

  administrator: trait({
    id: 'administrator',
    medicalConfidentiality: true
  }),

  association: trait({
    id: 'association',
  }),

  nurse: trait({
    id: 'nurse',
    medicalConfidentiality: true
  }),

  physician: trait({
    id: 'physician',
    medicalConfidentiality: true
  }),

  psychiatrist: trait({
    id: 'psychiatrist',
    medicalConfidentiality: true
  }),

  psychiatrist_expert: trait({
    id: 'psychiatrist_expert',
    medicalConfidentiality: true
  }),

  psychologist: trait({
    id: 'psychologist',
    medicalConfidentiality: true
  }),

  psychologist_expert: trait({
    id: 'psychologist_expert',
    medicalConfidentiality: true
  }),

  secretary: trait({
    id: 'secretary',
    medicalConfidentiality: true
  }),

  switchboard_operator: trait({
    id: 'switchboard_operator',
  })
});
