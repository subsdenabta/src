/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Factory, association } from 'ember-cli-mirage';
import faker from 'faker';
import moment from 'moment';

export default Factory.extend({
  questionnaire() { return {
    "pages": [
      {
        "name": 42,
        "elements": [
          {
            "name": "circumstances-ref",
            "title": "Circonstances",
            "description": "Ce qu'il s'est passé",
            "type": "comment"
          }
        ]
      }
    ]
  } },
  data() { return {
    "circumstances-ref": "Sortie de discothèque"
  } },
  validatedAt() { return moment.utc(faker.date.past()).format('YYYY-MM-DDTHH:mm[Z]') },
  deliveredAt() { return moment.utc(faker.date.past()).format('YYYY-MM-DDTHH:mm[Z]') },
  checksum() { return faker.random.uuid() },

  flowingConsultation: association(),
  validatedBy: association(),

  // see https://github.com/salsify/ember-cli-pact/issues/37
  afterCreate(certificate, server) {
    server.create('attached-file', 1, { id: '3', attachable: certificate })
  }
});
