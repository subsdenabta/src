/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Response from 'ember-cli-mirage/response';
import moment from 'moment';
import faker from 'faker';
import ENV from '../config/environment';

export default function() {

  /*
    Config (with defaults).
    Note: these only affect routes defined *after* them!
  */

  faker.locale = "fr"

  this.urlPrefix = ENV.APP.API_URL;    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  // Overriding default content-type https://github.com/samselikoff/ember-cli-mirage/pull/1334
  // this.responseContentType = 'application/vnd.api+json';
  // this.headers = {
  //   "Content-Type": "application/vnd.api+json"
  // }


  this.post('/oauth/token', (schema, request) => {
    var params = request.requestBody.split('&');

    if (params.includes('username=administrator') && params.includes('password=administrator')) {
      return new Response(200, {'Content-Type': 'application/json; charset=utf-8'}, {
        access_token: 'my_secret_token',
        token_type: 'bearer',
        expires_in: 7200
      });
    }
    else {
      return new Response(401, {'Content-Type': 'application/json'}, {
        error: 'invalid_grant',
        error_description: 'a long text of what happened'
      });
    }
  });

  /*
    http://www.ember-cli-mirage.com/docs/v0.3.x/shorthands/
  */

  this.resource('roles', { only: ['index', 'show'] });
  this.resource('certificates', { only: ['show', 'update', 'delete'] })
  this.get('certificates/:id/preview')
  this.patch('certificates/:id/send_by_mail')
  this.resource('certificate-layouts', { path: '/settings', only: ['show', 'update'] });

  this.get('/patients', (schema, request) => {
    let queryParams = request.queryParams

    if (queryParams['page[number]'] == undefined  || queryParams['page[size]'] == undefined) {
      return new Response(400, {'Content-Type': 'application/vnd.api+json'}, {
        data: [],
        errors: [{
          status: 400
        }]
      });
    }
    else {
      return schema.patients.all()
    }
  });
  this.resource('medical_files', { only: ['show', 'update'] });
  this.resource('master_documents', { only: ['show', 'delete'] });

  this.resource('users');
  this.get('/users/me', ({ users }) => {
    return users.find(1);
  });
  this.post('password_requests');

  this.resource('intervention_domains');
  this.resource('consultation_types');
  this.resource('consultation_subtypes');
  this.resource('consultation_reasons');
  this.resource('schedules');
  this.resource('time_slots');

  this.resource('calling_areas');
  this.resource('calling_institutions');
  this.resource('flowing_consultations');

  this.resource('duty_periods');
  this.get('duty_periods', ({dutyPeriods}, request) => {
    let consultationTypeId = request.queryParams['filter[consultation_type_id]'];
    let scheduledOn = moment(request.queryParams['filter[scheduled_on]'])

    return dutyPeriods.where(function (dutyPeriod) {
      return dutyPeriod.consultationTypeId === consultationTypeId && scheduledOn.isSame(dutyPeriod.startsAt, 'day')
    })
  });
  this.resource('appointments');
  this.get('appointments', ({appointments}, request) => {
    let consultationTypeId = request.queryParams['filter[consultation_type_id]'];
    let scheduledOn = moment(request.queryParams['filter[scheduled_on]'])

    return appointments.where(function (appointment) {
      return appointment.consultationTypeId === consultationTypeId && scheduledOn.isSame(appointment.startsAt, 'day')
    })
  });
}
