/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { JSONAPISerializer } from 'ember-cli-mirage';
import { PactEnabled } from 'ember-cli-pact/mock-provider/mirage';
import { underscore } from '@ember/string';
import { pluralize } from 'ember-inflector';

export default PactEnabled(JSONAPISerializer).extend({
  // serializeIds: 'always',
  alwaysIncludeLinkageData: true,

  typeKeyForModel(model) {
    return underscore(pluralize(model.modelName));
  },

  keyForAttribute(attr) {
    return underscore(attr);
  },

  keyForRelationship(key /*, relationship, method*/) {
    return underscore(key);
  }

});
