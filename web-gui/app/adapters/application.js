/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import JSONAPIAdapter from '@ember-data/adapter/json-api';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import AdapterArrayBufferMixin from 'ember-cli-file-saver/mixins/adapter-arraybuffer-mixin';
import ENV from '../config/environment';
import { underscore } from '@ember/string';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default JSONAPIAdapter.extend(DataAdapterMixin, AdapterArrayBufferMixin, {
  host: ENV.APP.API_URL,

  authHeader: computed('session.data.authenticated.access_token', function () {
    let { access_token } = this.get('session.data.authenticated');
    let header = {}

    if (isPresent(access_token)) {
      header['Authorization'] = `Bearer ${access_token}`;
    }

    return header
  }),

  headers: computed('authHeader', function() {
    let headers = {};
    if (this.session.isAuthenticated) {
      // OAuth 2
      headers = this.authHeader;
    }

    return headers;
  }),

  pathForType(type) {
    return underscore(this._super(type));
  }
});
