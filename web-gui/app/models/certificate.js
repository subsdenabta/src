/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { reads, filterBy, notEmpty } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed, or } from 'ember-awesome-macros';
import { memberAction } from 'ember-api-actions';

export default Model.extend({
  intl: service(),

  questionnaire: attr('json'),
  answers: attr('json'),

  deliveredAt: attr('datehourminute'),
  delivered: notEmpty('deliveredAt'),
  deliveredLabel: computed('delivered', function(delivered) {
    return this.intl.t('certificate.delivered.' + delivered);
  }),

  validatedAt: attr('datehourminute'),
  validated: notEmpty('validatedAt'),
  validatedBy: belongsTo('user'),

  checksum: attr('string'),

  appointment: belongsTo(),
  flowingConsultation: belongsTo(),

  attachedFiles: hasMany('attached-file', { inverse: 'attachable' }),

  signedCertificates: filterBy('attachedFiles', 'type', 'signed-certificate'),
  signedCertificate: reads('signedCertificates.lastObject'),

  validatedCertificates: filterBy('attachedFiles', 'type', 'validated-certificate'),
  validatedCertificate: reads('validatedCertificates.lastObject'),

  filename: or('{validatedCertificate,signedCertificate}.filename'),

  preview: memberAction({ path: 'preview', type: 'GET', ajaxOptions: { blob: true } }),

  sendByMail: memberAction({
    path: 'send_by_mail',
    type: 'PATCH',
    before(attributes) {
      let payload = this.serialize()
      payload.data.attributes = attributes
      delete payload.data.relationships
      return payload
    }})
});
