/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/string';

export default Model.extend({
  title: attr('string'),
  colour: attr('string'),
  callingInstitutions: hasMany('calling-institution'),

  cssBorderLeftColor: computed('colour', function () {
    let colour = this.colour;

    return colour ? htmlSafe('border-left-color: ' + colour) : null;
  }),

  cssColor: computed('colour', function () {
    let colour = this.colour;

    return colour ? htmlSafe('color: ' + colour) : null;
  })

});
