/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';
import { empty } from '@ember/object/computed';

export default Model.extend({
  title: attr('string'),
  disabledAt: attr('datehourminute'),
  questionnaire: attr('json'),
  masterDocument: belongsTo('master-document', {inverse: 'attachable'}),
  roles: hasMany('role'),

  isEnabled: empty('disabledAt'),
  status: computed('isEnabled', function() {
    if (this.isEnabled) { return 'active' }
    else { return 'inactive' }
  }),

  hasRole: computed('roles.@each.id', function () {
    let roleIds = this.roles.mapBy('id');

    return (role) => {
      return roleIds.includes(role.get('id'));
    }
  })
});
