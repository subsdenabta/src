/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';
import { equal, or, reads, filterBy, sort } from '@ember/object/computed';
import { isPresent } from '@ember/utils';
import moment from 'moment';

export default Model.extend({
  identifier: attr('string'),
  email: attr('string'),
  firstName: attr('string'),
  lastName: attr('string'),
  role: belongsTo('role'),
  password: attr('password'),
  disabledAt: attr('datehourminute'),

  isAdministrator: equal('role.id', 'administrator'),
  isAdmStaff: or('isAdministrator', 'isNurse' ,'isSecretary'),
  isAssociation: equal('role.id', 'association'),
  isPhysician: equal('role.id', 'physician'),
  isPsychiatrist: equal('role.id', 'psychiatrist'),
  isPsychiatristExpert: equal('role.id', 'psychiatrist_expert'),
  isPsychologist: equal('role.id', 'psychologist'),
  isPsychologistExpert: equal('role.id', 'psychologist_expert'),
  isNurse: equal('role.id', 'nurse'),
  isSecretary: equal('role.id', 'secretary'),
  isSwitchboardOperator: equal('role.id', 'switchboard_operator'),
  isDisabled: computed('activeStatus', function() {
    let activeStatus = this.activeStatus;

    if (activeStatus == 'inactive') { return true }
    else { return false }
  }),

  fullName: computed('firstName', 'lastName', function () {
    return `${this.firstName} ${this.lastName}`;
  }),

  abridgedName: computed('firstName', 'lastName', function () {
    let initial = "";
    if (this.firstName) {
      initial = this.firstName.slice(0, 1) + ".";
    }
    return `${initial} ${this.lastName}`;
  }),

  activeStatus: computed('disabledAt', function() {
    let deactivationTime = this.disabledAt;
    let now = moment();

    if ( !isPresent(deactivationTime )) { return 'active' }
    else if (deactivationTime <= now) { return 'inactive' }
    else { return 'deactivation-scheduled' }
  }),

  attachedFiles: hasMany('attached-file', { inverse: 'attachable' }),
  printedSignatures: filterBy('attachedFiles', 'type', 'printed-signature'),
  printedSignatureSorting: Object.freeze(['createdAt']),
  sortedPrintedSignatures: sort('printedSignatures', 'printedSignatureSorting'),
  printedSignature: reads('sortedPrintedSignatures.lastObject'),
});
