/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import EmberObject, { computed } from '@ember/object';
import { sort } from '@ember/object/computed';

const dutyPeriodsSorting = ['startsAt', 'endsAt:desc']
const appointmentsSorting = ['startsAt', 'endsAt:desc']

export default EmberObject.extend({

  assignmentsEvents: computed('dutyPeriods.@each.{startsAt,endsAt,assigneesNames}', function () {
    return this.dutyPeriods.map((dutyPeriod) => {
      return {
        dutyPeriodId: dutyPeriod.get('id'),
        title: dutyPeriod.get('assigneesNames'),
        start: dutyPeriod.get('startsAt'),
        end: dutyPeriod.get('endsAt'),
        color: dutyPeriod.get('color')
      }
    });
  }),

  dutyPeriodsSorting,
  sortedDutyPeriods: sort('dutyPeriods', 'dutyPeriodsSorting'),

  lanes: computed('dutyPeriods.@each.assigneesNames', 'sortedDutyPeriods', function () {
    let lanesResult = [];

    this.sortedDutyPeriods.forEach(function (dutyPeriod) {
      let dutyPeriodsInstances = dutyPeriod.get('assignees.length') || 1

      for (var i = 0; i < dutyPeriodsInstances; i++) {
        let availableLane = null;

        for (const lane of lanesResult) {
          let lastDutyPeriod = lane.get('dutyPeriods.lastObject');
          if (!lastDutyPeriod || lastDutyPeriod.get('endsAt') <= dutyPeriod.get('startsAt')) {
            availableLane = lane;
            break;
          }
        }

        if (!availableLane) {
          availableLane = EmberObject.create({dutyPeriods: []});
          lanesResult.pushObject(availableLane);
        }
        availableLane.get('dutyPeriods').pushObject(dutyPeriod);
      }
    });

    if (!lanesResult.get('length')) {
      lanesResult.pushObject(EmberObject.create({title: 'Ø', dutyPeriods: []}))
    }

    return lanesResult;
  }),

  lanesResources: computed('lanes', function () {
    return this.lanes.map(function (lane, index) {
      return {
        "id": `${index}`,
        "title": lane.get('title') || `${index + 1}`
      };
    });
  }),

  dutyPeriodsEvents: computed('lanes', function () {
    let eventsForLanes = this.lanes.map(function (lane, index) {
      return lane.get('dutyPeriods').map(function (dutyPeriod) {
        return {
          dutyPeriodId: dutyPeriod.get('id'),
          start: dutyPeriod.get('startsAt'),
          end: dutyPeriod.get('endsAt'),
          rendering: 'background',
          color: dutyPeriod.get('color'),
          resourceId: `${index}`
        }
      })
    })

    return eventsForLanes.reduce(function (result, eventsForLane) {
      return result.pushObjects(eventsForLane);
    }, []);
  }),

  appointmentsSorting,
  displayedAppointments: computed('appointments.@each.isCancelled', 'displayHidden', function () {
    let displayHidden = this.displayHidden;
    return this.appointments.filter(appointment => {
      return ( displayHidden || !appointment.get('isCancelled') );
    });
  }),
  sortedAppointments: sort('displayedAppointments', 'appointmentsSorting'),

  appointmentsEvents: computed('sortedAppointments.@each.{startsAt,endsAt,summary}', 'lanesResources', function () {
    let lastAppointmentByResource = this.lanesResources.reduce(function (result, laneResource) {
      result[laneResource.id] = null
      return result;
    }, {});

    let defaultResource = this.get('lanesResources.firstObject.id')
    return this.sortedAppointments.map((appointment) => {
      let availableResource = defaultResource;

      for (const resourceId in lastAppointmentByResource) {
        let lastAppointment = lastAppointmentByResource[resourceId];
        if (!lastAppointment || lastAppointment.get('endsAt') <= appointment.get('startsAt')) {
          availableResource = resourceId;
          break;
        }
      }

      let lastAppointment = lastAppointmentByResource[availableResource];
      if (!appointment.get('isCancelled') && (!lastAppointment || lastAppointment.get('endsAt') < appointment.get('endsAt'))) {
        lastAppointmentByResource[availableResource] = appointment;
      }

      return {
        appointmentId: appointment.get('id'),
        start: appointment.get('startsAt'),
        end: appointment.get('endsAt'),
        title: appointment.get('summary'),
        resourceId: availableResource,
        backgroundColor: appointment.get('backgroundColor'),
        borderColor: appointment.get('borderColor'),
        className: appointment.get('classNames')
      }
    });
  }),

  agendaEvents: computed('dutyPeriodsEvents.[]', 'appointmentsEvents.[]', function () {
    return [].pushObjects(this.dutyPeriodsEvents).pushObjects(this.appointmentsEvents);
  }),

  eventRender: function (event, element) {
    element.find('.fc-title').html(event.title);
  }
});
