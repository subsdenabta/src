/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';

export default Model.extend({
  title: attr('string'),
  interventionDomain: belongsTo('intervention-domain'),
  consultationSubtypes: hasMany('consultation-subtype'),
  consultationReasons: hasMany('consultation-reason'),
  complementaryConsultationTypes: hasMany('consultation-type', { inverse: null }),
  dutyPeriods: hasMany('duty-period'),
  roles: hasMany('role'),
  assignedRoles: hasMany('role'),
  iconName: attr('string'),

  rolesTitles: computed('roles.@each.title', function () {
    return this.roles.mapBy('title').join(', ');
  }),

  hasRole: computed('roles.@each.id', function () {
    let roleIds = this.roles.mapBy('id');

    return (role) => {
      return roleIds.includes(role.get('id'));
    }
  }),

  hasAssignedRole: computed('assignedRoles.@each.id', function () {
    let roleIds = this.assignedRoles.mapBy('id');

    return (role) => {
      return roleIds.includes(role.get('id'));
    }
  })
});
