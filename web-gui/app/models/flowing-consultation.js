/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { belongsTo, attr, hasMany } from '@ember-data/model';
import { computed } from '@ember/object';
import { alias, reads, filterBy, filter } from '@ember/object/computed';
import { isPresent } from '@ember/utils';
import moment from 'moment';
import { w } from '@ember/string';
import { inject as service } from '@ember/service';

let cancellationReasons = w('custody-ended sent-to-hospital brought-before-the-courts unconcerned sent-to-morgue moved-to-appointments other')

export default Model.extend({
  intl: service(),

  interventionDomain: belongsTo('intervention-domain'),
  consultationType: belongsTo('consultation-type'),

  calledAt: attr('datehourminute'),
  onmlId: attr('string'),
  callingInstitution: belongsTo('calling-institution'),
  callingInstitutionTitleWithArea: reads('callingInstitution.titleWithArea'),
  officerName: attr('string'),
  officerPhone: attr('string'),
  officerEmail: attr('string'),
  officerLambdaUser: attr('boolean'),

  patient: belongsTo('patient'),
  patientFirstName: attr('string'),
  patientLastName: attr('string'),
  patientBirthYear: attr('number'),
  patientGender: attr('string'),

  updatedAt: attr('date'),

  patientAge: computed('calledAt','patient.{birthYear,birthdate}', function () {
    let calledAt = this.calledAt;

    if (isPresent(calledAt)) {
      return this.get('patient.content').ageAtMoment(moment(calledAt));
    }
  }),

  comment: attr('string'),

  assignedAt: attr('datehourminute'),
  assignee: belongsTo('user'),
  userInCharge: alias('assignee'),
  assigneeName: reads('assignee.abridgedName'),
  assigneeEmail: reads('assignee.email'),

  validatedAt: attr('datehourminute'),
  cancelledAt: attr('datehourminute'),
  cancelledBy: attr('string'),
  cancellationReasons,

  concernsMinor: attr('boolean', { defaultValue: false }),

  state: computed('assignedAt', 'validatedAt', 'cancelledAt', function () {
    if (this.cancelledAt) {
      let label = 'Annulée. '
      label += this.intl.t('flowing-consultation.cancelledBy.' + this.cancelledBy);
      label += '.'

      return { label: label, icon: 'ban' }
    }
    if(this.validatedAt) { return { label: 'Validée', icon: 'check' } }
    if(this.assignedAt) { return { label: 'En cours', icon: 'user-doctor' } }
    return { label: 'À transmettre', icon: 'hourglass-empty' }
  }),

  isMessage: computed('officerLambdaUser', function () {
    return this.officerLambdaUser;
  }),

  consultationTypeStyleAttribute: reads('callingInstitution.callingArea.cssBorderLeftColor'),

  callingInstitutionStyleAttribute: reads('callingInstitution.callingArea.cssColor'),

  supportsMultipleCertificates: true,
  certificates: hasMany(),
  unvalidatedCertificates: filter('certificates.@each.validatedAt', function(certificate) {
    return certificate.get('validatedAt') == null
  }),
  validatedCertificates: filter('certificates.@each.validatedAt', function(certificate) {
    return certificate.get('validatedAt') != null
  }),

  attachedFiles: hasMany('attached-file', { inverse: 'attachable' }),
  standardAttachedFiles: filterBy('attachedFiles', 'type', 'standard'),
  medicalContentFiles: filterBy('attachedFiles', 'type', 'medical-content'),
  nbAttachedFiles: reads('attachedFiles.length')
});
