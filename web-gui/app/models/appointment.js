/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, belongsTo, hasMany } from '@ember-data/model';
import Copyable from 'ember-data-copyable';
import { computed } from '@ember/object';
import {
  reads,
  notEmpty,
  filter,
  filterBy,
  sort
} from '@ember/object/computed';
import {
  computed as awesomeComputed,
  raw,
  not,
  and,
  or,
  array
} from 'ember-awesome-macros';
import { isPresent } from '@ember/utils';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { w } from '@ember/string';
import { memberAction } from 'ember-api-actions';
import Workflow from './workflow';

export default Model.extend(Copyable, {
  intl: service(),
  workflow: awesomeComputed(function () {
    return Workflow.create({appointment: this});
  }),

  emergency: attr("boolean", { defaultValue: false }),
  isEmergency: reads('emergency'),

  consultationType: belongsTo('consultation-type'),
  consultationSubtype: belongsTo('consultation-subtype'),
  consultationReason: belongsTo('consultation-reason'),

  userInCharge: belongsTo('user'),
  userInChargeAbridgedName: reads('userInCharge.abridgedName'),
  userInChargeEmail: reads('userInCharge.email'),
  takenInCharge: computed('userInCharge.id', function() {
    return this.get('userInCharge.id') !== undefined
  }),
  takenInChargeLabel: computed('takenInCharge', function() {
    let status = this.takenInCharge;

    return this.intl.t('appointment.takenInCharge.' + status);
  }),

  startsAt: attr('datehourminute'),
  onmlId: attr('string'),
  duration: attr('duration'),
  durationAsMinutes: computed('duration', {
    get(/* key */) {
      return this.duration && this.duration.asMinutes();
    },
    set(key, value) {
      this.set('duration',  moment.duration(parseInt(value), 'minutes'));
      return value;
    }
  }),

  endsAt: computed('startsAt', 'duration', function () {
    return moment(this.startsAt).add(this.duration);
  }),

  cancelledAt: attr('datehourminute'),
  isCancelled: notEmpty('cancelledAt'),
  cancelledBy: attr('string'),
  cancelledByLabel: awesomeComputed('isCancelled', 'isPostponed', 'cancelledBy', 'nextAppointment.startsAt', function(isCancelled, isPostponed, cancelledBy, nextAppointmentStartsAt) {
    let result = undefined;

    if (isCancelled) {
      result = this.intl.t('appointment.isCancelled');

      if (isPostponed) {
        result = this.intl.t('appointment.isPostponedAt', { startsAt: nextAppointmentStartsAt });
      }

      result += ' '
      result += this.intl.t('appointment.cancelledBy.' + cancelledBy);
    }

    return result;
  }),

  updatedAt: attr('date'),
  createdAt: attr('date'),

  callingInstitution: belongsTo('calling-institution'),
  officerName: attr('string'),
  officerPhone: attr('string'),
  officerEmail: attr('string'),
  officialReport: attr('string'),
  officerContactInfo: computed('officerName', 'officerEmail', 'officerPhone', function() {
    let result = '';

    if (this.officerName) {
      result += this.officerName
    }

    let contactInfo = ['officerPhone', 'officerEmail'].map((attribute) => {
      return this.get(attribute)
    }).compact().join(', ')

    if (contactInfo) {
      result += ` (${contactInfo})`
    }

    return result;
  }),

  patient: belongsTo('patient'),
  patientFirstName: attr('string'),
  patientLastName: attr('string'),
  patientBirthdate: attr('dateOnly'),
  patientGender: attr('string'),
  patientHumanGender: awesomeComputed('patientGender', function(gender) {
    return this.intl.t('patient.gender.' + gender);
  }),
  patientPhone: attr('string'),
  patientArrivedAt: attr('datehourminute'),
  patientArrived: notEmpty('patientArrivedAt'),
  patientReleasedAt: attr('datehourminute'),
  patientReleased: notEmpty('patientReleasedAt'),
  patientMissing: attr('boolean', { default: false }),
  patientStatus: or('patientArrived', 'patientMissing'),
  patientStatusLabel: computed('patientArrived', 'patientMissing', function() {

    let status = w('patientArrived patientMissing').find((status) => {
      return this.get(status);
    })

    return this.intl.t('appointment.patientStatus.' + status);
  }),

  previousAppointment: belongsTo('appointment', {inverse: 'nextAppointment'}),
  previousAppointmentPostponedBy: attr('string'),
  nextAppointment: belongsTo('appointment', {inverse: 'previousAppointment'}),
  isPostponed: and('isCancelled', 'nextAppointment.id'),

  originatingAppointment: belongsTo('appointment', { inverse: 'complementaryAppointments' }),
  isRootAppointment: not('originatingAppointment.id'),
  complementaryAppointments: hasMany('appointment', { inverse: 'originatingAppointment' }),
  activeComplementaryAppointments: filter('complementaryAppointments.@each.isCancelled', function (appointment) {
    return !appointment.get('isCancelled');
  }),

  patientFullJourneyAppointments: computed('originatingAppointment.patientFullJourneyAppointments.[]', 'patientDownstreamJourneyAppointments.[]', function() {
    return this.get('originatingAppointment.patientFullJourneyAppointments') || [this].pushObjects(this.patientDownstreamJourneyAppointments);
  }),
  patientDownstreamJourneyAppointments: awesomeComputed('activeComplementaryAppointments.@each.patientDownstreamJourneyAppointments', function(activeComplementaryAppointments) {
    let result = activeComplementaryAppointments;

    result.forEach((appointment) => {
      result.pushObjects(appointment.get('patientDownstreamJourneyAppointments'))
    })

    return result;
  }),

  patientFullJourneySummary: computed('originatingAppointment.patientFullJourneySummary', 'patientDownstreamJourneySummary', function() {
    return this.get('originatingAppointment.patientFullJourneySummary') || this.patientDownstreamJourneySummary;
  }),
  patientDownstreamJourneySummary: computed('consultationType.title', 'patientClass', 'activeComplementaryAppointments.@each.patientDownstreamJourneySummary', function() {
    let result = ''

    let title = this.get('consultationType.title');
    let iconName = this.get('consultationType.iconName');
    if (title) {
      if (iconName) {
        result = `<span title="${title}" class="${this.patientClass} fas fas-narrow fa-${iconName}"></span>`;
      } else {
        result = `<span title="${title}" class="${this.patientClass}">${title[0]}</span>`;
      }
    }

    let downstreamJourneys = this.activeComplementaryAppointments.map((appointment) => {
      return appointment.get('patientDownstreamJourneySummary')
    }).join(' &amp; ')

    if (downstreamJourneys) {
      result = `(${result} => ${downstreamJourneys})`
    }

    return result;
  }),
  patientClass: or('patientIsOutClass', 'patientIsTakenInChargeClass', 'patientMayBeWaitingClass', raw('Patient--unavailable')),
  patientMayBeWaitingClass: and('patientArrived', raw('Patient--waiting')),
  patientIsTakenInChargeClass: and('takenInCharge', raw('Patient--takenInCharge')),
  patientIsOut: or('patientReleased', 'certificateValidated', 'certificateDelivered'),
  patientIsOutClass: and('patientIsOut', raw('Patient--out')),


  certificate: belongsTo(),
  certificateQuestionnaire: reads('certificate.questionnaire'),
  certificateData: reads('certificate.answers'),
  certificateDeliveredAt: reads('certificate.deliveredAt'),
  certificateDelivered: reads('certificate.delivered'),
  certificateDeliveredLabel: reads('certificate.deliveredLabel'),
  certificateValidatedAt: reads('certificate.validatedAt'),
  certificateValidated: reads('certificate.validated'),
  certificateCompleted: or(array.first('signedCertificates'), and('certificateQuestionnaire', 'certificateData')),
  certificateStatus: computed('certificateDelivered', 'certificateCompleted', 'certificateValidated', function() {
    if (this.certificateDelivered) { return 'delivered' }
    if (this.certificateValidated) { return 'validated' }
    if (this.certificateCompleted) { return 'completed' }
    return null
  }),
  certificateStatusLabel: computed('certificateStatus', function() {
    return this.intl.t('appointment.certificate_status.'+this.certificateStatus);
  }),

  requisitionReceivedAt: attr('datehourminute'),
  requisitionReceived: notEmpty('requisitionReceivedAt'),
  requisitionReceivedLabel: computed('requisitionReceived', function() {
    let status = this.requisitionReceived;

    return this.intl.t('appointment.requisitionReceived.' + status);
  }),

  attachedFiles: hasMany('attached-file', { inverse: 'attachable' }),
  nbAttachedFiles: reads('attachedFiles.length'),
  standardAttachedFiles: filterBy('attachedFiles', 'type', 'standard'),
  medicalContentFiles: filterBy('attachedFiles', 'type', 'medical-content'),
  redactedCertificates: filterBy('attachedFiles', 'type', 'redacted-certificate'),
  signedCertificates: reads('certificate.signedCertificates'),
  validatedCertificates: reads('certificate.validatedCertificates'),
  validatedCertificateSorting: Object.freeze(['createdAt']),
  sortedValidatedCertificates: sort('validatedCertificates', 'validatedCertificateSorting'),
  validatedCertificate: reads('sortedValidatedCertificates.lastObject'),
  certificateFilenamePrefix: computed('onmlId', 'patient.fullName', 'startsAt', function () {
    return [
      this.onmlId,
      this.get('patient.fullName'),
      this.startsAt.toISOString(),
    ].compact().join(' ');
  }),
  certificatePreview: memberAction({ path: 'certificate_preview.pdf', type: 'GET', ajaxOptions: { arraybuffer: true } }),
  get certificatePreviewFilename() {
    return [
      this.certificateFilenamePrefix,
      'preview',
      moment().toISOString()
    ].compact().join(' ') + '.pdf';
  },

  logs: hasMany('log', { inverse: 'loggable' }),

  medicalFile: belongsTo('medical-file'),

  status: computed('isCancelled', 'certificateDelivered', 'certificateCompleted', 'certificateValidated', 'patientArrived', 'takenInCharge', 'isPostponed', 'patientMissing', function() {
    if (this.isPostponed) { return 'postponed' }
    if (this.isCancelled) { return 'cancelled' }
    if (this.certificateDelivered) { return 'certificate_delivered' }
    if (this.certificateValidated) { return 'certificate_validated' }
    if (this.certificateCompleted) { return 'certificate_completed' }
    if (this.takenInCharge) { return 'taken_in_charge' }
    if (this.patientMissing) { return 'missing' }
    if (this.patientArrived) { return 'waiting' }
    return 'not_arrived'
  }),

  statusLabel: computed('status', function() {
    let status = this.status

    switch(status) {
      case 'postponed':
      case 'cancelled':
        return this.cancelledByLabel;
      case 'taken_in_charge':
        return this.takenInChargeLabel;
      case 'certificate_completed':
      case 'certificate_validated':
      case 'certificate_delivered':
        return this.certificateStatusLabel;
      case 'waiting':
      case 'missing':
      case 'not_arrived':
        return this.patientStatusLabel;
    }

    return undefined;
  }),

  statusIcon: computed('status', function() {
    let status = this.status

    switch(status) {
      case 'postponed':
        return 'rotate-right';
      case 'cancelled':
        return 'ban';
      case 'taken_in_charge':
        return 'handshake';
      case 'certificate_completed':
        return 'file-lines';
      case 'certificate_validated':
        return 'square-check';
      case 'certificate_delivered':
        return 'envelope';
      case 'waiting':
        return 'house-user';
      case 'missing':
        return 'user-slash';
      case 'not_arrived':
        return 'user-clock';
    }

    return undefined;
  }),

  state: computed('statusLabel', 'statusIcon', function () {
    return { label: this.statusLabel, icon: this.statusIcon }
  }),


  patientNumericalAge: computed('startsAt', 'patient.birthdate', function () {
    let startTime = this.startsAt;
    let birthdate = this.get('patient.birthdate');

    if (isPresent(startTime) && isPresent(birthdate)) {
      return moment(startTime).diff(birthdate, 'years');
    }

    return undefined;
  }),

  patientAge: computed('patientNumericalAge', function () {
    let age = this.patientNumericalAge;

    if (isPresent(age)) {
      if (age < 1) {
        return "Moins de 1 an."
      }
      else {
        return `${age} ans`;
      }
    }

    return undefined;
  }),

  isMinor: computed('patientNumericalAge', function () {
    let age = this.patientNumericalAge;

    if (isPresent(age)) {
      return age < 18;
    }

    return undefined;
  }),

  circumstances: attr('string'),
  comment: attr('string'),

  typeAndDateTime: computed('consultationType.title', 'startsAt', function() {
    return `${moment(this.startsAt).format("DD/MM/YYYY HH:mm")} (${this.get('consultationType.title')})`;
  }),

  summary: computed('patient.fullName', 'patientArrived', 'patientFullJourneySummary', 'isMinor', 'consultationReason.{code,title}', 'status', 'statusLabel', 'isEmergency', function () {
    let result = "";
    let patientFullName = this.get('patient.fullName');
    let patientArrived = this.patientArrived;
    let patientFullJourney = this.patientFullJourneySummary;
    let isMinor = this.isMinor;
    let consultationReasonTitle = this.get('consultationReason.title');
    let consultationReasonCode = this.get('consultationReason.code');
    let status = this.status;
    let statusLabel = this.statusLabel;
    let isEmergency = this.isEmergency;

    if (patientArrived && patientFullJourney) {
      result += `<span class="PatientJourney">${patientFullJourney}</span>`
    }

    if (isPresent(consultationReasonCode)) {
      result += `<span title="${consultationReasonTitle}">[${consultationReasonCode}]</span>`
    }

    if (isEmergency) {
      result += ' <i class="fas fas-narrow fa-exclamation-circle" title="Urgence"></i>';
    }
    if (isMinor) {
      result += ' <i class="fas fas-narrow fa-child" title="Personne mineure à la date du rendez-vous"></i>';
    }

    result += `<span class="Summary--fullName" data-test-summaryFullName="${patientFullName}">${patientFullName}</span>`;

    switch(status) {
      case 'postponed':
        result += ` <i class="fas fa-redo" title="${statusLabel}"></i>`;
        break;
      case `cancelled`:
        result += ` <i class="fas fa-ban" title="${statusLabel}"></i>`;
        break;
      case `certificate_completed`:
        result += ` <i class="far fa-file" title="${statusLabel}"></i>`;
        break;
      case `certificate_validated`:
        result += ` <i class="fas fa-stamp" title="${statusLabel}"></i>`;
        break;
      case `certificate_delivered`:
        result += ` <i class="far fa-envelope" title="${statusLabel}"></i>`;
        break;
      case `missing`:
        result += ` <i class="far fa-user" title="${statusLabel}"></i>`;
        break;
      case 'not_arrived':
        break;
    }

    return result;
  }),

  borderColor: reads('consultationReason.colour'),

  backgroundColor: computed('isCancelled', 'patientMissing', 'borderColor', function() {
    if (this.isCancelled || this.patientMissing) { return "grey" }
    return this.borderColor;
  }),

  classNames: computed('requisitionReceived', function() {
    let classNames = []

    if (this.requisitionReceived) { classNames.pushObject('requisition-received')}

    return classNames
  }),
});
