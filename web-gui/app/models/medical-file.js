/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr } from '@ember-data/model';

export default Model.extend({
  address: attr('string'),
  chaperonName: attr('string'),
  chaperonQuality: attr('string'),
  phoneNumber: attr('string'),
  placed: attr('boolean'),
  actorName: attr('string'),
  obsGeneral: attr('string'),
  obsNurse: attr('string'),
  obsPsychologist: attr('string'),
  wristXRay: attr('boolean'),
  teethXRay: attr('boolean'),
  clavicleXRay: attr('boolean'),
  stdTest: attr('boolean'),
  preTherapyTest: attr('boolean'),
  cgaTest: attr('boolean'),
  dnaTest: attr('boolean'),
  chemicalSubmission: attr('boolean'),
  tripleTherapy: attr('boolean'),
  dressing: attr('boolean'),
  psychologicalImpactRequested: attr('boolean'),
  medicalObs: attr('string'),
  caseHistory: attr('string'),
  background: attr('string'),
  treatment: attr('string'),
  allergy: attr('string'),
  clinicalExamination: attr('string'),
  complementaryInfo: attr('string'),
});
