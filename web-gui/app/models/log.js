/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Model, { attr, hasMany, belongsTo } from '@ember-data/model';
import { computed } from '@ember/object';
import { mapBy } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { equal, and } from '@ember/object/computed'
import moment from 'moment';

export default Model.extend({
  intl: service(),

  occuredAt: attr('datehourminute'),
  who: attr('string'),
  actions: hasMany('log-action'),
  actionsIntlLabels: mapBy('actions', 'intlLabel'),
  actionsIntlChanges: mapBy('actions', 'intlChanges'),

  loggable: belongsTo('loggable', { inverse: 'logs' }),

  logForAppointment: equal('loggable.content.constructor.modelName', 'appointment'),
  appointment: and('logForAppointment', 'loggable'),

  logForPatient: equal('loggable.content.constructor.modelName', 'patient'),
  patient: and('logForPatient', 'loggable'),

  humanTitle: computed('intl.locale', 'actionsIntlLabels', 'occuredAt', 'who', function() {
    let occuredAt = moment(this.occuredAt).format('DD/MM/YYYY à HH:mm');
    let who = this.who
    let actionsLabels = this.actionsIntlLabels.join(', ')

    return  `${actionsLabels} le ${occuredAt} par ${who}`
  }),

  humanChanges: computed('intl.locale', 'actionsIntlChanges', function() {
    return this.actionsIntlChanges.join(', ')
  })
});
