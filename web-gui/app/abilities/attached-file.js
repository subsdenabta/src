/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Ability } from 'ember-can';
import { inject as service } from '@ember/service';
import { computed, or } from 'ember-awesome-macros';
import { reads } from '@ember/object/computed'

export default Ability.extend({
  currentUser: service(),

  attachedFile: reads('model'),
  attachable: reads('attachedFile.attachable'),

  canDownload: reads('currentUser.medicalConfidentiality'),
  canValidate: or('currentUserIsInCharge', 'currentUser.isAdmStaff'),
  canDestroy: computed('currentUser.{medicalConfidentiality,isAdmStaff,isAdministrator}',
    'attachedFile.type',
    'currentUserIsInCharge',
    function(medicalConfidentiality, isAdmStaff, isAdministrator, type, currentUserIsInCharge) {

    switch (type) {
      case 'validated-certificate':
        return false
      default:
        return isAdministrator || medicalConfidentiality && (isAdmStaff || currentUserIsInCharge)
    }
  }),

  currentUserIsInCharge: computed('attachable.{userInCharge,appointment.userInCharge,flowingConsultation.userInCharge}', function (userInCharge, appointmentUserInCharge, flowingConsultationUserInCharge) {
    let effectiveUser = userInCharge || appointmentUserInCharge || flowingConsultationUserInCharge

    return this.currentUser.is(effectiveUser);
  })
});
