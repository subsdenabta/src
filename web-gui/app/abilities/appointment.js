/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import { Ability } from 'ember-can';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object'
import { reads, and, or } from '@ember/object/computed'
import { ability } from 'ember-can/computed';

export default Ability.extend({
  currentUser: service(),

  appointment: reads('model'),

  consultationTypeAbility: ability('consultation-type', 'appointment.consultationType'),

  canCreate: and('consultationTypeAbility','rolesCanCreate'),
  rolesCanCreate: or('currentUser.{isAdmStaff,isPhysician,isPsychiatrist,isPsychologist,isPsychologistExpert,isAssociation}'),
  canEdit: reads('canCreate'),
  canCancel: reads('canCreate'),
  canPostpone: reads('canCreate'),
  canConfirm: reads('currentUser.isAdmStaff'),
  canFinalize: and('consultationTypeAbility','rolesCanFinalize'),
  rolesCanFinalize: or('currentUser.{isAdmStaff,isPhysician}'),
  canSetRequisitionStatus: reads('currentUser.isAdmStaff'),
  canTakeCharge: reads('consultationTypeAbility.canBeAssigned'),

  canEditArchived: reads('currentUser.isAdmStaff'),

  canConsultCertificate: or('canEditCertificate', 'currentUser.medicalConfidentiality'),
  canEditCertificate: or('currentUser.isAdministrator', 'ownedByMe'),
  ownedByMe: computed('currentUser', 'appointment.userInCharge', function() {
    return this.currentUser.is(this.get('appointment.userInCharge'));
  }),

  canEditMedicalFile: reads('currentUser.medicalConfidentiality')
});
