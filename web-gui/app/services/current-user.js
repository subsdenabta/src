/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
// https://github.com/simplabs/ember-simple-auth/blob/4bf3aa52a3edb9611a46b1dfdabc7d64980f14b0/guides/managing-current-user.md
import Service, { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import { computed } from 'ember-awesome-macros'
import { reads } from '@ember/object/computed'

export default Service.extend({
  session: service(),
  store: service(),
  can: service(),

  load() {
    if (this.get('session.isAuthenticated')) {
      return this.store.queryRecord('user', { me: true }).then((user) => {
        this.set('user', user);
        return user.get('role');
      });
    }
    else {
      return RSVP.resolve();
    }
  },

  is(user) {
    return user && this.user && this.get('user.id') === user.get('id');
  },

  isAuthenticated: computed('user', function() {
    return this.user;
  }),

  firstName: reads('user.firstName'),

  role: reads('user.role'),
  isAdministrator: reads('user.isAdministrator'),
  isAdmStaff: reads('user.isAdmStaff'),
  isPhysician: reads('user.isPhysician'),
  isPsychiatrist: reads('user.isPsychiatrist'),
  isPsychiatristExpert: reads('user.isPsychiatristExpert'),
  isPsychologist: reads('user.isPsychologist'),
  isPsychologistExpert: reads('user.isPsychologistExpert'),
  isNurse: reads('user.isNurse'),
  isSecretary: reads('user.isSecretary'),
  isSwitchboardOperator: reads('user.isSwitchboardOperator'),
  isAssociation: reads('user.isAssociation'),
  medicalConfidentiality: reads('user.role.medicalConfidentiality'),

  allConsultationTypes: computed(function () {
    return this.store.findAll('consultation-type', {reload: true});
  }),

  consultationTypes: computed('role', 'allConsultationTypes.[]', function (role, allConsultationTypes) {
    return allConsultationTypes.filter((consultationType) => {
      return this.can.can('consult appointments of consultation-type', consultationType);
    });
  }),

  allInterventionDomains: computed(function () {
    return this.store.findAll('intervention-domain', {reload: true});
  }),

  interventionDomains: computed('role', 'allInterventionDomains.[]', 'consultationTypes.[]', function (role, allInterventionDomains, consultationTypes) {
    return allInterventionDomains.filter((interventionDomain) => {
      if (interventionDomain.get('isStream')) {
        return this.can.can('consult flowing consultations of intervention-domain', interventionDomain);
      } else {
        let domainConsultationTypeIds = interventionDomain.get('consultationTypes').mapBy('id')
        let consultationTypeIds = consultationTypes.mapBy('id')
        return consultationTypeIds.some((id) => {
          return domainConsultationTypeIds.includes(id)
        })
      }
    });
  })

});
