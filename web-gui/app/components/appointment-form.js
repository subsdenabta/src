/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
import moment from 'moment';
import { task, timeout } from 'ember-concurrency';
import { or } from '@ember/object/computed';

const dateFormat = "DD/MM/YYYY"
const dateTimeFormat = "DD/MM/YYYY HH:mm"
const patientGenders = ['woman', 'man', 'unknown']

export default Component.extend({
  store: service(),

  onSubmitTask: task(function * () {
    this.changeset.set('durationAsMinutes', this.durationAsMinutes);

    if (this.shouldCreateOther) {
      this.set('shouldCreateOther', false);
      yield this.onSubmitAndCreateOther();
    }
    else {
      yield this.onSubmit()
    }
  }).drop(),

  recommendedDuration: computed('changeset.{consultationSubtype,consultationReason}.appointmentDuration', function () {
    return this.get('changeset.consultationSubtype.appointmentDuration') || this.get('changeset.consultationReason.appointmentDuration');
  }),

  recommendedDurationAsMinutes: computed('recommendedDuration', function () {
    return this.recommendedDuration && this.recommendedDuration.asMinutes();
  }),

  durationAsMinutes: or('changeset.durationAsMinutes', 'recommendedDurationAsMinutes'),

  startsAt: computed('changeset.startsAt', {
    get(/* key */) {
      let startTime = this.get('changeset.startsAt')

      if (startTime && isPresent(startTime)) {
        let startMoment =  moment(startTime);
        if (startMoment.isValid()) {
          return startMoment.format(dateTimeFormat);
        }
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('startsAt', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('startsAt', null);
      }
      return value;
    }
  }),

  callingInstitutions: computed(function () {
    return this.store.findAll('calling-institution', {include: 'calling_area'});
  }),

  patientGenders,

  isPatientSet: computed('changeset.patient.id', function() {
    return this.get('changeset.patient.id') != null
  }),

  patientBirthdate: computed('changeset.patient.birthdate,changeset.patientBirthdate', {
    get(/* key */) {
      let startTime = this.get('changeset.patient.birthdate') || this.get('changeset.patientBirthdate')

      if (startTime && isPresent(startTime)) {
        return moment(startTime).format(dateFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('patientBirthdate', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('patientBirthdate', null);
      }
      return value;
    }
  }),

  patientAge: computed('changeset.{startsAt,patient.birthdate,patientBirthdate}', function () {
    let startTime = this.get('changeset.startsAt');
    let birthdate = this.get('changeset.patient.birthdate') || this.get('changeset.patientBirthdate');

    if (isPresent(startTime) && isPresent(birthdate)) {
      return moment(startTime).diff(birthdate, 'years');
    }

    return undefined;
  }),

  ageHint: computed('patientAge', function () {
    let age = this.patientAge;

    if (isPresent(age)) {
      if (age < 1) {
        return "Moins de 1 an."
      }
      else {
        return `${age} ans`;
      }
    }

    return undefined;
  }),

  isMinor: computed('patientAge', function () {
    let age = this.patientAge;

    if (isPresent(age)) {
      return age < 18;
    }

    return undefined;
  }),

  consultationType: computed('changeset.data.consultationType', function () {
    return this.get('changeset.data.consultationType');
  }),

  consultationTypes: computed('consultationType.interventionDomain.sortedConsultationTypes.[]', function () {
    return this.get('consultationType.interventionDomain.sortedConsultationTypes')
  }),

  consultationSubtypes: computed('consultationType.consultationSubtypes.[]', function () {
    return this.get('consultationType.consultationSubtypes');
  }),

  consultationSubtype: computed('changeset.data.consultationSubtype', {
    get(key) {
      return this.get('changeset.data.' + key);
    },
    set(key, value) {
      this.changeset.set(key, value)
      return value;
    }
  }),

  consultationSubtypeAffected: computed('consultationSubtype.id', function () {
    return this.get('consultationSubtype.id');
  }),

  affectedConsultationSubtypes: computed('consultationSubtypeAffected', function () {
    if (this.consultationSubtypeAffected) {
      return [this.consultationSubtype];
    }
    else {
      return [];
    }
  }),

  remainingConsultationSubtypes: computed('consultationSubtypes.[]', 'consultationSubtypeAffected', function () {
    if (this.consultationSubtypeAffected) {
      return [];
    }
    else {
      return this.consultationSubtypes;
    }
  }),

  consultationSubtypePlaceholder: computed('consultationSubtypeAffected', 'consultationSubtypes.@each.title', function () {
    if (this.consultationSubtypeAffected) {
      return '';
    }
    else {
      let subtypes = this.consultationSubtypes.mapBy('title').sortBy('length').join(', ');

      if (isPresent(subtypes)) {
        return `Sous-type, par ex. ${subtypes}.`
      }
      else {
        return 'Aucun sous-type disponible pour ce type de consultation';
      }
    }
  }),

  consultationReasons: computed('consultationType.consultationReasons.[]', function () {
    return this.get('consultationType.consultationReasons');
  }),

  consultationReason: computed('changeset.data.consultationReason', {
    get(key) {
      return this.get('changeset.data.' + key);
    },
    set(key, value) {
      this.changeset.set(key, value)
      return value;
    }
  }),

  consultationReasonAffected: computed('consultationReason.id', function () {
    return this.get('consultationReason.id');
  }),

  affectedConsultationReasons: computed('consultationReasonAffected', function () {
    if (this.consultationReasonAffected) {
      return [this.consultationReason];
    }
    else {
      return [];
    }
  }),

  remainingConsultationReasons: computed('consultationReasons.[]', 'consultationReasonAffected', function () {
    if (this.consultationReasonAffected) {
      return [];
    }
    else {
      return this.consultationReasons;
    }
  }),

  consultationReasonPlaceholder: computed('consultationReasonAffected', 'consultationReasons.@each.code', function () {
    if (this.consultationReasonAffected) {
      return '';
    }
    else {
      let reasons = this.consultationReasons.mapBy('code').join(', ');

      if (isPresent(reasons)) {
        return `Motif, par ex. ${reasons}.`
      }
      else {
        return 'Aucun motif disponible pour ce type de consultation';
      }
    }
  }),

  fetchPatients: task(function * (chars) {
    yield timeout(1000)
    return this.store.query('patient', {
      filter: {
        last_name: chars
      }
    })
  }),

  actions: {
    addCallingInstitution(callingInstitution) {
      this.set('callingInstitution', callingInstitution);
    },

    removeCallingInstitution(callingInstitution) {
      if (this.get('callingInstitution.id') === callingInstitution.get('id')) {
        this.set('callingInstitution', null);
      }
    },

    addConsultationSubtype(consultationSubtype) {
      this.set('consultationSubtype', consultationSubtype);
    },

    removeConsultationSubtype(consultationSubtype) {
      if (this.get('consultationSubtype.id') === consultationSubtype.get('id')) {
        this.set('consultationSubtype', null);
      }
    },

    addConsultationReason(consultationReason) {
      this.set('consultationReason', consultationReason);
    },

    removeConsultationReason(consultationReason) {
      if (this.get('consultationReason.id') === consultationReason.get('id')) {
        this.set('consultationReason', null);
      }
    },

    searchPatients(chars) {
      this.changeset.set('patientLastName', chars)
      return this.get('fetchPatients').perform(chars)
    }
  }
});
