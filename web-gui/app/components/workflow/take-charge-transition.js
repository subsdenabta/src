/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';
import { inject as service } from '@ember/service';
import { computed, array, raw } from 'ember-awesome-macros';
import moment from 'moment';

export default Component.extend(FileSaverMixin, {
  store: service(),
  currentUser: service(),
  router: service(),

  certificateTemplates: computed(function() {
    return this.store.findAll('certificate-template');
  }),
  enabledCertificateTemplates: array.filterBy('certificateTemplates', raw('isEnabled')),
  availableCertificateTemplates: computed('enabledCertificateTemplates.@each.hasRole', 'currentUser.role', function (certificateTemplates, role) {
    let templates = certificateTemplates.filter((template) => {
      return template.get('hasRole')(role);
    });

    if (templates.get('length') > 0) {
      templates.pushObject(undefined)
    }

    return templates;
  }),

  appointmentOwnedByMe: computed('appointment.userInCharge.id', 'currentUser.user.id', function(userInChargeId, currentUserId) {
    return userInChargeId === currentUserId;
  }),

  actions: {

    takeCharge(something) {
      let appointment = this.appointment;
      appointment.startTrack();
      appointment.set('userInCharge', this.get('currentUser.user'));

      return appointment.save().then((savedAppointment) => {
        this.flashMessages.success('Vous venez de prendre en charge cette consultation.')
        if (something) {
          if (something.then) {
            return something.then((downloadableMasterDocument) => {
              return downloadableMasterDocument.download().then((content) => {
                let filenameComponents = [
                  savedAppointment.get('patient.fullName'),
                  savedAppointment.get('onmlId'),
                  moment(savedAppointment.get('startsAt')).format('DDMMYYYY-HHmm')
                ].compact()
                let filename = filenameComponents.join(' - ') + '.' + downloadableMasterDocument.get('filename').split('.').get('lastObject')
                this.saveFileAs(filename, content, downloadableMasterDocument.get('contentType'))
                this.router.transitionTo('certificates.edit', savedAppointment.id);
              }).catch((e) => {
                this.flashMessages.danger("Impossible de télécharger le certificat vierge. Vous devriez annuler la prise en charge et essayer à nouveau. " + e.message)
              });
            }).catch((e) => {
              this.flashMessages.danger("Impossible de récupérer la maquette de certificat. Vous devriez annuler la prise en charge et essayer à nouveau. " + e.message)
            });
          }
          else {
            let questionnaire = something;
            appointment.get('certificate').then((certificate) => {
              certificate.set('questionnaire', questionnaire);
              certificate.save().then(() => {
                this.router.transitionTo('certificates.edit', savedAppointment.id);
              }).catch((e) => {
                this.flashMessages.danger("Impossible de charger la maquette embarquée. Vous devriez annuler la prise en charge et essayer à nouveau. " + e.message)
              })
            })
          }
        }
        else {
          this.router.transitionTo('certificates.edit', savedAppointment.id);
        }
      }).catch((e) => {
        appointment.rollback();
        this.flashMessages.danger("Impossible de prendre en charge cette consultation. Vous devriez essayer à nouveau. " + e.message)
      });
    },

    resignCharge() {
      let appointment = this.appointment;

      appointment.get('certificate').then((certificate) => {
        certificate.set('questionnaire', null);
        certificate.set('answers', null);
        return certificate.save();
      }).then(() => {
        appointment.startTrack();
        appointment.set('userInCharge', null);

        appointment.save().then(() => {
          this.flashMessages.success('La consultation n\'est plus prise en charge par vous.')
        }).catch(() => {
          appointment.rollback();
          this.flashMessages.danger('Désolé, une erreur a été rencontrée ; merci de réessayer… Si cela se reproduit, veuillez prendre contact avec un administrateur.')
        });
      });
    },

  }
});
