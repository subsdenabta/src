/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import moment from 'moment';
import { reads } from '@ember/object/computed';

export default Component.extend({
  officerEmail: reads('appointment.officerEmail'),

  actions: {
    toggleSendByEmail() {
      this.toggleProperty('shouldSendByEmail')
    },

    sendByEmail() {
      return this.get('appointment.certificate').then((certificate) => {
        certificate.sendByMail({ recipient: this.officerEmail}).then(() => {
          this.flashMessages.success("Envoi du certificat mis en file d'attente avec succès, il devrait partir d'ici une minute.")
          certificate.set('deliveredAt', moment().toDate()) // Optimistic and transient. The API will set the definitive one.
          this.toggleProperty('shouldSendByEmail')
        }).catch(() => {
          certificate.rollbackAttributes();
          this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer… Si cela se reproduit, veuillez prendre contact avec un administrateur.')
        });
      });
    },

    delivered() {
      return this.get('appointment.certificate').then((certificate) => {
        certificate.set('deliveredAt', moment().toDate())
        certificate.save().then(() => {
          this.flashMessages.success('Vous avez indiqué que le certificat a été envoyé.')
        }).catch(() => {
          certificate.rollbackAttributes();
          this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer… Si cela se reproduit, veuillez prendre contact avec un administrateur.')
        });
      });
    },

    undeliver() {
      return this.get('appointment.certificate').then((certificate) => {
        certificate.set('deliveredAt', null)
        certificate.save().then(() => {
          this.flashMessages.success('Finalement, il semblerait que le certificat n\'ait pas été envoyé. C\'est noté.')
        }).catch(() => {
          certificate.rollbackAttributes();
          this.flashMessages.danger('Désolé, une erreur a été rencontrée. Merci de réessayer… Si cela se reproduit, veuillez prendre contact avec un administrateur.')
        });
      });
    },
  }
});
