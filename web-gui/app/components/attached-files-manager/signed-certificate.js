/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import AttachedFilesManager from '../attached-files-manager';
import moment from 'moment';

export default AttachedFilesManager.extend({
  actions: {
    preview(attachedFile) {
      attachedFile.get('attachable').then((attachable) => {
        attachable.preview().then((content) => {
          this.saveFileAs(attachable.get('filename'), content, attachable.get('contentType'))
        }).catch((e) => {
          this.flashMessages.danger("Impossible de récupérer l'aperçu du document. " + e.message)
        });
      })
    },

    validate(attachedFile) {
      attachedFile.get('attachable').then((certificate) => {
        certificate.set('validatedAt', moment())

        return certificate.save().then(() => {
          this.flashMessages.success('Certificat validé.')
        }).catch(() => {
          this.flashMessages.danger('Impossible de valider le certificat.')
        });
      })
    }
  }
});
