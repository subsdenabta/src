/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
import { dateFormat, genders } from '../../constants';
import { Changeset } from 'ember-changeset';
import moment from 'moment';

export default class PatientEditFormComponent extends Component {
  @service flashMessages;

  @tracked changeset;
  @tracked birthdate;
  @tracked birthdateValid = false;

  constructor() {
    super(...arguments);

    this.changeset = Changeset(this.args.patient, this.validate);
    if (this.changeset.birthdate) {
      this.birthdate = moment(this.changeset.birthdate).format(dateFormat);
    }
  }

  @action
  setBirthdate(formattedBirthdate) {
    this.birthdate = formattedBirthdate;
    this.birthdateValid = false;

    if (isPresent(formattedBirthdate)) {
      let newDate =  moment(formattedBirthdate, dateFormat, true);
      this.birthdateValid = newDate.isValid();
      if (this.birthdateValid) {
        this.changeset.set('birthdate', newDate.toDate());
      }
    }
    else {
      this.changeset.set('birthdate', null);
      this.birthdateValid = true;
    }
  }

  get patientGenders() {
    return genders
  }

  @action
  validate() {
    return true
  }

  @action
  submit() {
    return this.changeset.save().then(() => {
      this.flashMessages.success('Mise à jour réussie.')
      this.args.parentRoute();
    }).catch((err) => {
      err.errors.forEach((error) => {
        let key = error.source.pointer.split('/').pop().camelize();
        let code = error.code;
        changeset.pushErrors(key,this.intl.t('form.error.'+code));
      })
      this.flashMessages.danger('Impossible de sauvegarder vos changements.')
    });
  }
}
