/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { task, timeout } from 'ember-concurrency';
import { genders } from '../../constants';

export default class PatientAutocompleteComponent extends Component {
  @service
  store;

  genders = genders;

  @task
  *fetchPatients(chars) {
    yield timeout(1000);
    return this.store.query('patient', {
      filter: {
        last_name: chars
      }
    })
  }

  @action
  searchPatients(chars) {
    this.args.changeset.patientLastName = chars;
    return this.fetchPatients.perform(chars)
  }
}
