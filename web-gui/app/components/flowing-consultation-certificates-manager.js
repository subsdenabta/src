/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import moment from 'moment';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';

export default Component.extend(FileSaverMixin, {
  store: service(),
  tagName: 'span',
  shouldDisplayCertificatesManager: false,

  actions: {
    toggleCertificatesDialog() {
      this.toggleProperty('shouldDisplayCertificatesManager');
    },

    downloadMasterDocument(certificateTemplate) {
      let flowingConsultation = this.flowingConsultation
      let masterDocument = certificateTemplate.get('masterDocument');

      if (masterDocument.get('id')) {
        masterDocument.then((downloadableMasterDocument) => {
          downloadableMasterDocument.download().then((content) => {
            let filenameComponents = [
              flowingConsultation.get('patient.fullName'),
              flowingConsultation.get('onmlId'),
              flowingConsultation.get('consultationType.title'),
              moment().format('DDMMYYYY-HHmm')
            ].compact()
            let filename = filenameComponents.join(' - ') + '.' + downloadableMasterDocument.get('filename').split('.').get('lastObject')
            this.saveFileAs(filename, content, downloadableMasterDocument.get('contentType'))
          }).catch((e) => {
            this.flashMessages.danger("Impossible de récupérer un certificat vierge. " + e.message)
          });
        })
      }
    },

    uploadCertificate(file) {
      let attachable = this.flowingConsultation
      let adapter = this.store.adapterFor('application')
      let baseURL = adapter.buildURL(attachable.constructor.modelName, attachable.get('id'))
      let authHeader = adapter.get('authHeader')

      file.upload(`${baseURL}/certificates`, { headers: authHeader}).then( response => {
        if(response.status < 400) {
          this.store.pushPayload(response.body)
        } else {
          this.flashMessages.danger('Impossible d\'attacher le certificat rédigé.')
        }
      })
    }
  }
});
