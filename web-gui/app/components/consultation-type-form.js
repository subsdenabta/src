/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { reads } from '@ember/object/computed';
import { computed as awesomeComputed, array } from 'ember-awesome-macros';
import DS from 'ember-data';

const NOT_FOUND = -1;

export default Component.extend({
  store: service(),

  assignableRoles: computed(function () {
    return this.store.findAll('role');
  }),

  checkedRoles: computed('changeset.data.roles', {
    get(/* key */) {
      return this.get('changeset.data.roles');
    },

    set(key, value) {
      this.changeset.set('roles', value)
      return value;
    }
  }),

  checkedAssignedRoles: computed('changeset.data.assignedRoles', {
    get(/* key */) {
      return this.get('changeset.data.assignedRoles');
    },

    set(key, value) {
      this.changeset.set('assignedRoles', value)
      return value;
    }
  }),

  interventionDomain: reads('changeset.interventionDomain'),
  allConsultationTypes: reads('interventionDomain.consultationTypes'),
  consultationTypes: awesomeComputed('allConsultationTypes', function (allConsultationTypes) {
    return allConsultationTypes.filter(consultationType => consultationType.id !== this.changeset.id)
  }),

  complementaryConsultationTypes: computed('changeset.data.complementaryConsultationTypes', {
    get(/* key */) {
      return this.get('changeset.data.complementaryConsultationTypes');
    },

    set(key, value) {
      this.changeset.set('complementaryConsultationTypes', value)
      return value;
    }
  }),

  actions: {
    toggleRole(changeset, role) {
      let roles = this.checkedRoles;
      let indexOfRole = roles.indexOf(role);

      if (indexOfRole === NOT_FOUND) {
        roles.unshiftObject(role);
      }
      else {
        roles.removeAt(indexOfRole);
      }

      this.set('checkedRoles', roles);
      this.notifyPropertyChange('checkedRoles');
    },

    toggleAssignedRole(changeset, role) {
      let roles = this.checkedAssignedRoles;
      let indexOfRole = roles.indexOf(role);

      if (indexOfRole === NOT_FOUND) {
        roles.unshiftObject(role);
      }
      else {
        roles.removeAt(indexOfRole);
      }

      this.set('checkedAssignedRoles', roles);
      this.notifyPropertyChange('checkedAssignedRoles');
    },

    toggleComplementationConsultationType(changeset, consultationType) {
      let consultationTypes = this.complementaryConsultationTypes;
      let indexOfRole = consultationTypes.indexOf(consultationType);

      if (indexOfRole === NOT_FOUND) {
        consultationTypes.unshiftObject(consultationType);
      }
      else {
        consultationTypes.removeAt(indexOfRole);
      }

      this.set('complementaryConsultationTypes', consultationTypes);
      this.notifyPropertyChange('complementaryConsultationTypes');
    }
  }
});
