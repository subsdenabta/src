/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class FlowingConsultationsReasonBadge extends Component {
  @tracked showPatientForm = false;

  get title() {
    return [
      this.args.flowingConsultation.interventionDomain.get('title'),
      this.args.flowingConsultation.consultationType.get('title')
    ].compact().join(' - ')
  }

  get text() {
    return this.args.flowingConsultation.consultationType.get('title') ||
      this.args.flowingConsultation.interventionDomain.get('title');
  }
}
