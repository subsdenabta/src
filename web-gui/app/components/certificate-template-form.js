/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { sort } from '@ember/object/computed';

const NOT_FOUND = -1;

export default Component.extend({
  store: service(),

  rolesSorting: Object.freeze(['title']),
  roles: computed('changeset', function () {
    return this.store.findAll('role');
  }),
  sortedRoles: sort('roles', 'rolesSorting'),

  checkedRoles: computed('changeset.data.roles', {
    get(/* key */) {
      return this.get('changeset.data.roles');
    },

    set(key, value) {
      this.changeset.set('roles', value)
      return value;
    }
  }),

  questionnaire: computed('changeset.data.questionnaire', {
    get() {
      return JSON.stringify(this.get('changeset.data.questionnaire'));
    },

    set(key, value) {
      this.set('changeset.questionnaire', JSON.parse(value));
      return value;
    }
  }),

  actions: {
    toggleRole(changeset, role) {
      let roles = this.checkedRoles;
      let indexOfRole = roles.indexOf(role);

      if (indexOfRole === NOT_FOUND) {
        roles.unshiftObject(role);
      }
      else {
        roles.removeAt(indexOfRole);
      }

      this.set('checkedRoles', roles);
      this.notifyPropertyChange('checkedRoles');
    }
  }
});
