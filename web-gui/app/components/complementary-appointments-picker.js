/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { mapBy } from '@ember/object/computed';
import { w } from '@ember/string';
import moment from 'moment';

export default Component.extend({

  complementaryAppointmentsConsultationTypes: mapBy('appointment.activeComplementaryAppointments', 'consultationType'),
  complementaryAppointmentsConsultationTypesIds: mapBy('complementaryAppointmentsConsultationTypes', 'id'),

  actions: {

    cancelComplementaryAppointment(consultationType) {
      let complementaryAppointments = this.get('appointment.activeComplementaryAppointments');

      let appointment = complementaryAppointments.findBy('consultationType.id', consultationType.get('id'));

      appointment.set('cancelledAt', moment().toDate());
      appointment.set('cancelledBy', 'patient');
      appointment.save().catch(() => {
        appointment.rollbackAttributes();
      });
    },

    addComplementaryAppointment(consultationType) {
      let appointment = this.appointment

      let commonAttributes = w('id onmlId');
      let workflowAttributes = w('cancelledAt cancelledBy previousAppointment previousAppointmentPostponedBy postponedBy originatingAppointment complementaryAppointments certificate userInCharge patientReleasedAt attachedFiles')
      let consultationTypeRelatedAttributes = w('consultationSubtype consultationReason');

      let copyOptions = {
        ignoreAttributes: commonAttributes + workflowAttributes + consultationTypeRelatedAttributes,
        overwrite: {consultationType: consultationType, originatingAppointment: appointment, patient: appointment.patient}
      }

      appointment.copy(false, copyOptions).then((appointmentCopy) => {
        appointmentCopy.save();
      })
    }


  }

});
