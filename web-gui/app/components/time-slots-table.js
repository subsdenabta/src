/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import Changeset from 'ember-changeset';

const dayNames = [
  'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'
]

export default Component.extend({
  store: service(),
  dayNames,

  title: computed('timeSlotChangeset.consultationType.title', 'timeSlotChangeset.readableWeekDay', function () {
    return "Créneau "+this.timeSlotChangeset.consultationType.title+" le "+this.timeSlotChangeset.readableWeekDay;
  }),

  typeSlotByConsultationType: computed('schedule.timeSlots.[]', function () {
    let result = []

    for (let consultationType of this.consultationTypes.toArray()) {
      let consultationObject = {
        consultationType: consultationType,
        weekDays: weekDays(this.schedule.get('timeSlots').filterBy('consultationType.id', consultationType.id))
      }
      result.pushObject(consultationObject);
    }

    return result;
  }),

  validateTimeSlot() {
    true
  },

  actions: {
    startCreating(schedule, consultationType, weekDay) {
      let timeSlot = this.store.createRecord('time-slot', {
        schedule: schedule,
        consultationType: consultationType,
        weekDay: weekDay
      });

      let validator = this.validateTimeSlot;
      this.set('timeSlotChangeset', new Changeset(timeSlot, validator));
    },

    startEditing(timeSlot) {
      let validator = this.validateTimeSlot;
      this.set('timeSlotChangeset', new Changeset(timeSlot, validator));
    },

    saveChanges(changeset) {
      changeset.save().then(() => {
        this.set('timeSlotChangeset', null);
      })
    },

    stopEditing(changeset) {
      changeset.get('data').rollbackAttributes();
      this.set('timeSlotChangeset', null)
    },

    destroy(changeset) {
      changeset.destroyRecord();
    }
  }
});

function weekDays(timeSlots) {
  let result = []

  for (let index of [...Array(7).keys()]) {
    let weekDay = index + 1
    result[index] = {
      weekDay: weekDay,
      timeSlots: []
    }
  }

  for (let timeSlot of timeSlots) {
    result[timeSlot.get('weekDay') - 1].timeSlots.pushObject(timeSlot);
  }

  return result;
}
