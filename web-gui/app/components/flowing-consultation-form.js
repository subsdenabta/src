/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { computed } from '@ember/object';
import { filterBy } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
import moment from 'moment';
import { task, timeout } from 'ember-concurrency';

const dateTimeFormat = "DD/MM/YYYY HH:mm"
const patientGenders = ['woman', 'man', 'unknown']

export default Component.extend({
  store: service(),

  onSubmitTask: task(function * () {
    yield this.onSubmit()
  }).drop(),

  isNewRecord: computed('changeset.isNew', function () {
    return this.get('changeset.isNew')
  }),

  isMessage: computed('changeset.officerLambdaUser', function () {
    return this.get('changeset.officerLambdaUser');
  }),

  spokespersonLegend: computed('isMessage', function () {
    return this.isMessage ? 'Interlocuteur' : 'OPJ';
  }),

  commentLabel: computed('isMessage', function () {
    return this.isMessage ? 'Message' : 'Remarques';
  }),

  flexClass: computed('isMessage', function () {
    return this.isMessage ? 'flex-gt-xs-50' : 'flex-gt-xs-33';
  }),

  calledAt: computed('changeset.calledAt', {
    get(/* key */) {
      let callTime = this.changeset.calledAt;
      let callMoment = moment(callTime);

      return callMoment.format(dateTimeFormat);
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('calledAt', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('calledAt', null);
      }
      return value;
    }
  }),

  callingInstitutions: computed(function () {
    return this.store.findAll('calling-institution', {include: 'calling_area'});
  }),

  patientGenders,

  isPatientSet: computed('changeset.patient.id', function() {
    return this.get('changeset.patient.id') != null
  }),

  ageHint: computed('changeset.patientBirthYear', function () {
    let birthYear = this.get('changeset.patientBirthYear');

    if (isPresent(birthYear)) {
      let currentYear = moment().year();
      let age = currentYear - parseInt(birthYear);
      if (age < 2) {
        return "1 an ou moins…"
      }
      else {
        return `${age - 1}-${age} ans`;
      }
    }

    return undefined;
  }),

  consultationTypes: computed(function () {
    let interventionDomain = this.get('changeset.interventionDomain') || this.get('changeset.consultationType.interventionDomain');

    return interventionDomain.get('consultationTypes');
  }),

  consultationTypeAffected: computed('changeset.consultationType', function () {
    return !!this.changeset.consultationType;
  }),

  users: computed(function () {
    return this.store.findAll('user');
  }),

  physicians: filterBy('users', 'isPhysician'),

  assigned: computed('changeset.assignee', function () {
    return this.get('changeset.assignee.id');
  }),

  assignee: computed('changeset.assignee', {
    get(/* key */) {
      return this.changeset.assignee;
    },

    set(key, value) {
      if (isPresent(value)) {
        this.changeset.set('assignee', value);
        this.changeset.set('assignedAt', moment().toDate());
      }
      else {
        this.changeset.set('assignee', null);
        this.changeset.set('assignedAt', null);
      }
      return value;
    }
  }),

  assignedAt: computed('changeset.assignedAt', {
    get(/* key */) {
      let callTime = this.get('changeset.assignedAt');

      if (isPresent(callTime)) {
        return moment(callTime).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('assignedAt', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('assignedAt', null);
      }
      return value;
    }
  }),

  validatedAt: computed('changeset.validatedAt', {
    get(/* key */) {
      let callTime = this.get('changeset.validatedAt');

      if (isPresent(callTime)) {
        return moment(callTime).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('validatedAt', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('validatedAt', null);
      }
      return value;
    }
  }),

  cancelledAt: computed('changeset.cancelledAt', {
    get(/* key */) {
      let callTime = this.get('changeset.cancelledAt');

      if (isPresent(callTime)) {
        return moment(callTime).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.changeset.set('cancelledAt', newMoment.toDate());
        }
      }
      else {
        this.changeset.set('cancelledAt', null);
      }
      return value;
    }
  }),

  isFormDisabled: computed('changeset.isInvalid', 'isMessage', 'consultationTypeAffected', function () {
    return this.get('changeset.isInvalid') || (!this.isMessage && !this.consultationTypeAffected);
  }),

  fetchPatients: task(function * (chars) {
    yield timeout(1000)
    return this.store.query('patient', {
      filter: {
        last_name: chars
      }
    })
  }),

  actions: {
    setDateAtNow(key) {
      this.set(key, moment().format(dateTimeFormat));
    },

    searchPatients(chars) {
      return this.get('fetchPatients').perform(chars)
    }
  }
});
