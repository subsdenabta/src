/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { computed } from '@ember/object';
import { reads } from '@ember/object/computed';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';
import moment from 'moment';

export default Component.extend(FileSaverMixin, {
  officerEmail: reads('certificate.flowingConsultation.officerEmail'),

  fileToDownload: computed('certificate.{validated,validatedCertificate,signedCertificate}', function() {
    if (this.get('certificate.validated')) {
      return this.get('certificate.validatedCertificate')
    } else {
      return this.get('certificate.signedCertificate')
    }
  }),

  actions: {
    delete() {
      this.certificate.destroyRecord().catch(() => {
        this.flashMessages.danger('Impossible de supprimer le fichier attaché.')
      })
    },

    downloadAttachedFile() {
      let fileToDownload = this.fileToDownload

      fileToDownload.download().then((content) => {
        this.saveFileAs(fileToDownload.get('filename'), content, fileToDownload.get('contentType'))
      }).catch(() => {
        this.flashMessages.danger('Impossible de récupérer le fichier.')
      });
    },

    previewAttachedFile() {
      let signedCertificate = this.get('certificate.signedCertificate')

      this.certificate.preview().then((content) => {
        this.saveFileAs(`Aperçu de ${signedCertificate.get('filename')}.pdf`, content, content.type)
      }).catch(() => {
        this.flashMessages.danger(`Impossible de générer l'aperçu PDF du fichier.`)
      });
    },

    sendByMail() {
      let certificate = this.certificate

      certificate.sendByMail({ recipient: this.officerEmail}).then(() => {
        this.flashMessages.success(`Envoi du certificat mis en file d'attente avec succès, il devrait partir d'ici une minute.`)
        this.toggleProperty('shouldSendByMail')
      }).catch(() => {
        this.flashMessages.danger(`Impossible d'envoyer le certificat par email, merci d'essayer de nouveau et de contacter un administrateur en cas de récidive.`)
      })
    },

    toggleSendByMail() {
      this.toggleProperty('shouldSendByMail')
    },

    validate() {
      let certificate = this.certificate

      certificate.set('validatedAt', moment())
      certificate.save().then(() => {
        this.flashMessages.success('Certificat validé avec succès.')
      }).catch(() => {
        this.flashMessages.danger(`Impossible de valider le certificat, merci d'essayer de nouveau et de contacter un administrateur en cas de récidive.`)
      })
    },

    unvalidate() {
      let certificate = this.certificate

      certificate.set('validatedAt', null)
      certificate.save().then(() => {
        this.flashMessages.success('Certificat invalidé avec succès.')
      }).catch(() => {
        this.flashMessages.danger(`Impossible de dévalider le certificat, merci d'essayer de nouveau et de contacter un administrateur en cas de récidive.`)
      })
    },
  }
});
