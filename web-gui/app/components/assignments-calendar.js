/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import Changeset from 'ember-changeset';

export default Component.extend({
  store: service(),

  actions: {
    startAssigning(fullCalendarEvent) {
      this.store.findRecord('duty-period', fullCalendarEvent.dutyPeriodId).then((dutyPeriod) => {
        this.set('dutyPeriodChangeset', new Changeset(dutyPeriod));
      })
    },

    saveChanges(changeset) {
      changeset.save().then(() => {
        this.set('dutyPeriodChangeset', null);
      })
    },

    stopAssigning(changeset) {
      changeset.get('data').rollbackAttributes();
      this.set('dutyPeriodChangeset', null)
    }
  }

});
