/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@ember/component';
import { computed } from 'ember-awesome-macros'
import { and, equal } from '@ember/object/computed'
import Changeset from 'ember-changeset';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),

  medicalFile: and('appointment.medicalFile.content.isLoaded', 'appointment.medicalFile.content'),
  changeset: computed('medicalFile', function(medicalFile) {
    if (medicalFile) {
      return new Changeset(medicalFile);
    }
    else {
      return undefined;
    }
  }),

  actions: {

    stopMedicalFileEdition() {
      this.changeset.rollback()
      this.onCloseToggle()
    },

    saveMedicalFile() {
      return this.changeset.save().then(() => {
        this.onCloseToggle()
        this.flashMessages.success('Dossier médical mis à jour.')
      });
    }
  }
});
