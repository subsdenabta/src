/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { isPresent } from '@ember/utils';
import moment from 'moment';

const dateTimeFormat = "DD/MM/YYYY HH:mm"

export default class UserDisableFormComponent extends Component {
  @tracked disabledAt;
  @tracked disabledAtValid = false;

  constructor() {
    super(...arguments);

    let disabledMoment = moment();
    if (this.args.user.disabledAt) {
      disabledMoment = moment(this.args.user.disabledAt);
    }
    this.setDisabledAt(disabledMoment.format(dateTimeFormat));
  }

  @action
  setDisabledAt(formattedDisabledAt) {
    this.disabledAt = formattedDisabledAt;
    this.disabledAtValid = false;

    if (isPresent(formattedDisabledAt)) {
      let newMoment =  moment(formattedDisabledAt, dateTimeFormat, true);
      this.disabledAtValid = newMoment.isValid();
      if (this.disabledAtValid) {
        this.args.user.set('disabledAt', newMoment.toDate());
      }
    }
    else {
      this.args.user.set('disabledAt', null);
      this.disabledAtValid = true;
    }
  }

  @action
  setDateAtNow() {
    this.setDisabledAt(moment().format(dateTimeFormat));
  }

}
