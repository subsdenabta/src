/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { reads } from '@ember/object/computed';

export default Controller.extend({
  intl: service(),

  setting: reads('model'),
  settingName: reads('setting.constructor.modelName'),

  formComponent: computed('settingName', function () {
    return this.settingName + '-form';
  }),

  intlSettingRoot: computed('settingName', function () {
    return 'setting.' + this.settingName + '.';
  }),

  actions: {
    validate(/* { key, newValue, oldValue, changes, content } */) {
      return true
    },

    save(changeset) {
      return changeset.save().then(() => {
        this.flashMessages.success('Mise à jour réussie.')
        this.transitionToRoute(changeset.get('transitionBack'));
      }).catch((err) => {
        err.errors.forEach((error) => {
          let key = error.source.pointer.split('/').pop().camelize();
          let code = error.code;
          changeset.pushErrors(key,this.intl.t('form.error.'+code));
        })
        this.flashMessages.danger('Impossible de sauvegarder vos changements.')
      });
    }
  }
});
