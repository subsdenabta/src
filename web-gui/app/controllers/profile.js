/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { reads } from '@ember/object/computed';

export default Controller.extend({
  currentUser: service(),
  me: reads('currentUser.user'),
  changePasswordToggled: false,

  printedSignature: reads('me.printedSignature'),

  actions: {
    deletePrintedSignature() {
      this.get('me.printedSignatures').forEach((sig) => {
        sig.destroyRecord()
      })
    },

    togglePasswordChange() {
      this.toggleProperty('changePasswordToggled');
    },

    mutPassword(changeset, value) {
      changeset.set('password', value);
      changeset.set('passwordConfirmation', undefined);
    },

    validate({ key, newValue, changes }) {
      let errors = [];

      if ('password' === key) {
        if (newValue.length < 8) {
          errors.pushObject('doit faire plus de 8 caractères')
        }
        if (!newValue.match(/(?=.*[0-9])(?=.*[A-Z])/)) {
          errors.pushObject('doit contenir au moins un chiffre et une majuscule')
        }
      }
      else if ('passwordConfirmation' === key) {
        if (newValue != changes.password) {
          errors.pushObject("doit être identique au mot de passe saisi")
        }
      }

      return errors.length === 0 || errors;
    },

    save(changeset) {
      changeset.save().then(() =>{
        let model = this.me;
        model.set('password', null);
        model.set('passwordConfirmation', null);
        this.toggleProperty('changePasswordToggled');
        this.flashMessages.success('Mise à jour du mot de passe réussie.')
      })
    },

    rollback(changeset) {
      this.toggleProperty('changePasswordToggled');
      return changeset.rollback();
    },

    uploadPrintedSignature(file) {
      let attachable = this.me
      let adapter = this.store.adapterFor('application')
      let baseURL = adapter.buildURL(attachable.constructor.modelName, attachable.get('id'))
      let authHeader = adapter.get('authHeader')

      file.upload(`${baseURL}/attached_files?type=printed-signature`, { headers: authHeader}).then( response => {
        if(response.status < 400) {
          this.store.pushPayload(response.body)
        } else {
          this.flashMessages.danger('Impossible d\'attacher la signature numérisée.')
        }
      })
    }
  }

});
