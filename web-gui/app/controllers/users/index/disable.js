/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { reads } from '@ember/object/computed';
import moment from 'moment';

export default Controller.extend({
  user: reads('model'),

  @action
  save() {
    let userName = this.user.fullName;

    let successMessage = `La désactivation de l'utilisateur ${userName} est maintenant annulée.`
    if (this.user.disabledAt) {
      let deactivationTime = moment(this.user.disabledAt).format('LLLL');
      successMessage = `La désactivation de l'utilisateur ${userName} est planifiée pour le ${deactivationTime}`;
    }

    this.user.save().then(() => {
      this.flashMessages.success(successMessage);
      this.backToIndex();
    }).catch(() => {
      this.flashMessages.danger(`Impossible de désactiver l'utilisateur ${userName}.`)
    });
  },

  @action
  backToIndex() {
    this.transitionToRoute('users');
  }

});
