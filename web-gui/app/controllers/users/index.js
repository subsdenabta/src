/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { action } from '@ember/object';

export default Controller.extend({

  get roles() {
    return this.model.roles.value.toArray();
  },

  get users() {
    return this.model.users.value.toArray();
  },

  @action
  enable(user) {
    let userName = user.get('fullName');

    user.set('disabledAt', null);
    user.save().then(() => {
      this.flashMessages.success(`L'utilisateur ${userName} vient d'être activé.`);
    }).catch(() => {
      this.flashMessages.danger(`Impossible d'activer l'utilisateur ${userName}.`)
    });
  }

});
