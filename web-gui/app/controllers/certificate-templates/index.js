/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { reads, filterBy } from '@ember/object/computed';

export default Controller.extend({
  certificateLayout: computed(function () {
    let promise = this.store.findRecord('certificate-layout', 'certificate_layout')

    return promise;
  }),

  certificateTemplates: reads('model'),
  enabledCertificateTemplates: filterBy('certificateTemplates', 'isEnabled'),
  displayedCertificateTemplates: computed('certificateTemplates', 'enabledCertificateTemplates', 'shouldDisplayDisabledCertificateTemplates', function() {
    if (this.shouldDisplayDisabledCertificateTemplates) {
      return this.certificateTemplates;
    }
    else {
      return this.enabledCertificateTemplates;
    }
  })

});
