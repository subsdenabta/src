/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed, array } from 'ember-awesome-macros';
import moment from 'moment';

export default Controller.extend({
  currentUser: service(),

  greetings: computed(function () {
    let hour = moment().hour();
    let greetings = "night";

    if (hour >= 6) {
      greetings = "day";
    }
    if (hour >= 13) {
      greetings = "afternoon";
    }
    if (hour >= 18) {
      greetings = "evening";
    }
    if (hour >= 22) {
      greetings = "night";
    }

    return greetings;
  }),

  interventionDomains: array.sort('currentUser.interventionDomains', ['modusOperandi:asc', 'id:asc'])
});
