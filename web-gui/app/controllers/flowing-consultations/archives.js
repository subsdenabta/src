/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import EmberObject, { computed, observer } from '@ember/object';
import { reads, filterBy, sort } from '@ember/object/computed';
import moment from 'moment';

const patientGenders = ['woman', 'man', 'unknown', null]
const dateFormat = 'DD-MM-YYYY'
const dateTimeFormat = dateFormat+' HH:mm'
const actions = ['called_at', 'assigned_at', 'cancelled_at', 'validated_at']
const statuses = ['cancelled', 'validated']

export default Controller.extend({
  session: service(),
  intl: service(),

  // Common

  interventionDomain: reads('model.interventionDomain'),

  blankOption: computed( function() {
    return EmberObject.create({ title: 'Tous', abridgedName: 'Tous'})
  }),

  flowingConsultations: computed('interventionDomain', 'filterDidChange', function() {
    return this.store.query('flowing-consultation', {
      filter: {
        intervention_domain_id: this.get('interventionDomain.id'),
        status: this.get('selectedStatus.id'),
        action: this.get('selectedAction.id'),
        start_date_time: this.startDateTime,
        end_date_time: this.endDateTime,
        consultation_type_id: this.get('selectedConsultationType.id'),
        calling_institution_id: this.get('selectedCallingInstitution.id'),
        patient_gender: this.get('selectedPatientGender.id'),
        patient_last_name: this.patientLastName,
        patient_age_range: this.get('selectedPatientAgeRange.id'),
        assignee_id: this.get('selectedPhysician.id'),
        state: 'archive'
      },
      sort: '-called_at'
    });
  }),

  flowingConsultationsObserver: observer('flowingConsultations', function() {
    this.set('isFetchingFlowingConsultations', true)

    this.flowingConsultations.then(() => {
      this.set('isFetchingFlowingConsultations', false)
    })
  }).on('init'),

  dateFormat,

  // Status

  selectedStatus: reads('blankOption'),

  statuses: computed( function() {
    let blankStatus = this.blankOption;
    let statusObjects = [blankStatus];

    return statusObjects.pushObjects(statuses.map( (status) => {
      return EmberObject.create({ title: this.intl.t('flowing-consultation.status.'+status), id: status});
    }));
  }),

  // Action

  selectedAction: reads('calledAtOption'),

  calledAtOption: computed( function() {
    return this.actionsWithBlank.find(action => action.id === 'called_at');
  }),

  actionsWithBlank: computed( function() {
    let blankAction = this.blankOption;
    let actionObjects = [blankAction];

    return actionObjects.pushObjects(actions.map( (action) => {
      return EmberObject.create({ title: this.intl.t('flowing-consultation.action.'+action), id: action});
    }));
  }),

  startDate: computed(function() {
    return moment().subtract(2, 'days').toDate()
  }),

  startDateTime: computed('startDate', 'startTime', function() {
    if (this.startDate) {
      let startDate = moment(this.startDate).format(dateFormat)
      let startTime = this.startTime || "00:00"
      let startDateTime = moment(startDate+' '+standardizeTime(startTime), dateTimeFormat, true)

      if (startDateTime.isValid()) {
        return startDateTime.toISOString()
      }
    }

    return undefined
  }),

  endDateTime: computed('endDate', 'endTime', function() {
    if (this.endDate) {
      let endDate = moment(this.endDate).format(dateFormat)
      let endTime = this.endTime || "23:59"
      let endDateTime = moment(endDate+' '+standardizeTime(endTime), dateTimeFormat, true)

      if (endDateTime.isValid()) {
        return endDateTime.toISOString()
      }
    }

    return undefined
  }),

  // Consultation type

  selectedConsultationType: reads('blankOption'),

  consultationTypes: reads('interventionDomain.consultationTypes'),

  consultationTypesWithBlank: computed('consultationTypes', function() {
    let blankConsultationType = this.blankOption;

    return this.consultationTypes.then( (consultationTypes) => {
      return consultationTypes.toArray().unshiftObjects([blankConsultationType])
    });
  }),

  // Calling institution

  selectedCallingInstitution: reads('blankOption'),

  callingInstitutions: computed(function() {
    return this.store.findAll('calling-institution');
  }),

  callingInstitutionsWithArea: computed('callingInstitutions.@each.titleWithArea', function() {
    return this.callingInstitutions.map(function (callingInstitution) {
      return EmberObject.create({ title: callingInstitution.get('titleWithArea'), id: callingInstitution.get('id')});
    });
  }),

  callingInstitutionsSorting: Object.freeze(['title']),
  sortedCallingInstitutions: sort('callingInstitutionsWithArea', 'callingInstitutionsSorting'),

  callingInstitutionsWithBlank: computed('sortedCallingInstitutions', function() {
    let blankCallingInstitution = this.blankOption;

    return this.sortedCallingInstitutions.toArray().unshiftObjects([blankCallingInstitution]);
  }),

  // Physician

  selectedPhysician: reads('blankOption'),

  users: computed(function () {
    return this.store.findAll('user', {reload: true});
  }),

  physicians: filterBy('users', 'isPhysician'),

  physiciansWithBlank: computed('physicians', function() {
    return this.physicians.toArray().unshiftObjects([this.blankOption]);
  }),

  // Patient gender

  selectedPatientGender: reads('blankOption'),

  patientGenders: computed( function() {
    let blankPatientGender = this.blankOption;
    let patientGenderObjects = [blankPatientGender];

    return patientGenderObjects.pushObjects(patientGenders.map( (gender) => {
      return EmberObject.create({ title: this.intl.t('patient.gender.'+gender), id: gender});
    }));
  }),

  // Patient age ranges

  patientAgeRanges: computed( function() {
    let blankPatientAgeRange = this.blankOption;
    let patientAgeRangeObjects = [blankPatientAgeRange];

    for(let i=0; i<13; i++) {
      let ageRangeLabel = (i*10).toString() + '-' + (i*10+9).toString()
      let ageRange = {title: ageRangeLabel, id: ageRangeLabel}
      patientAgeRangeObjects.pushObject(ageRange);
    }

    return patientAgeRangeObjects
  }),

  actions: {

    filter() {
      this.toggleProperty('filterDidChange');
    }
  }
});

function standardizeTime(time) {
  if (time) { return time.replace(/^(\d)(:\d\d)$/, "0$1$2"); }
  else { return time }
}
