/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { reads } from '@ember/object/computed';
import FileSaverMixin from 'ember-cli-file-saver/mixins/file-saver';

export default Controller.extend(FileSaverMixin, {
  appointment: reads('model'),
  consultationType: reads('appointment.consultationType'),

  actions: {
    downloadValidatedCertificate() {
      let attachedFile = this.get('appointment.validatedCertificate')

      attachedFile.download().then((content) => {
        this.saveFileAs(attachedFile.get('filename'), content, attachedFile.get('contentType'))
      }).catch((e) => {
        this.flashMessages.danger("Impossible de récupérer le certificat validé. " + e.message)
      });
    },

    save(survey) {
      return this.get('appointment.certificate').then((certificate) => {
        if (survey) {
          certificate.set('answers', JSON.stringify(survey.data))
        }

        return certificate.save().then(() => {
          this.transitionToRoute('appointments/conclusion', certificate.get('appointment.id'));
        }).catch(() => {
          this.flashMessages.danger('Impossible de sauvegarder vos changements.')
        });
      });
    },

    unvalidateCertificate() {
      return this.appointment.get('certificate').then((certificate) => {
        certificate.set('validatedAt', null);

        return certificate.save().then(() => {
          this.flashMessages.success('Certificat dévalidé.')
        }).catch((e) => {
          this.flashMessages.danger('Impossible de dévalider le certificat.' + e.message)
        });
      })
    },

    toggleMedicalFileEdition() {
      this.toggleProperty('needsToEditMedicalFile')
    }
  }
});
