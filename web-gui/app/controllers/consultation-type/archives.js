/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import EmberObject, { computed, observer } from '@ember/object';
import { reads, sort } from '@ember/object/computed';
import moment from 'moment';

const patientGenders = ['woman', 'man', 'unknown', null]
const dateFormat = 'DD-MM-YYYY'
const statuses = ['cancelled','postponed']
const certificateStatuses = ['completed','validated','delivered']

export default Controller.extend({
  intl: service(),

  consultationType: reads('model'),
  dateFormat,

  blankOption: computed( function() {
    return EmberObject.create({ title: 'Tous', abridgedName: 'Tous'})
  }),

  appointments: computed('consultationType.id', 'filterDidChange', function() {
    return this.store.query('appointment', {
      filter: {
        calling_institution_id: this.get('selectedCallingInstitution.id'),
        consultation_reason_id: this.get('selectedConsultationReason.id'),
        consultation_type_id: this.get('consultationType.id'),
        end_date: iso8601Date(this.endDate),
        official_report: this.officialReport,
        onml_id: this.onmlId,
        patient_age_range: this.get('selectedPatientAgeRange.id'),
        patient_gender: this.get('selectedPatientGender.id'),
        patient_last_name: this.patientLastName,
        patient_birthdate: iso8601Date(this.patientBirthdate),
        start_date: iso8601Date(this.startDate),
        user_in_charge_id: this.get('selectedUserInCharge.id')
      },
      sort: 'starts_at'
    });
  }),

  filteredAppointments: computed('appointments.[]', 'selectedStatus.id', 'selectedCertificateStatus.id', 'selectedEmergencyStatus.id', function() {
    let appointments = this.appointments
    let selectedStatus = this.get('selectedStatus.id')
    let selectedCertificateStatus = this.get('selectedCertificateStatus.id')
    let selectedEmergencyStatus = this.get('selectedEmergencyStatus.id')

    if (selectedStatus) {
      appointments = appointments.filterBy('status', selectedStatus)
    }

    if (selectedCertificateStatus) {
      appointments = appointments.filterBy('certificateStatus', selectedCertificateStatus)
    }

    if (selectedEmergencyStatus != undefined) {
      appointments = appointments.filterBy('isEmergency', selectedEmergencyStatus)
    }

    return appointments
  }),

  appointmentsObserver: observer('appointments', function() {
    this.set('isFetchingAppointments', true)

    this.appointments.then(() => {
      this.set('isFetchingAppointments', false)
    })
  }).on('init'),

  // Dates

  startDate: computed(function() {
    return moment().subtract(2, 'days').toDate()
  }),

  // Status

  selectedStatus: reads('blankOption'),

  statuses: computed( function() {
    let blankStatus = this.blankOption;
    let statusObjects = [blankStatus];

    statusObjects.pushObjects(statuses.map( (status) => {
      return EmberObject.create({ title: this.intl.t('appointment.is'+status.capitalize()), id: status});
    }));

    statusObjects.pushObject(EmberObject.create({ title: this.intl.t('appointment.patientStatus.patientMissing'), id: 'missing'}))

    return statusObjects
  }),

  // Certificate Status

  selectedCertificateStatus: reads('blankOption'),

  certificateStatuses: computed( function() {
    let blankStatus = this.blankOption;
    let statusObjects = [blankStatus];

    return statusObjects.pushObjects(certificateStatuses.map( (status) => {
      return EmberObject.create({ title: this.intl.t('appointment.certificate_status.'+status), id: status});
    }));
  }),

  // Emergency Status

  selectedEmergencyStatus: reads('blankOption'),

  emergencyStatuses: computed( function() {
    let blankStatus = this.blankOption;
    let statusObjects = [blankStatus];

    statusObjects.pushObject(EmberObject.create({ title: this.intl.t('misc.no'), id: false}))
    statusObjects.pushObject(EmberObject.create({ title: this.intl.t('misc.yes'), id: true}))

    return statusObjects
  }),

  // Consultation reason

  selectedConsultationReason: reads('blankOption'),

  consultationReasons: reads('consultationType.consultationReasons'),

  consultationReasonsWithBlank: computed('consultationReasons', function() {
    let blankConsultationReason = this.blankOption;

    return this.consultationReasons.then( (consultationReasons) => {
      return consultationReasons.toArray().unshiftObjects([blankConsultationReason])
    });
  }),

  // Calling institution

  selectedCallingInstitution: reads('blankOption'),

  callingInstitutions: computed(function() {
    return this.store.findAll('calling-institution');
  }),

  callingInstitutionsWithArea: computed('callingInstitutions.@each.titleWithArea', function() {
    return this.callingInstitutions.map(function (callingInstitution) {
      return EmberObject.create({ title: callingInstitution.get('titleWithArea'), id: callingInstitution.get('id')});
    });
  }),

  callingInstitutionsSorting: Object.freeze(['title']),
  sortedCallingInstitutions: sort('callingInstitutionsWithArea', 'callingInstitutionsSorting'),

  callingInstitutionsWithBlank: computed('sortedCallingInstitutions', function() {
    let blankCallingInstitution = this.blankOption;

    return this.sortedCallingInstitutions.toArray().unshiftObjects([blankCallingInstitution]);
  }),

  // User in charge

  selectedUserInCharge: reads('blankOption'),

  users: computed(function () {
    return this.store.findAll('user', {reload: true});
  }),

  usersInCharge: computed('users', 'consultationType.hasAssignedRole', function() {
    return this.users.then(users => {
      return users.filter(user => this.get('consultationType.hasAssignedRole')(user.get('role')))
    });
  }),

  usersInChargeWithBlank: computed('usersInCharge', function() {
    return this.usersInCharge.then(users => {
      return users.toArray().unshiftObjects([this.blankOption]);
    });
  }),

  // Patient gender

  selectedPatientGender: reads('blankOption'),

  patientGenders: computed( function() {
    let blankPatientGender = this.blankOption;
    let patientGenderObjects = [blankPatientGender];

    return patientGenderObjects.pushObjects(patientGenders.map( (gender) => {
      return EmberObject.create({ title: this.intl.t('patient.gender.'+gender), id: gender});
    }));
  }),

  // Patient age ranges

  patientAgeRanges: computed( function() {
    let blankPatientAgeRange = this.blankOption;
    let patientAgeRangeObjects = [blankPatientAgeRange];

    patientAgeRangeObjects.pushObject({title: 'Mineur', id: '0-17'})
    for(let i=0; i<13; i++) {
      let ageRangeLabel = (i*10).toString() + '-' + (i*10+9).toString()
      let ageRange = {title: ageRangeLabel, id: ageRangeLabel}
      patientAgeRangeObjects.pushObject(ageRange);
    }

    return patientAgeRangeObjects
  }),

  actions: {

    filter() {
      this.toggleProperty('filterDidChange');
    }
  }
});

function iso8601Date(date) {
  if (date) {
    let momentDate = moment(date)

    if (momentDate.isValid()) {
      return momentDate.format('YYYY-MM-DD')
    }
  }

  return undefined
}
