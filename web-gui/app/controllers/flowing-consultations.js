/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { reads, sort, filterBy } from '@ember/object/computed';
import Changeset from 'ember-changeset';
import { isPresent } from '@ember/utils';
import moment from 'moment';
import { inject as service } from '@ember/service';

const dateTimeFormat = "DD/MM/YYYY HH:mm"

export default Controller.extend({
  currentUser: service(),

  interventionDomain: reads('model.interventionDomain'),
  flowingConsultations: reads('model.flowingConsultations'),
  callingInstitutionTitleWithAreaSorting: Object.freeze(['callingInstitutionTitleWithArea']),
  flowingConsultationsByCallingInstitutionTitleWithArea: sort('flowingConsultations', 'callingInstitutionTitleWithAreaSorting'),

  consultationStates: computed('flowingConsultationsByCallingInstitutionTitleWithArea.@each.{validatedAt,cancelledAt,assignedAt}', function () {
    let toTransmit = {
      title: 'À transmettre',
      flowingConsultations: [],
      expanded: true
    };
    let inProgress = {
      title: 'En cours',
      flowingConsultations: [],
      expanded: true
    };
    let validated = {
      title: 'Validées',
      flowingConsultations: [],
      expanded: false
    };
    let cancelled = {
      title: 'Annulées',
      flowingConsultations: [],
      expanded: false
    };

    this.flowingConsultationsByCallingInstitutionTitleWithArea.forEach(function (flowingConsultation) {
      if (flowingConsultation.get('validatedAt')) {
        validated.flowingConsultations.pushObject(flowingConsultation);
      }
      else if (flowingConsultation.get('cancelledAt')) {
        cancelled.flowingConsultations.pushObject(flowingConsultation);
      }
      else if (flowingConsultation.get('assignedAt')) {
        inProgress.flowingConsultations.pushObject(flowingConsultation);
      }
      else {
        toTransmit.flowingConsultations.pushObject(flowingConsultation);
      }
    })

    return [
      toTransmit,
      inProgress,
      validated,
      cancelled,
    ]
  }),

  // Validation

  validatedAt: computed('validationDateChangeset.validatedAt', {
    get(/* key */) {
      let time = this.validationDateChangeset.validatedAt;

      if (isPresent(time)) {
        return moment(time).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.validationDateChangeset.set('validatedAt', newMoment.toDate());
        }
      }
      else {
        this.validationDateChangeset.set('validatedAt', null);
      }
      return value;
    }
  }),

  // Cancellation

  cancelledAt: computed('cancellationDateChangeset.cancelledAt', {
    get(/* key */) {
      let time = this.cancellationDateChangeset.cancelledAt;

      if (isPresent(time)) {
        return moment(time).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.cancellationDateChangeset.set('cancelledAt', newMoment.toDate());
        }
      }
      else {
        this.cancellationDateChangeset.set('cancelledAt', null);
      }
      return value;
    }
  }),

  // Assignment

  users: computed(function () {
    return this.store.findAll('user');
  }),

  physicians: filterBy('users', 'isPhysician'),

  assigned: computed('assignmentChangeset.assignee', function () {
    return this.get('assignmentChangeset.assignee.id');
  }),

  assignedAt: computed('assignmentChangeset.assignedAt', {
    get(/* key */) {
      let time = this.assignmentChangeset.assignedAt;

      if (isPresent(time)) {
        return moment(time).format(dateTimeFormat);
      }

      return undefined;
    },

    set(key, value) {
      if (isPresent(value)) {
        let newMoment =  moment(value, dateTimeFormat, true);
        if (newMoment.isValid()) {
          this.assignmentChangeset.set('assignedAt', newMoment.toDate());
        }
      }
      else {
        this.assignmentChangeset.set('assignedAt', null);
      }
      return value;
    }
  }),

  // Certificates

  certificateTemplates: computed(function() {
    return this.store.findAll('certificate-template');
  }),
  enabledCertificateTemplates: filterBy('certificateTemplates', 'isEnabled'),
  availableCertificateTemplates: computed('enabledCertificateTemplates.@each.hasRole', 'currentUser.role', function () {
    return this.enabledCertificateTemplates.filter((template) => {
      return template.get('hasRole')(this.get('currentUser.role'));
    });
  }),

  // Validations

  flowingConsultationValidator(/* { key, newValue, oldValue, changes, content } */) {
    return true
  },

  actions: {

    // Validation

    startValidationDateUpdate(flowingConsultation) {
      let validator = this.flowingConsultationValidator;
      let changeset = new Changeset(flowingConsultation, validator)

      if (!isPresent(flowingConsultation.validatedAt)) {
        changeset.set('validatedAt', moment().toDate());
      }

      this.set('validationDateChangeset', changeset);
    },

    stopValidationDateUpdate(changeset) {
      changeset.rollbackAttributes();
      this.set('validationDateChangeset', null);
    },

    saveValidationDateUpdate(changeset) {
      this.set('validationDateChangeset', null);
      return changeset.save().then(() => {
        _resetLastUpdateCheck(this);
        this.flashMessages.success('Consultation validée.')
      }).catch(() => {
        this.set('validationDateChangeset', changeset);
        this.flashMessages.danger('Impossible de sauvegarder vos changements.')
      });
    },

    // Cancellation

    startCancellationDateUpdate(flowingConsultation, cause) {
      let validator = this.flowingConsultationValidator;

      let changeset = new Changeset(flowingConsultation, validator);
      changeset.set('cancelledBy', cause);
      if (!isPresent(flowingConsultation.cancelledAt)) {
        changeset.set('cancelledAt', moment().toDate());
      }

      this.set('cancellationDateChangeset', changeset);
    },

    stopCancellationDateUpdate(changeset) {
      changeset.rollback();
      this.set('cancellationDateChangeset', null);
    },

    saveCancellationDateUpdate(changeset) {
      this.set('cancellationDateChangeset', null);
      return changeset.save().then(() => {
        _resetLastUpdateCheck(this);
        this.flashMessages.success('Consultation annulée.')
      }).catch(() => {
        this.set('cancellationDateChangeset', changeset);
        this.flashMessages.danger('Impossible de sauvegarder vos changements.')
      });
    },

    // Assignment

    startAssignmentUpdate(flowingConsultation) {
      let validator = this.flowingConsultationValidator;
      let changeset = new Changeset(flowingConsultation, validator)

      if (!isPresent(flowingConsultation.assignedAt)) {
        changeset.set('assignedAt', moment().toDate());
      }

      this.set('assignmentChangeset', changeset);
    },

    stopAssignmentUpdate(changeset) {
      changeset.rollbackAttributes();
      this.set('assignmentChangeset', null);
    },

    saveAssignmentUpdate(changeset) {
      this.set('assignmentChangeset', null);
      return changeset.save().then(() => {
        _resetLastUpdateCheck(this);
        this.flashMessages.success('Consultation assignée.')
      }).catch(() => {
        this.set('assignmentChangeset', changeset);
        this.flashMessages.danger('Impossible de sauvegarder vos changements.')
      });
    },

    addAssignee(user) {
      this.set('assignee', user);
    },

    removeAssignee(user) {
      if (this.get('assignee.id') === user.get('id')) {
        this.set('assignee', null);
        this.set('assignmentChangeset.assignedAt', null);
      }
    },

    // Certificate filling

    displayCertificatesManager(flowingConsultation) {
      this.set('concernedFlowingConsultationForCertificates', flowingConsultation)
    },

    // Common

    setDateAtNow(key) {
      this.set(key, moment().format(dateTimeFormat));
    }
  }

});

function _resetLastUpdateCheck(controller) {
  controller.set('lastUpdateCheck', moment().toDate())
}
