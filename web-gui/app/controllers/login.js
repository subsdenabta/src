/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  store: service(),
  session: service(),
  flashMessages: service(),

  errorMessages: computed('identification', 'password', 'forgottenPassword', {
    get() {
      return [];
    },

    set(key, value) {
      return value;
    }
  }),

  actions: {
    authenticate() {
      let { identification, password, forgottenPassword } = this;

      if (forgottenPassword) {
        this.store.createRecord('password-request', {identification}).save().then(() => {
          this.flashMessages.success('Un nouveau mot de passe vient de vous être envoyé par courriel.')
          this.set('forgottenPassword', false);
        }).catch(() => {
          this.set('errorMessages', ['Impossible de réinitialiser le mot de passe… Si le problème persiste, veuillez contacter qui de droit.']);
        })
      }
      else {
        this.session.authenticate('authenticator:realm', identification, password).catch((err) => {
          let error = err.error;
          let errorMessage;

          if (error == 'inactive_user') {
            errorMessage = "Désolé, votre compte n'est plus actif, merci de vous rapprocher d'un administrateur."
          } else {
            errorMessage = 'Connexion impossible, veuillez vérifier vos identifiant et mot de passe.'
          }

          this.set('errorMessages', [errorMessage]);
        });
      }
    }
  }
});
