/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Controller from '@ember/controller';
import { computed, action } from '@ember/object';
import { reads } from '@ember/object/computed';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { dateFormat } from '../../constants';
import moment from 'moment';

export default class PatientsIndexController extends Controller {
  @service router;

  dateFormat = dateFormat;
  queryParams = ['page', 'size', 'lastNameFilter', 'firstNameFilter', 'birthdateFilter', 'birthYearFilter'];
  page = 1;
  @tracked size = 25;

  @reads('model.patients')
  patients = [];

  @reads('model.meta')
  meta = {};

  @reads('model.meta.totalPages')
  totalPages = 1;

  @computed('totalPages')
  get allPages() {
    return [...Array(this.totalPages).keys()].map((e,i) => i+1)
  }

  @computed('patients')
  get numberOfRows() {
    return this.patients.length
  }

  @action
  filtersDidChange(lastName, firstName, birthdate, birthYear) {
    return this.router.transitionTo({
      queryParams: {
        lastNameFilter: lastName,
        firstNameFilter: firstName,
        birthdateFilter: birthdate && birthdate.format('YYYY-MM-DD'),
        birthYearFilter: birthYear,
        page: 1
      }
    });
  }
}
