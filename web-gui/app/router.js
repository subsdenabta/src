/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('login');
  // Users
  this.route('users', function () {
    this.route('new');
    this.route('edit', { path: ':user_id/edit' });
    this.route('index', { path: '/' }, function () {
      this.route('disable', { path: ':user_id/disable' });
    });
  });
  // Patients
  this.route('patients', function () {
    this.route('index', { path: '/' }, function () {
      this.route('duplicating', { path: ':patient_id/duplicating' });
      this.route('edit', { path: ':patient_id/edit' });
      this.route('history', { path: ':patient_id/history' });
    });
  });
  this.route('profile');
  // Interventions
  this.route('interventions');
  this.route('intervention-domains/new');
  this.route('intervention-domains/edit', { path: 'intervention-domains/:intervention_domain_id/edit' });
  this.route('consultation-types/new', { path: 'intervention-domains/:intervention_domain_id/consultation-types/new' });
  this.route('consultation-types/edit', { path: 'consultation-types/:consultation_type_id/edit' });
  this.route('consultation-subtypes/new', { path: 'consultation-types/:consultation_type_id/consultation-subtypes/new' });
  this.route('consultation-subtypes/edit', { path: 'consultation-subtypes/:consultation_subtype_id/edit' });
  this.route('consultation-reasons/new', { path: 'consultation-types/:consultation_type_id/consultation-reasons/new' });
  this.route('consultation-reasons/edit', { path: 'consultation-reasons/:consultation_reason_id/edit' });
  this.route('schedules/new', { path: 'intervention-domains/:intervention_domain_id/schedules/new' });
  this.route('schedules/edit', { path: 'schedules/:schedule_id/edit' });
  // Calling areas and institutions
  this.route('calling-areas', function () {
    this.route('new');
    this.route('edit', { path: ':calling_area_id/edit' });
  });
  this.route('calling-institutions/new', { path: 'calling-areas/:calling_area_id/calling-institutions/new' });
  this.route('calling-institutions', function () {
    this.route('edit', { path: ':calling_institution_id/edit' });
  });
  // Consultations
  this.route('intervention-domain.appointments', { path: 'intervention-domains/:intervention_domain_id/appointments' }, function () {
    this.route('show', { path: ':appointment_id' });
  });
  this.route('consultation-type.appointments', { path: 'consultation-types/:consultation_type_id/appointments' }, function () {
    this.route('new');
    this.route('show', { path: ':appointment_id' });
    this.route('edit', { path: ':appointment_id/edit' });
  });
  this.route('appointments/conclusion', { path: 'appointments/:appointment_id/conclusion' });
  this.route('consultation-type/archives', { path: 'consultation-types/:consultation_type_id/archives' });
  this.route('flowing-consultations', { path: 'intervention-domains/:intervention_domain_id/flowing-consultations' });
  this.route('flowing-consultations/new', { path: 'intervention-domains/:intervention_domain_id/flowing-consultations/new' });
  this.route('flowing-consultations/edit', { path: 'flowing-consultations/:flowing_consultation_id/edit' });
  this.route('flowing-consultations/archives', { path: 'intervention-domains/:intervention_domain_id/flowing-consultations/archives' });
  // Certificates
  this.route('certificate-templates', function () {
    this.route('new');
    this.route('edit', { path: ':certificate_template_id/edit' });
  });
  this.route('settings/edit', { path: 'settings/:setting_id/edit' });
  this.route('certificates.edit', { path: 'certificates/:appointment_id/edit' });
});
