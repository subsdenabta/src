/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import JSONAPISerializer from '@ember-data/serializer/json-api';
import { underscore, dasherize, camelize } from '@ember/string';

export default JSONAPISerializer.extend({
  modelNameFromPayloadType(payloadType) {
    return dasherize(payloadType);
  },

  payloadTypeFromModelName(modelName) {
    return underscore(modelName);
  },

  modelNameFromPayloadKey(payloadType) { // Soon deprecated
    return this.modelNameFromPayloadType(this._super(payloadType));
  },

  payloadKeyFromModelName(modelName) { // Soon deprecated
    return this.payloadTypeFromModelName(this._super(modelName));
  },

  keyForAttribute(attr) {
    return underscore(attr);
  },

  keyForRelationship(key /*, relationship, method*/) {
    return underscore(key);
  },

  normalizeQueryResponse(store, clazz, payload) {
    const result = this._super(...arguments);

    if (payload.meta) {
      result.meta = this._normalizeMeta(payload.meta)
    }

    return result;
  },

  _normalizeMeta(data) {
    let meta = {};

    Object.keys(data).forEach(key => {
      let new_key = camelize(key)
      meta[new_key] = data[key]
    })

    return meta
  }
});
