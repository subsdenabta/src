/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Transform from '@ember-data/serializer/transform';
import { isPresent } from '@ember/utils'
import moment from 'moment';

const momentFormat = 'YYYY-MM-DDTHH:mmZ'

export default Transform.extend({
  deserialize(serialized) {
    if (isPresent(serialized)) {
      return moment(serialized, momentFormat).toDate();
    }
    return serialized;
  },

  serialize(deserialized) {
    if (isPresent(deserialized)) {
      return moment.utc(deserialized).format(momentFormat.replace('Z', '[Z]'));
    }
    return deserialized;
  }
});
