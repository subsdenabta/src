/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  can: service(),

  beforeModel() {
    let result = this._super(...arguments);

    if (this.can.cannot('configure intervention-domains')) {
      this.flashMessages.danger("Vous n'êtes pas habilité·e·s à accéder à ces informations.")
      result = this.transitionTo('index');
    }

    return result;
  },

  model(params) {
    return this.store.findRecord('intervention-domain', params.intervention_domain_id).then((interventionDomain) => {
      interventionDomain.startTrack(); // About to change a relation (roles), enabling tracking…
      if (interventionDomain.get('roles.length') === 0) {
        return this.store.findRecord('role', 'administrator').then((administrator) => {
          interventionDomain.get('roles').unshiftObject(administrator);
          return interventionDomain;
        });
      }
      else {
        return interventionDomain;
      }
    });
  },

  actions : {
    willTransition() {
      this.controller.get('model').rollback();
    }
  }

});
