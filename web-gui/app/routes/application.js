/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import { inject as service } from '@ember/service';

export default Route.extend(ApplicationRouteMixin, {
  intl: service(),
  currentUser: service(),

  beforeModel() {
    this._super(...arguments);

    this.intl.setLocale(['fr-fr']);
    return this._loadCurrentUser()
  },

  sessionAuthenticated() {
    this._super(...arguments);

    return this._loadCurrentUser();
  },

  // https://github.com/simplabs/ember-simple-auth/blob/4bf3aa52a3edb9611a46b1dfdabc7d64980f14b0/guides/managing-current-user.md
  _loadCurrentUser() {
    return this.currentUser.load().catch(() => this.session.invalidate());
  }

});
