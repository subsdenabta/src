/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { bind } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default Route.extend(AuthenticatedRouteMixin, {
  can: service(),

  model() {
    let consultationType = this.modelFor('consultation-type.appointments');

    return this.store.createRecord('appointment', {consultationType});
  },

  afterModel(model) {
    let result = this._super(...arguments);

    if (this.can.cannot('create appointment', model)) {
      this.flashMessages.danger("Vous n'êtes pas habilité·e·s à accéder à cette information.")
      return this.transitionTo('index');
    }

    return result;
  },

  setupController(controller, post) {
    this._super(controller, post);
    let parentController = this.controllerFor('consultation-type.appointments');
    controller.set('onSave', bind(parentController, parentController._updateDataFetch));
  },

  resetController(controller) {
    controller.resetQueryParams();
  }

});
