/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';

const refreshInterval = 20 * 1000; // 20s

export default Route.extend(AuthenticatedRouteMixin, {
  flashMessages: service(),
  can: service(),

  model(params) {
    return this.store.findRecord('consultation-type', params.consultation_type_id);
  },

  afterModel(consultationType) {
    if (this.can.cannot('consult appointments of consultation-type', consultationType)) {
      this.flashMessages.danger("Vous n'êtes pas habilité·e·s à accéder à cette information.")
      return this.transitionTo('index');
    }
  },

  setupController(controller, model) {
    this._super(...arguments);
    this.pollServerForChanges.perform(model);
  },

  pollServerForChanges: task(function * (consultationType) {
    let message = 'Une ou plusieurs consultations ont été créées. Cliquer ici pour recharger la page.';

    while(true) {
      yield timeout(refreshInterval);
      let allDates = this.controller.get('allDates');

      if (allDates) {
        let starts_at = allDates.get('firstObject').startOf('day')
        let ends_at = allDates.get('lastObject').endOf('day')
        let lastDataFetch = this.controller.get('lastDataFetch')

        this.store.query('appointment', {
          filter: {
            'consultation_type_id': consultationType.id,
            'updated_after': lastDataFetch.toISOString(),
            'start_date': starts_at.toISOString(),
            'end_date': ends_at.toISOString(),
          }
        }).then( (appointments) => {
          this.controller._updateDataFetch();

          let currentRoute = this;
          let shouldDisplayMessage = false;

          appointments.forEach((appointment) => {
            let createdAt = appointment.get('createdAt');

            if (createdAt > lastDataFetch) {
              shouldDisplayMessage = true
            }
          })

          if(shouldDisplayMessage) {
            this.flashMessages.warning(message, {
              sticky: true,
              onDestroy: function () { currentRoute.controller.toggleProperty('forceRefetchToggle') }
            })
          }
        }).catch( (err) => {
          if (err.errors && err.errors[0] && err.errors[0].status === '404') {
            return // No appointment at all in the date range
          }
          this.flashMessages.danger(err.message);
        })
      }
    }
  }).cancelOn('deactivate').restartable()

});
