/*
 * Filaé is a tool supporting French Forensic Medical Units.
 * Copyright (C) 2018-2022 infoPiiaf SARL
 *
 * This file is part of Filaé which is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, version 3.
 *
 * Filaé is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 */
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class PatientsIndexRoute extends Route {
  @service session;
  @service store;
  @service can;

  queryParams = {
    page: {
      refreshModel: true
    },
    size: {
      refreshModel: true
    },
    lastNameFilter: {
      refreshModel: true
    },
    firstNameFilter: {
      refreshModel: true
    },
    birthdateFilter: {
      refreshModel: true
    },
    birthYearFilter: {
      refreshModel: true
    }
  }

  beforeModel(transition) {
    this.session.requireAuthentication(transition, 'login');

    if (this.can.cannot('list patients')) {
      this.flashMessages.danger("Vous n'êtes pas habilité·e·s à accéder à ces informations.")
      this.transitionTo('index');
    }
  }

  model(params) {
    return this.store.query('patient', {
      page: {
            number: params.page,
            size: params.size
      },
      filter : {
        last_name: params.lastNameFilter,
        first_name: params.firstNameFilter,
        birthdate: params.birthdateFilter,
        birth_year: params.birthYearFilter
      }
    }).then(results => {
      return {
        patients: results.sortBy('fullName'),
        meta: results.meta
      }
    }).catch((e) => {
      this.flashMessages.danger("Impossible de récupérer la liste des patients. Si le problème persiste, communiquez ce qui suit à un administrateur : " + e.message, {sticky: true})
    });
  }
}
