#!/bin/bash
# Filaé is a tool supporting French Forensic Medical Units.
# Copyright (C) 2018-2022 infoPiiaf SARL
#
# This file is part of Filaé which is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, version 3.
#
# Filaé is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>
#

cd web-api

if [ $LEADER ] && [ ! $DEPLOY ]
then
  # Install dependencies
  bundle check || bundle install

  # Cleanup
  rm -f tmp/pids/server.pid
  # Prepare DB
  bundle exec rails db:setup
else
  # Wait for it…
  while true
  do
    curl --output /dev/null --silent --head --fail "web-api:3000"
    if [[ $? -eq 22 ]]; then break; fi
    sleep 1
  done
fi

# Start
exec "$@"
