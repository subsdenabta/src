*titre à* :scissors: : `Échec pipeline audit n°<num>`

## Problème rencontré

Le pipeline [<num>](https://framagit.org/filae/src/-/pipelines/<num>) a échoué :
```
Coller ici le résultat du pipeline
```

## Comment reproduire ?

1. Considérant le code de Filaé
1. Lorsque je lance `make audit-api`
1. Alors j'ai le message d'erreur ci-dessus

## Comportement attendu

1. Considérant le code de Filaé
1. Lorsque je lance `make audit-api`
1. Alors je n'ai pas de message d'erreur

/label ~BUG
/label ~"Dépendance"
/label ~"Sécu"
