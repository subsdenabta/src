# Développement et tests unitaires

Pour le développement et les tests unitaires nous sommes partis sur la solution [Docker](https://www.docker.com/) via [docker-compose](https://docs.docker.com/compose/). Il est donc nécessaire d'installer ces deux outils au préalable.

Pour lancer la solution, pour une démonstration par exemple :
1. récupérer le code
1. récupérer les images nécessaires et générer les images applicatives : `make build`
1. lancer la solution : `make up`
1. accéder à l'application : http://localhost:4200/
1. se connecter à un compte selon le rôle désiré :
   * les rôles disponibles sont listés dans ce [document JSON](https://framagit.org/filae/src/-/blob/main/web-api/db/seeds/roles_by_title.json) ;
   * trois comptes pour chaque rôle sont créés au démarrage de *web-api*, avec des identifiants de la forme `title`, `title2`, `title3` et comme mot de passe le `title` ;
   * ainsi, vous pouvez par exemple vous connecter avec le couple identifiant/mot de passe `administrator`/`administrator` ou encore `nurse2`/`nurse`.

Pour lancer les tests isolément :
1. lancer la solution : `make up`
1. lancer les tests :
    - web-api : `make test-api`
    - web-gui (sans IHM): `make test-gui`
    - web-gui (avec IHM):
        1. `make test-gui-s`
        1. se connecter à http://localhost:7357
    - web-uat (Gwen) : `make test-uat`

Pour lancer tous les tests les uns à la suite des autres : `make [test]`

Quelques outils complémentaires :
- [bundler-audit](https://github.com/rubysec/bundler-audit) (utilisé pour lister les failles de sécurité connues de certaines *gems*) : `docker-compose exec web-api bash -c 'cd web-api && bundle-audit check [--update]'` (`--update` force la mise à jour de la base de données des failles connues)
