# Cycle de vie des certificats

```mermaid
sequenceDiagram
participant Ordinateur
participant ModèleDeCertificat
participant PrisEnCharge
participant CertificatAmorcé
participant CertificatSigné
participant CertificatScellé
participant CertificatEnvoyé
participant OPJ

alt Simple suivi de prise en charge
    Ordinateur->>PrisEnCharge: [Praticien] Clic
    Ordinateur->>Ordinateur: [Praticien] Rédaction
    note over Ordinateur: Document Texte
    Ordinateur->>Ordinateur: [Praticien] Impression et signature
    note over Ordinateur: Document Papier
    Ordinateur->>Ordinateur: [Praticien] Numérisation
    note over Ordinateur : Document PDF
    Ordinateur->>OPJ: [Secrétaire] Envoi par courriel
    note right of OPJ: Document PDF
    PrisEnCharge->>CertificatEnvoyé: [Secrétaire] Clic
else Référentiel de modèles de certificats
    Ordinateur->>ModèleDeCertificat: [Administrateur] Téléversement VV.odt
    note right of ModèleDeCertificat: Document Texte
    Ordinateur->>PrisEnCharge: [Praticien] Clic VV.odt
    PrisEnCharge->>Ordinateur: [Praticien] Téléchargement d'une copie vierge
    note left of Ordinateur: Document Texte
    Ordinateur->>Ordinateur: [Praticien] Rédaction
    note over Ordinateur: Document Texte
    Ordinateur->>Ordinateur: [Praticien] Impression et signature
    note over Ordinateur: Document Papier
    Ordinateur->>Ordinateur: [Praticien] Numérisation
    note over Ordinateur : Document PDF
    Ordinateur->>CertificatSigné: [Secrétaire] Téléversement
    note right of CertificatSigné: Document PDF
    CertificatSigné->>CertificatSigné: [Secrétaire] Clic pour aperçu
    note right of CertificatSigné: Document PDF
    CertificatSigné->>CertificatScellé: [Secrétaire] Clic
    note right of CertificatScellé: Document PDF + Sceau
    CertificatScellé->>OPJ: [Secrétaire] Clic
    note right of OPJ: Document PDF + Sceau
    CertificatScellé->>CertificatEnvoyé: [Automatique] Horodatage
    note right of CertificatEnvoyé: Document PDF + Sceau
else Questionnaires intégrés (fonctionnalité expérimentale cible)
    Ordinateur->>ModèleDeCertificat: [Administrateur] Conception
    note right of ModèleDeCertificat: Document JSON
    Ordinateur->>PrisEnCharge: [Praticien] Clic
    note right of PrisEnCharge: Formulaire Web
    PrisEnCharge->>CertificatAmorcé: [Praticien] Rédaction
    note right of CertificatAmorcé: Formulaire Web
    CertificatAmorcé->>CertificatSigné: [Praticien] Clic pour aperçu
    note right of CertificatSigné: Document PDF
    CertificatSigné->>CertificatScellé: [Praticien] Clic
    note right of CertificatScellé: Document PDF + Sceau
    CertificatScellé->>OPJ: [Praticien] Clic
    note right of OPJ: Document PDF + Sceau
    CertificatScellé->>CertificatEnvoyé: [Automatique] Horodatage
    note right of CertificatEnvoyé: Document PDF + Sceau
end
```
