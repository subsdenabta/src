FILAÉ permet l'édition PDF de certificats à partir de modèles de questionnaire et de formulaires de rédaction standardisés. Ces modèles reposent sur une description `JSON` interprété par une [bibliothèque externe](https://www.surveyjs.io/). Cette bibliothèque fournit un [outil de conception de questionnaire](https://www.surveyjs.io/Survey/Builder/) afin de faciliter la rédaction de ces descriptions `JSON`.

Cette documentation a pour objet de synthétiser les pratiques actuelles de conception des modèles de certificat et de leurs impacts lors de l'édition PDF.

## Structure

### Pages

* L'utilisation de différentes pages permet un remplissage du formulaire plus agréable, ainsi qu'un gain de performance ressentie par l'utilisateur ;
* Chaque titre de page est un titre de niveau 2, au style caractéristique et créant une entrée dans la table des matières à l'édition PDF ;
* N.B. : une page du questionnaire ne correspond pas à une page du PDF édité.

### Panel

* L'utilisation des panels permet de regrouper les questions par thématique ;
* Chaque titre de panel est un titre de niveau 3, au style caractéristique et créant une entrée dans la table des matières à l'édition PDF ;
* Le titre d'un panel est facultatif, si le seul intéret est de regrouper les questions sans nécessairement avoir un titre explicite.

![Arborescence des pages et panels](./panels.png)

À noter que si une page ou un panel ne contient aucune réponse à l'édition PDF, le titre est alors masqué pour éviter les sections vides.

## Types de question

Un [questionnaire d'exemples](./design.surveyjs.json) vous permet de découvrir les différents types de question et leurs paramètres. Nous reprenons ici quelques captures d'écran de ce questionnaire pour faciliter la lecture simple et ajouter d'éventuelles précisions concernant l'édition des PDF.

![Questions de type booléen](./booleens.png)

![Questions à choix multiple](./choix-multiple.png)

![Questions à texte libre](./texte-libre.png)

![Téléversement de fichiers](./televersement.png)

![Questions de type expression](./expressions.png)

### Expressions pour données pré-remplies

Les questions de type `expression` peuvent également servir à utiliser des données pré-remplies venant de la prise de rendez-vous. Ces données sont aujourd'hui au nombre de 3 :

1. `filae-patient-nom-complet` ;
1. `filae-patient-date-naissance`;
1. `filae-consultation-date`.

La question ci-dessous reprend tout simplement le nom complet de la victime dans le formulaire, pour éviter la double-saisie.

```json
{
  "type": "expression",
  "name": "conclusion-victime-nom",
  "expression": "{filae-patient-nom-complet}",
}
```

## Astuce de saisie et d'édition PDF

Le conception des certificats serait d'une simplicité enfantine s'il suffisait de liste des intitulés de questions et leurs réponses associées. Cependant, les exigences d'un certificat médico-judiciaire sont bien plus subtiles que cela. Il faut donc user de plus de subtilités (pour ne pas pas dire supercheries) également dans la conception des modèles.

![Aides au langage naturel](./langage-naturel.png)

![Affichage conditionnel](./affichage-conditionnel.png)

`expression + defaultValue + visibleIf` est un de ces usages subtiles. La difficulté ici est l'affichage conditionnel à l'édition PDF **des suffixes** : texte simple associé à une réponse précédente.

Considérant que :
* nous n'avons pas l'intention de re-développer l'évaluation des expressions `visibleIf` côté serveur ;
* pour tous les types de question, nous avons décidé d'utiliser l'absence de réponse comme l'indication de "non-retranscription en PDF" ;
* toutes les questions non-visibles n'ont pas de réponses.

Alors, pour ne pas retranscrire un suffixe si la question associée n'a pas de réponse, il faut un type de question qui n'a pas de valeur s'il n'est pas visible, et qui a une valeur "automagiquement" si elle est visible :

1. le type `HTML` qui vient naturellement en tête quand il s'agit de présenter du simple texte ne fonctionne pas. N'ayant jamais de valeur de réponse, son contenu est (malheureusement, dans ces circonstances) retranscrit systématiquement ;
1. l'autre type en lecture seule, `expression` fonctionne comme attendu. S'il n'est pas visible, aucune valeur de réponse, aucune retranscription. S'il est visible, la valeur par défaut devient sa valeur de réponse, et donc retranscrite dans le PDF.
    * Utiliser la valeur par défaut plutôt que la valeur de l'expression permet l'affichage du contenu dans le _Builder_, ce qui facilite la lecture du modèle pendant sa conception ;
    * `enableIf` ne fonctionne pas, à la place de `visibleIf`. Il aurait été intéressant de pouvoir afficher ce suffixe en permanence, et n'envoyer le réponse que si c'est `enabled`, comme le font les formulaires HTML. Mais ce n'est pas le fonctionnement de SurveyJS, malheureusement, qui envoie la valeur de réponse systématiquement. Ainsi, dans le cas d'une unité comme ci-dessus, il nous faut utiliser le champ `description` pour offrir à l'intervenant une aide à la saisie, avant que le suffixe n'apparaisse…

![Aides à la saisie](./aide-a-la-saisie.png)
