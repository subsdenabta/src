# Mon profil

Une fois connecté·e, vous pouvez accéder à votre profil pour y configurer différents éléments.

![Menu "Mon profil"](menu-mon-profil.png)

## Mon mot de passe

Vous pouvez modifier votre mot de passe à tout moment, aussi souvent que vous le désirez. Quelques contraintes sont appliquées sur le format du mot de passe mais comme toujours, nous ne pouvons que chaudement vous conseiller de suivre les [recommandations éditées par l'ANSSI](https://www.ssi.gouv.fr/guide/mot-de-passe/).

![Formulaire de modification de mot de passe](formulaire-mot-de-passe.png)

## Ma signature numérisée

Brièvement envisagée comme une alternative temporaire à une signature électronique en bonne et due forme, votre signature numérisée (_"scannée"_) peut être téléversée dans Filaé pour l'emporter toujours avec vous. Vous pouvez ainsi aisément la copier/coller dans vos documents LibreOffice ou autre.

Attention, la signature numérisée n'a pas de valeur probante reconnue par la loi, comme pourrait l'avoir une signature électronique via votre CPS par exemple. Mais si cette solution peut vous dépanner…
