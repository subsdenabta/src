# Administration des comptes-utilisateurs

Chaque compte-utilisateur est authentifié par un couple identifiant/mot de passe et associé à un rôle qui lui donne accès à plus ou moins de fonctionnalités. Il existe dix rôles dans Filaé, dont le détail des autorisations est précisé dans la [documentation sur la sécurité](doc/03 Développement/01 Sécurité).

## Création d'un compte-utilisateur

Tous les administrateurs peuvent créer un nouveau compte-utilisateur. Après avoir renseigné les informations nécessaires, un courriel est envoyé avec le mot de passe généré par l'application. Le destinataire peut alors se connecter et est invité à choisir un mot de passe de son choix.

![Menu "Utilisateurs"](menu-utilisateurs.png)
![Formulaire de création d'un utilisateur](formulaire-nouvel-utilisateur.png)

## Désactivation d'un compte-utilisateur

Il n'est pas possible de supprimer un compte-utilisateur afin de conserver l'historique de ses actions dans l'application. Il est cependant possible de désactiver son accès. Pour ce faire, un bouton 🚫 est présent en face de chaque compte-utilisateur dans la liste.

Cette désactivation peut même être planifiée à l'avance si la date de fin d'activité est déjà connue, dans le cadre d'un CDD par exemple.

La liste des utilisateurs reflète l'état de chaque compte-utilisateur :

1. Les comptes actifs sont présentés en premier, avec des couleurs contrastées ;
1. Les comptes désactivés sont présentés en premier, avec des couleurs très atténuées ;
1. Les comptes planifiés pour désactivation sont entre les deux.
