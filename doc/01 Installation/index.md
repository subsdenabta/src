# Déploiement sur serveur

## Pré-requis

La solution Filaé est prévue pour s'exécuter dans un environnement Unix (testée sur Debian dans sa version `stable`, Ubuntu devrait fonctionner aussi sans problème).

Il vous faudra également un serveur Web pour exposer deux sous-domaines, un pour l'IHM, l'autre pour l'API (testé sur `nginx`, `Apache` devrait fonctionner aussi sans problème). Cela va sans dire, mais toutes ces communications Web doivent être chiffrées via TLS, en utilisant des certificats [`Let's Encrypt`](https://letsencrypt.org/) par exemple.

Filaé stocke ses données dans une base de données relationnelle : [`PostgreSQL`](https://www.postgresql.org/). Filaé a débuté avec la version `9.6`, pour s'exécuter actuellement avec la `13`.

## IHM JavaScript

L'IHM est développée avec [`Ember.js`](https://emberjs.com/), un cadriciel qui produit une application JavaScript. Il ne reste plus qu'à déposer les fichiers générés dans le dossier exposé par votre serveur Web sur le sous-domaine pour l'IHM.

Toutes les informations pour construire une version de l'IHM sont présentes dans le [README de web-gui](web-gui).

## API Ruby

L'API est développée avec [`Ruby on Rails`](https://rubyonrails.org/), un cadriciel qui produit une application Rack. Il faut ensuite exécuter cette application et l'exposer via votre serveur Web sur le sous-domaine pour l'API.

Il existe de nombreuses façons d'y arriver, celle ayant déjà fait ses preuves avec Filaé étant le couple [`nginx+Passenger`](https://www.phusionpassenger.com/docs/tutorials/deploy_to_production/installations/oss/ownserver/ruby/standalone/).

## Autres dépendances lâches

Filaé dépend aussi d'autres logiciels pour certaines fonctionnalités. Il s'agit notamment de `LibreOffice` et `wkhtmltopdf` pour la conversion des certificats en PDF.

## En résumé

```mermaid
flowchart LR
  subgraph Serveur
    subgraph Serveur Web
        IHM
        API
    end
    PostgreSQL[(PostgreSQL)]
    wkhtmltopdf
    LibreOffice
  end
  Navigateur -.-> IHM
  Navigateur -.-> API
  API -.-> PostgreSQL
  API --> wkhtmltopdf
  API --> LibreOffice
```

Les lignes pointillées représentent les communications via le réseau, les lignes pleines les exécutions locales.
