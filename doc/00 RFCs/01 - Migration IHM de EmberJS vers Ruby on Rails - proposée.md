# Évolution discutée

Statut: Brouillon :pencil:

## Résumé

Dès l'origine, la solution Filaé est composée de deux entités : une IHM développée avec le cadriciel EmberJS et une API développée avec le cadriciel Ruby on Rails. L'idée est de n'avoir plus qu'un seul cadriciel pour simplifier et accélérer le maintien en condition opérationnelle ainsi que les évolutions à venir.

## Motivation

Dès le départ fut prise en compte la possibilité d'utiliser Filaé en mode non-connecté, c'est à dire sans avoir avec à aucun réseau. Ce fut la raison principale pour laquelle nous sommes partis sur une architecture à deux entités : une IHM en HTML/CSS/JS et une API en Ruby on Rails, toutes deux communiquant entre elles via [JSON:API](https://jsonapi.org/).

Après une première tentative de développement de l'IHM via [Elm](https://elm-lang.org/), intéressant mais trop peu mature à l'époque à notre goût, nous avons basculé vers [EmberJS](https://emberjs.com/) qui nous a donné rapidement satisfaction. Le fait que nous croyions encore à l'époque que Filaé pourrait prendre son envol en temps que logiciel libre dans l'univers des Unités Médico-Judiciaires nous rendait confiant quant à la possibilité de maintenir dans de bonnes conditions ces deux entités (ainsi que la communication entre elles). Malheureusement cela n'a pas eu lieu. Nous avons donc dû nous réorganiser en fonction de nos forces, avons privilégié le suivi de très près de la partie la plus critique, l'API, tout en effectuant le suivi d'un point de vue sécurité de base de l'IHM (moins critique). Nous n'avons pas pris la peine de suivre les montées de versions de EmberJS, qui se sont accompagnées de très grosses évolutions, notamment au passage de la version 4. Depuis début janvier 2023, EmberJS 3 n'est plus maintenu... Nous avons donc lancé des travaux pour faire ces montées de version de EmberJS, en dehors de tout contrat et de financements, mais le chemin s'annonce très long. En résumé, il existe une très grosse dette technique en ce qui concerne l'IHM.

De plus, la problématique du mode non-connecté n'a en définitive jamais été remontée et au vu du tournant que prennent non seulement Filaé (une seule UMJ l'utilise) mais également la société (connectivité partout et tout le temps), il semblerait que cette énorme contrainte n'en soit plus une. De toute façon, ce mode apporterait également de très fortes problématiques en matière de synchronisation des données, donc beaucoup de développement, de tests, de recherches de bogues : et par conséquent pas mal d'argent que les hôpitaux publics n'ont pas.

Une solution serait donc de remplacer cette IHM en EmberJS par une IHM en Ruby on Rails (qui est fait pour ça à la base).

## Détails

### Avantages

La suppression de l'IHM en EMberJS éliminerait :
- le suivi des versions de NodeJS (base de EmberJS) ;
- le suivi des versions de EmberJS (une nouvelle version tous les 2 mois) ;
- le suivi des multiples modules `npm` utilisés (voir plus bas) ;
- les tests côtés IHM, qui sont souvent redondants avec ceux côté API ;
- la communication entre l'IHM et l'API (cf. JSON:API), tout étant interne à une seule application ;
- les tests de communication (cf. PACT).

Concernant les modules `npm`, lors de la tentative de pasage à EmberJS 4, il s'est avéré que de nombreux modules (`ember-fullcalendar`, `ember-parachute`, `ember-data-copyable` par exemple), et les plus importants, n'ont pas franchi le pas également et deviennent donc bloquants.

Concernant les tests d'intégration, nous pourrions également nous passer de Gwen et revenir à une solution plus intégrée à rails, à base de Capybara et/ou Cucumber.


Concernant l'API :
- il sera possible de simplifier également le système d'authentification en se passant d'Oauth et en revenant à une solution plus conventionnelle ;
- il n'y aura _a priori_ aucun changement au niveau des modèles de données (sauf peut-être pour doorkeeper) et donc ne remettra pas en cause l'intégrité de la base de données.
- concernant JSON:API, nous utilisons une version de la gem [jsonapi-rails](https://rubygems.org/gems/jsonapi-rails) que nous avons dû légèrement modifier pour correspondre à nos besoins. Avec cette suppression nous n'ourions plus à maintenir cela non plus.

### Inconvénients

Cette suppression nous obligerait bien évidemment à recoder entièrement l'IHM avec sans doutes quelques changements non seulement au niveau de l'affichage (assez peu a priori) mais également au niveau de l'ergonomie. En effet certaines transitions/affichages sont effectués au niveau du navigateur, pour améliorer l'ergonomie.

Note : à voir, si c'est vraiment trop perturbant, si l'utilisation de [Hotwire](https://hotwired.dev/) ne pourrait permettre d'avoir la même ergonomie dans ces quelques cas particulier ?

Cette réécriture prendra du temps, sans aucun doute, mais moins que de faire une montée de version de EmberJS, qui au final n'est ni plus ni moins qu'une réécriture également.

## Alternatives

Faire les montées de versions de EmberJS : a priori inconcevable vus le temps nécessaire et les forces disponibles actuellement.

Une autre solution serait de réécrire l'IHM en utilisant un autre cadriciel Javascript.

Note : Javascript n'est pas notre langage de prédilection (euphémisme).

## Questions non résolues

La fonctionnalité en mode non-connecté est-elle indispensable ?
- Si OUI, la question ne se pose pas et montées de versions de EmberJS obligatoires ;
- Si NON, la meilleure solution semble de basculer sur Ruby on Rails entièrement.

Faudra-t-il conserver l'API pour une utilisation future potentielle ?
