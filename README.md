Bienvenue dans le dépôt de code source de [FILAÉ](https://filae.frama.io/) pour la planification, la rédaction et la communication des certificats de ses consultations médico-judiciaires. Ce dépôt a pour objectif de contenir toutes les ressources nécessaires au développement de cette solution logicielle libre.

## Comment contribuer ?

**Le code source n'est pas la seule contribution de valeur**. Avis, problèmes, guides d'utilisation… Entrez dans la danse et appréciez la construction collaborative de biens communs offerte par le logiciel libre.

Informations détaillées et coordonnées dans le [guide de contribution](CONTRIBUTING.md).

## Aspects techniques 🛠️

Cette solution logicielle prend la forme d'une constellation de plusieurs projets, aux responsabilités bien déterminées, aux interactions nombreuses, accessibles dans une interface utilisateur unifiée.

### Interface utilisateur

Le dossier `web-gui` contient une [_single-page application_](https://en.wikipedia.org/wiki/Single-page_application) écrite en [EmberJS](https://www.emberjs.com/), une bibliothèque JavaScript à destination de nos navigateurs.

Ce dossier est la partie émergée de l'iceberg. Il offre à l'utilisateur l'accès à toutes les informations pour lesquelles il est habilité, et les interactions possibles sur celles-ci. Toutes les données et interactions sont issues et à destination de la constellation côté serveur.

### Serveur d'API

Le dossier `web-api` contient une application [_Rails API-only_](https://guides.rubyonrails.org/api_app.html), interlocuteur principal pour l'interface utilisateur.

Il met à disposition des points d'entrée HTTP pour récupérer et agir sur les données dans le système. Il orchestre toutes ces opérations conformément aux besoins métier et à la réglementation en vigueur. Il interagit alors avec les différentes bases de données afin de construire ou présenter une information consolidée et cohérente.

#### Base de données relationnelle

Le serveur d'API communique avec une base de données [PostgreSQL](https://www.postgresql.org/). Cette base de données renferme toutes les informations intrinsèquement relationnelles, notamment la configuration du mode de fonctionnement du service (domaines d'intervention, motifs de consultation, etc.), la gestion des utilisateurs et des habilitations…

#### Base de données orientée documents

Le serveur d'API profite également des capacités "orientées documents" de PostgreSQL pour le stockage de documents JSON, version expérimentale des certificats et de leurs modèles.

### Tests de recette automatisés

Le dossier `web-uat` contient un harnais de tests automatisés écrit grâce à [_Gwen_](https://gweninterpreter.org/), un outil d'aide à la [programmation pilotée par le comportement](https://fr.wikipedia.org/wiki/Programmation_pilot%C3%A9e_par_le_comportement). Nécessite l'installation locale de Firefox.

Orientés utilisateurs, ils sont écrits de telle manière à pouvoir être lus comme un manuel d'utilisation.

### Tests par contrats pilotés par les consommateurs

Nous utilisons [Pact](https://docs.pact.io/) pour les tests par contrats pilotés par les consommateurs. Les pactes sont dans leur propre [répertoire](web-gui/pacts) de *web-gui*. Autant que possible nous essayons de les [générer automatiquement](web-gui/pacts/generated) via des [tests de *web-gui*](web-gui/tests/pacts) mais en cas de difficultés nous les écrivons parfois [à la main](web-gui/pacts/web-api).

## Aspects documentaires 📚

Le dossier `doc` a pour vocation de contenir tous les guides nécessaires à l'installation, l'utilisation et le développement de Filaé. C'est un effort permanent qui ne demande pas mieux que d'être partagé 😇

Ainsi, vous y trouverez plus de détails sur comment [déployer](doc/01 Installation), [utiliser](doc/02 Utilisation), [développer](doc/03 Développement) Filaé… Il y même une documentation sur comment [documenter](doc).

À nos claviers !
