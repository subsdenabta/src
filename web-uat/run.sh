#!/usr/bin/env bash

while getopts a:g:h NAME
do
  case $NAME in
    a)FILAE_API_BRANCH=$OPTARG;;
    g)FILAE_GUI_BRANCH=$OPTARG;;
    h)NEED_HELP=1;;
    *)echo "Invalid arg";;
  esac
done
shift $(($OPTIND -1))

if [[ $NEED_HELP ]]; then
  cat << HELP

Run the Gwen test suite against web-gui and web-api:
    - with local (see ENV VARS) ;
    - or remote (see ARGS) repositories.

SYNOPSIS
    ./run.sh [-h] [-a api_branch] [-g gui_branch] [features_path]

ARGUMENTS
    features_path  Space separated list of feature files and/or directories (see Gwen command line interface)
                   Default: features

ENV VARS (local repositories)
    FILAE_API_PATH  Already checked out and runnable web-api repository and branch
    FILAE_GUI_PATH  Already checked out and runnable web-gui repository and branch

ARGS (remote repositories)
    -a api_branch  The branch to checkout when cloning web-api repository (defaults to master)
    -g gui_branch  The branch to checkout when cloning web-gui repository (defaults to master)

MISC
    -h  Show this help message and quit

EXAMPLES
    Local run:
        FILAE_API_PATH=../../web-api FILAE_GUI_PATH=../../web-gui ./run.sh
    Remote run on master branches:
        ./run.sh
    Remote run on specific branches:
        ./run.sh -a 130-horodater-un-certificat -g 237-envoyer-un-certificat
    Mixing local web-gui with remote web-api on specific branch:
        FILAE_GUI_PATH=../../web-gui ./run.sh -a 130-horodater-un-certificat
HELP
  exit 0
fi

if [[ ! -f '.overmind.env' ]]; then
  if [[ -z $FILAE_API_PATH ]]; then
    if [[ -z $FILAE_API_BRANCH ]]; then
      FILAE_API_BRANCH="master"
    fi
    if [[ $FILAE_API_USERNAME ]]; then
      FILAE_API_AUTH="${FILAE_API_USERNAME}:${FILAE_API_TOKEN}@"
      FILAE_API_REPO_URL="https://${FILAE_API_AUTH}framagit.org/infopiiaf/umj78/web-api.git"
    else
      FILAE_API_REPO_URL="git@framagit.org:infopiiaf/umj78/web-api.git"
    fi
    echo && echo Installing web-api…
    FILAE_API_PATH=`mktemp -d --suffix .web-api`
    git clone -b "$FILAE_API_BRANCH" "$FILAE_API_REPO_URL" "$FILAE_API_PATH"
    (cd $FILAE_API_PATH && bundle && bin/rails db:create)
  fi

  if [[ -z $FILAE_GUI_PATH ]]; then
    if [[ -z $FILAE_GUI_BRANCH ]]; then
      FILAE_GUI_BRANCH="master"
    fi
    if [[ $FILAE_GUI_USERNAME ]]; then
      FILAE_GUI_AUTH="${FILAE_GUI_USERNAME}:${FILAE_GUI_TOKEN}@"
      FILAE_GUI_REPO_URL="https://${FILAE_GUI_AUTH}framagit.org/infopiiaf/umj78/web-gui.git"
    else
      FILAE_GUI_REPO_URL="git@framagit.org:infopiiaf/umj78/web-gui.git"
    fi
    echo && echo Installing web-gui…
    FILAE_GUI_PATH=`mktemp -d --suffix .web-gui`
    git clone -b "$FILAE_GUI_BRANCH" "$FILAE_GUI_REPO_URL" "$FILAE_GUI_PATH"
    (cd $FILAE_GUI_PATH && npm install && npx ember build)
  fi

  ENV_FILE=`mktemp --suffix .overmind.env`
  cat << HEREDOC | tee $ENV_FILE
FILAE_API_PATH=$FILAE_API_PATH
FILAE_GUI_PATH=$FILAE_GUI_PATH
HEREDOC
fi

if [[ $# -eq 0 ]]; then
  FEATURES_PATH=features
else
  FEATURES_PATH="$@"
fi

echo && echo Ready to rumble…
OVERMIND_ENV=$ENV_FILE overmind s -p 8400 -P 1 -D && \
sleep 10 && \
./gwen "$FEATURES_PATH" -b

overmind q
