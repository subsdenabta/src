Fonctionnalité: Envoi du certificat de consultation en mode flux
  Afin de transmettre les conclusions d'une consultation en mode flux
  En tant que secrétaire
  Je peux envoyer par courriel le certificat validé

  Scénario: Renseigner l'adresse électronique au moment de l'envoi
    Étant donné que je me connecte en tant que "secretary:secretary"
    Et j'accède à la file d'attente de consultations "Antenne"
    Et je crée une nouvelle consultation de type "GAV" pour "Camille BOUTAMBOU"
    Et je transmets la consultation pour "Camille BOUTAMBOU" à "Physician"
    Et je me connecte en tant que "physician:physician"
    Et j'accède à la file d'attente de consultations "Antenne"
    Et je téléverse un certificat signé pour la consultation de "Camille BOUTAMBOU"
    Et je valide le certificat signé pour "Camille BOUTAMBOU"
    Quand je souhaite envoyer par courriel le certificat validé pour "Camille BOUTAMBOU"
    Et j'envoie le certificat à "cain@pj.fr"
    Et je valide la consultation pour "Camille BOUTAMBOU"
    Alors le courriel est dans la boîte de réception de "cain@pj.fr"
