Fonctionnalité: Envoi du certificat de consultation en mode agenda
  Afin de transmettre les conclusions d'une consultation en mode agenda
  En tant que secrétaire
  Je peux envoyer par courriel le certificat validé

  Scénario: Renseigner l'adresse électronique au moment de l'envoi
    Étant donné que je me connecte en tant que "secretary:secretary"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que je crée un nouveau rendez-vous à "09:00" pour "Camille BOUTAMBOU"
    Et que je renseigne que "Camille BOUTAMBOU" est arrivé
    Et que je me connecte en tant que "physician:physician"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que je prends en charge le rendez-vous de "Camille BOUTAMBOU"
    Et que je me connecte en tant que "secretary:secretary"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que j'accède au détail de la consultation de "Camille BOUTAMBOU"
    Et que je téléverse un certificat signé
    Et que je valide le certificat signé
    Et que j'accède à l'agenda "Consultation médicale"
    Quand je souhaite envoyer par courriel le certificat validé pour "Camille BOUTAMBOU"
    Et que j'envoie le certificat à "cain@pj.fr"
    Alors le certificat est marqué comme envoyé
    Et le courriel est dans la boîte de réception de "cain@pj.fr"
