Fonctionnalité: Envoi du certificat de consultation en mode agenda
  Afin de corriger une erreur dans les conclusions d'une consultation en mode agenda
  En tant que secrétaire
  Je peux dévalider un certifat puis le supprimer

  Scénario: Supprimer juste avant d'envoyer
    Étant donné que je me connecte en tant que "secretary:secretary"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que je crée un nouveau rendez-vous à "09:00" pour "Camille TOMBALO"
    Et que je renseigne que "Camille TOMBALO" est arrivé
    Et que je me connecte en tant que "physician:physician"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que je prends en charge le rendez-vous de "Camille TOMBALO"
    Et que je me connecte en tant que "secretary:secretary"
    Et que j'accède à l'agenda "Consultation médicale"
    Et que j'accède au détail de la consultation de "Camille TOMBALO"
    Et que je téléverse un certificat signé
    Et que je valide le certificat signé
    Et que je dévalide le certificat validé
    Et que je supprime le certificat signé
    Alors aucun certificat signé n'est disponible
