Fonctionnalité: Création d'un nouvel utilisateur
  Afin de permettre à d'autres personnes d'utiliser FILAÉ
  En tant qu'administrateur
  Je peux leur créer un compte-utilisateur

  Scénario: Médecin
    Étant donné que je me connecte en tant que "administrator:administrator"
    Et que j'accède à l'administration des utilisateurs
    Lorsque je souhaite ajouter "c.doc (c.doc@ch.tld)" en tant que "physician"
    Alors "c.doc" est dans la liste des utilisateurs
    Et le courriel est dans la boîte de réception de "c.doc@ch.tld"
