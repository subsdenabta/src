Fonctionnalité: Création d'un nouveau modèle de certificat
  Afin de constituer le référentiel de modèles de certificat
  En tant qu'administrateur
  Je peux créer un modèle de certificat embarqué ou à télécharger

  Scénario: Modèle à télecharger
    Étant donné que je me connecte en tant que "administrator:administrator"
    Et que j'accède à l'administration des modèles de certificat
    Lorsque je souhaite ajouter "AS Femme DOC" pour les "Administrateur"
    Alors "AS Femme DOC" est dans la liste des modèles de certificat
    Lorsque je déplie "AS Femme DOC"
    Et que je téléverse une maquette à télécharger
    Alors "AS Femme DOC" définit une maquette "à télécharger"

  Scénario: Modèle embarqué
    Étant donné que je me connecte en tant que "administrator:administrator"
    Et que j'accède à l'administration des modèles de certificat
    Lorsque je souhaite ajouter "OFPRA" pour les "Administrateur" avec le questionnaire '{"pages":[{"name":"page1","elements":[]}]}'
    Alors "OFPRA" est dans la liste des modèles de certificat
    Et "OFPRA" définit une maquette "embarquée"
