Fonctionnalité: Création d'un nouveau domaine d'intervention
  Afin d'organiser les différentes modalités d'intervention
  En tant qu'administrateur
  Je peux créer un domaine d'intervention par mode opératoire

  Scénario: Mode agenda
    Étant donné que je me connecte en tant que "administrator:administrator"
    Et que j'accède à l'administration des interventions
    Lorsque je souhaite ajouter "Victimologie" en mode "Agenda"
    Alors "Victimologie" est dans la liste des interventions

  Scénario: Mode flux
    Étant donné que je me connecte en tant que "administrator:administrator"
    Et que j'accède à l'administration des interventions
    Lorsque je souhaite ajouter "Antenne mobile" en mode "Flux"
    Alors "Antenne mobile" est dans la liste des interventions
